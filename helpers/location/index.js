var q = require('q');

module.exports.getLocation = function (address) {
    var deferred = q.defer();
    var geocoder = new google.maps.Geocoder();
    
    var location = {
        address: '',
        coords: {
            lat: '',
            lng: ''
        }
    }

    geocoder.geocode({
        'address': address
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            location.address = results[0].formatted_address;
            location.coords.lat = results[0].geometry.location.lat().toString();
            location.coords.lng = results[0].geometry.location.lng().toString();
            deferred.resolve(location);
        } else {
          deferred.resolve(location);
        }
    });
    
    return deferred.promise;
}      
