module.exports = function(req,res,next){
	if(req.url.match(/(^\/en$|^\/en\/)/)){
		req.env.lang = 'en';
		req.env.rootPath = '/en/';
		req.url = req.url.replace(/(^\/en$|^\/en\/)/, '/');
	}else{
		req.env.lang = 'es';
		req.env.rootPath = '/';
	}
	return next();
}