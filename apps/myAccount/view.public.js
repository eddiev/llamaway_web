var directives = require('../../scripts/llamawayDirectives');
var services = require('../../scripts/llamawayServices');
var llamaApi = require('../../helpers/api');
var llamaLocation = require('../../helpers/location/ng.module');
var geoLocation = require('../../helpers/location');
var utils = require('../../scripts/utils');
var gaModule = require('../../helpers/ga/ng.module');
var ga = require('../../helpers/ga');

angular.module('myAccount',[
	'ngRoute', 
	'angularTrix',
	'isteven-multi-select',
	directives.name, 
	services.name, 
	llamaLocation.name,
	'ngFileUpload',
	'ngImgCrop'
])
.config(function($routeProvider) {

	$routeProvider
	.when("/:section/:id?",{})
	.otherwise( { redirectTo: "/mis-datos" });

})
.controller('MyAccountCtrl', function($scope,
							          $timeout,
							          $route,
									  $location,
									  $window,
									  $routeParams,
									  $rootScope,
									  gzzRoute,
									  $q,
									  Upload){

	llamaApi.loadQ($q)

	$scope.data = {
		genericErrorMessage:'Completá este dato',
		invalidFormat: 'Ingresá un email válido',
		invalidPasswordLength: 'La longitud debe ser mayor a 6 y menor a 20',
		existingUser: 'El email ingresado está siendo utilizado por otro usuario'
	}
	$scope.ix = {};
	$scope.data.userLoaded = false;
	$scope.ix.isUserCompany = false;
	$scope.ix.existsPageName = false;
	$scope.ix.existingPageName = false;

	$scope.data.info = {
		email:'',
		password:''
	}

	$scope.multiselect = {
		localLang: {
	    	selectAll: "Tick all",
		    selectNone: "Tick none",
		    reset: "Undo all",
		    search: "Type here to search...",
		    nothingSelected : "Selecciona una o varias"
	    },
	    listActivityType : [
		  {id: 1, name: 'Acúatica'},
		  {id: 2, name: 'Avistaje'},
		  {id: 3, name: 'Cabalgatas'},
		  {id: 4, name: 'Camping'},
		  {id: 5, name: 'Ciclismo'},
		  {id: 6, name: 'Deportes Extremos'},
		  {id: 7, name: 'Deportes Invernales'},
		  {id: 8, name: 'Expediciones/Travesías'},
		  {id: 9, name: 'Montañismo y Trekking'},
		  {id: 10, name: 'Pesca'},
		  {id: 11, name: 'Tours/Otros'}
		]
	}

	var updateCompany = function(company){
		if (company) {
			$scope.$emit('sendLocation', company.location);
			if(company.pageName){
				$scope.ix.existsPageName = true;
			}

			var url = utils.getCompanyProfileThumbnailSearch(company, false);
			if(!company.profile_picture){
				company.profile_picture = {}
			}
			company.profile_picture.url = url;

			$scope.data.info.company = company;
			if(company.golocation){
				$scope.autocomplete = company.golocation.formatted_address;
				$scope.golocation = company.golocation;
			}

			if($scope.data.info.company.activities && $scope.data.info.company.activities.length > 0){
				$scope.data.info.company.activities.forEach(function(item) {
				    $scope.multiselect.listActivityType.forEach(function(itemList){
				    	if(itemList.id == item.id){
				    		itemList.check = true;
				    		return true;
				    	}
				    	return false;
				    });
				});
			}
		}
	}

	var init = {
		'mis-aventuras':function(){
			if($scope.user && $scope.user.companyId) {
				$scope.ix.isUserCompany = true;
				$rootScope.$emit('toogleSpinner', true);
				llamaApi.company.getAdventuresById($scope.user.companyId).then(function (adventures) {
				adventures.results.forEach(function(current){
					var url;
					if(window.device.mobile){
				 		url = utils.getAdventureProfileThumbnailSearch(current, true);
				 	}else{
                 		url = utils.getCompanyProfileThumbnail(current, true);
				 	}
	                if(!current.profile_picture){
	                    current.profile_picture[0] = {}
	                }
                	current.profile_picture[0] = {'url': url};
				});
					$scope.data.info.adventures = adventures.results;
				}).finally(function(){
					$rootScope.$emit('toogleSpinner', false);
				});
			}else{
				$rootScope.$emit('toogleSpinner', false);
			}
		},
		'mis-datos':function(){
			$scope.ix.infoUpdated = false;
			$rootScope.$emit('toogleSpinner', true);
			$scope.ix.existsPageName = false;
			$scope.ix.showWelcome = false;
			$scope.ix.passwordUpdated = false;

			if($scope.user){ // No estoy logueado
				$scope.data.info.email = $scope.user.email;
				$scope.data.info.companyId = $scope.user.companyId;
				$scope.data.info.notificationConfig = $scope.user.notificationConfig;
				$scope.ix.isUserCompany = $scope.user.companyId ? true : false;

				if ($scope.ix.isUserCompany) {
					if(localStorage.getItem('reg') == "emp"){
						ga.event('User', 'CreateOrganization', $scope.data.info.companyId);
						localStorage.removeItem('reg');
						$scope.ix.showWelcome = true;
					}

					// si no tengo un company lo pido
					if (typeof($scope.data.info.company) == 'undefined') {

						llamaApi.company.getById($scope.user.companyId, true).then(function(company) {
							updateCompany(company);
							$rootScope.$emit('toogleSpinner', false);
						});
					}else{
						$rootScope.$emit('toogleSpinner', false);
					}
				} else {
					// escondo los datos de empresa.
					$scope.ix.isUserCompany = false;
					$rootScope.$emit('toogleSpinner', false);
				}
			}else{
				$rootScope.$emit('toogleSpinner', false);
			}
		},
		'mis-paquetes':function(){
			$rootScope.$emit('toogleSpinner', true);
			if($scope.user){ // No estoy logueado
				if ($scope.ix.isUserCompany) {
					llamaApi.package.getPackages().then(function(packages){
						$scope.data.packages = packages;
						var selectedIndex;
						$scope.data.packages.forEach(function(current, index){
							if(current._id == $scope.selectedPackage){
								selectedIndex = index;
							}
						});
						if($scope.selectedPackage){
							var el = document.getElementById($scope.selectedPackage);
							$timeout(function(){
								window.scrollTo(0, document.getElementById($scope.selectedPackage).offsetTop-100);
							}, 500);
							$timeout(function(){
								document.getElementsByClassName('package selected')[0].classList.remove('selected');
							}, 3000);
						}
						$rootScope.$emit('toogleSpinner', false);
					});
				} else {
					location.hash = "mis-datos";
					$rootScope.$emit('toogleSpinner', false);
				}
			}else{
				$rootScope.$emit('toogleSpinner', false);
			}
		},
		'mis-consultas':function(){
			$rootScope.$emit('toogleSpinner', true);
			if($scope.user){
				if ($scope.ix.isUserCompany) {
					if (typeof($scope.data.info.company) == 'undefined') {
						llamaApi.company.getById($scope.user.companyId, true).then(function(company) {
							company = utils.getLeadsTransformationsCompany(company);
							updateCompany(company);
						});
					}
					llamaApi.lead.getById($scope.user.companyId).then(function(leads){
						$scope.data.leads = leads;
						var selectedIndex;
						$scope.data.leads.forEach(function(current, index){
							if(current._id == $scope.selectedLead){
								selectedIndex = index;
							}
							if(current.adventure){
					            /*var url = utils.getCompanyProfileThumbnailSearch(current.adventure, true);
					            current.adventure.profile_picture = {'url': url};
								*/
								current.adventure = utils.getLeadsTransformationsAdventure(current.adventure);
							}
						});
						if($scope.selectedLead){
							var el = document.getElementById($scope.selectedLead);
							$timeout(function(){
								window.scrollTo(0, document.getElementById($scope.selectedLead).offsetTop-100);
							}, 500);
							$timeout(function(){
								document.getElementsByClassName('lead selected')[0].classList.remove('selected');
							}, 3000);
						}
						$rootScope.$emit('toogleSpinner', false);
					});
				} else {
					location.hash = "mis-datos";
					$rootScope.$emit('toogleSpinner', false);
				}
			}else{
				$rootScope.$emit('toogleSpinner', false);
			}
		},
		'contraseña':function(){
		}
	}
	$scope.showCards = true;
	$scope.showCard = function(leadId){
		$scope.selectedCard = leadId;
		$scope.showCards = false;
	}

	$scope.showAdventures = function(){
		location.hash = "/mis-aventuras";
	}

	$scope.showPlan = function(){
		location.href = "/paquetes";
	}

	$scope.showPackages = function(){
		location.hash = "/mis-paquetes";
	}

	$scope.showContact = function(){
		location.hash = "/mis-consultas";
	}

	$scope.retomarPago = function(packageId){
		location.href = "/confirmarPaquete/" + packageId;
	}

	$scope.unlock = function(lead){
		if(lead.state == 'pending'){
			$rootScope.$emit('toogleSpinner', true);
			llamaApi.lead.unlock(lead._id).then(function(lead){
				$scope.data.leads.some(function(current){
					if(current._id == lead._id){
						current.description = lead.description;
						current.phone = lead.phone;
						current.contactName = lead.contactName;
						current.email = lead.email;
						current.state = lead.state
						$scope.data.info.company.leads--;
			            return true;
					}
					return false;
				});
				$rootScope.$emit('toogleSpinner', false);
			}).catch(function(err){
				if(err && err.statusCode){
					if(err.statusCode == 402){
						sessionStorage.setItem('slpkg', lead._id);
						location.href = "/paquetes";
					}
				}
				$rootScope.$emit('toogleSpinner', false);
			});
		}
	}

	$scope.updateInfo = function(){
		$scope.ix.emailConflict=false;
		$scope.ix.submitting = 'info';
		$scope.$broadcast('validateAll','info',function(valid){
			if(valid){
				llamaApi.user.updateInfo($scope.data.info).then(function(newUser){
					showSuccess('infoUpdated');
					$scope.ix.submitting = false;
				}).catch(function(err){
					if(err && err.statusCode == 409){
						$scope.ix.emailConflict=true;
					}
					$scope.$broadcast('validateOne', function(){});
				}).finally(function(){
					$scope.ix.submitting = false;
				})
			}else{
				$scope.ix.submitting = false;
			}
		});
	}

	$scope.updatePage = function(){
		$scope.ix.submitting = 'page';
		
		isValidCompanyName($scope.data.info.company.name);
		isValidPageName($scope.data.info.company.pageName);
		$scope.isValidLocation = $scope.isAddressComponentsComplete($scope.golocation);
		$scope.$broadcast('validateAll','myPageForm',function(valid){
			if(valid){				
				llamaApi.company.updatePage({
					name: $scope.data.info.company.name,
					pageName: $scope.data.info.company.pageName,
					description: $scope.data.info.company.description,
					activities: $scope.data.activities,
					golocation: $scope.golocation,
					_geoloc: $scope.golocation.location
				}).then(function(newCompany){
					updateCompany(newCompany);
					$scope.ix.existsPageName = true;
					showSuccess('pageUpdated');
				}).catch(function(err){
					$scope.$broadcast('validateOne', function(){});
				}).finally(function(){
					$scope.ix.submitting = false;
				});
			}else{
				$scope.ix.submitting = false;
			}
		});
	}

	$scope.updatePassword = function(){
		$scope.ix.submitting = 'pass';
		$scope.$broadcast('validateAll','pass',function(valid){
			if(valid){

				llamaApi.user.updatePassword($scope.data.password).then(function(status){
					showSuccess('passwordUpdated');
				}).catch(function(err){
					$scope.$broadcast('validateOne', function(){});
				}).finally(function(){
					$scope.ix.submitting = false;
				})
			}else{
				$scope.ix.submitting = false;
			}
		});
	}
	
	$scope.sendValidateEmail = function(){
		$scope.ix.submitting = 'info';
		llamaApi.user.sendValidationEmail().then(function(response){
			ga.event('User', 'Email-Validation', $scope.data.info.email);
			$scope.ix.infoUpdated = true;
			$window.scrollTo(0,0);
			$scope.ix.submitting = false;
		}).catch(function(err){
			$scope.$broadcast('validateOne', function(){});
		}).finally(function(){
			$scope.ix.submitting = false;
		})
	}

	// Remover aventuras
	$scope.removeAdventure = function(adventure){
		llamaApi.company.removeAdventure(adventure).then(function(res){
			adventure.deleted = true;
			//location.reload();
		}).finally(function(){
		});
	}	

	$scope.askRemove = function(adventure){
		adventure.showRemove = !adventure.showRemove;
	}

	$scope.viewAdventure = function(adventure){
		ga.event('User', 'ViewReview');
		location.href = "/aventura/"+ adventure.safeUrlName + "_" + adventure._id;
	}

	$scope.editAdventure = function(adventure){
		ga.event('User', 'EditReview');
		location.href = "/editar-aventura/"+ adventure.safeUrlName + "_" + adventure._id;
	}

	$scope.$on('$routeChangeSuccess', function (ev, current, prev) {
		checkSession().then(function(user){
			if(user){
				$scope.user = user;
				$scope.data.userLoaded=true;
				$scope.ix.section = $routeParams.section;
				switch($scope.ix.section){
					case 'mis-consultas':
						$scope.selectedLead = $routeParams.id;
					break;
					case 'mis-paquetes':
						$scope.selectedPackage = $routeParams.id;
					break;
				}

				if ($scope.user.companyId != undefined) {
					if($scope.data.info.company == undefined){
						llamaApi.company.getById($scope.user.companyId, true).then(function(company) {
							$scope.ix.isUserCompany = true;
							updateCompany(company);
							if(init[$scope.ix.section]){
								init[$scope.ix.section]();
							}
						});
					}else{
						if(init[$scope.ix.section]){
							init[$scope.ix.section]();
						}
					}
				} else {
					$scope.ix.isUserCompany = false;
				}
			} else {
				$scope.data.userLoaded=false;
				gzzRoute.loginAndComeBack();
			}
		});
	});

	$scope.changeCompanyName = function(){
		if($scope.data.info.company.name){
			$scope.data.info.company.name = $scope.data.info.company.name.replace(/[^0-9a-zA-Z-áéíóúñ&% \:\.\'\,]/gi, '')
			$scope.data.info.company.pageName = $scope.data.info.company.name;
			$scope.changePageName();
		}
	}

	var checkSession = function(){
		return llamaApi.user.getCurrent().catch(function(){
			return false;
		});
	}

	$scope.showSearchBox = function(){
		$rootScope.$broadcast('showSearchBox');
	}

	function isValidCompanyName(companyName){
		if(companyName && !companyName.match(/[^0-9a-zA-Z-áéíóúñ&% \:\.\'\,]/gi)){
			$scope.data.isInvalidName = false;
			return true;
		} else {
			$scope.data.isInvalidName = true;
			return false;
		}
	}

	$scope.changePageName = function(){
		if($scope.data.info.company.pageName){
			var pageName = $scope.data.info.company.pageName.toLowerCase();
			pageName = pageName.replace(/[\b&% \.\'\,]/g, '-')
			.replace(/á/g, 'a')
			.replace(/é/g, 'e')
			.replace(/í/g, 'i')
			.replace(/ó/g, 'o')
			.replace(/ú/g, 'u')
			.replace(/ñ/g, 'n')
			.replace(/[^0-9a-z\-]/gi, '');

			while(pageName.match(/--/gi)){
				pageName = pageName.replace(/--/g, '-');
			}

			$scope.data.info.company.pageName = pageName;
		}
	}

	$scope.verifyPageName = function(){
		llamaApi.company.verifyPageName($scope.data.info.company.pageName).then(function(res){
			if(res.exists == false){
				$scope.ix.existingPageName = false;
			}else{
				$scope.ix.existingPageName = true;
			}
		}).catch(function(){
			$scope.ix.existingPageName = true;
		})
		.finally(function(){
		});
	}

	function isValidPageName(pageName){
		if(pageName && !pageName.match(/[^0-9a-zA-Z-áéíóúñ&% \:\.\'\,]/gi) && pageName.length > 3){
				$scope.data.isInvalidPageName = false;
				return true;
		} else {
			$scope.data.isInvalidPageName = true;
			return false;
		}
	}

	var showSuccess = function(view){
		$scope.ix[view] = true;
		$timeout(function(){
			$scope.ix[view] = false;
		}, 3000);
	}


	function getDivisorRatio(width, height){
		var aspectRatio = 1;
		var divisor = 2;
		var originalWidth = width;
		var originalHeight = height;

		while (width > 1000 || height >1000) {
    		width = (originalWidth/divisor)*aspectRatio;
    		height = (originalHeight/divisor)*aspectRatio;
			divisor++;
		}
		return divisor-1;
	}

	//Para que se actualice con cada movimiento
	$scope.blockingObject = {'block':true};

	$scope.cancelUpload = function(){
		$scope.picFile = null;
	}

	/*Magia, refactorizarla bien*/
	$scope.upload = function (name) {
		$rootScope.$emit('toogleSpinner', true);

		$scope.blockingObject.render(function(dataUrl){
			if(Upload.isResizeSupported()){

				var rawFile = Upload.dataUrltoBlob(dataUrl, name);

				Upload.imageDimensions(rawFile).then(function(dimensions){
					if(dimensions.width > 1000 || dimensions.height >1000){
						var divisor = getDivisorRatio(dimensions.width, dimensions.height);
						Upload.resize(rawFile, 1000, 1000, 1, 'image/jpeg', divisor+':1', false, function (width, height) {
							if(dimensions.width > 1000 || dimensions.height > 1000){
								return true;
							} else {
								return false;
							}
						}).then(function(resizedFile){
							if($scope.user.companyId && resizedFile){
								llamaApi.company.updateCompanyProfilePicture(
									$scope.user.companyId, 
									resizedFile,
									true)
								.then(function(){
									location.reload();
								}).catch(function(err){
									$scope.ix.submitting = false;
								}).finally(function(){
									$rootScope.$emit('toogleSpinner', false);
								})
							}
						});
					} else {
						if($scope.user.companyId && rawFile){
							llamaApi.company.updateCompanyProfilePicture(
								$scope.user.companyId, 
								rawFile,
								true)
							.then(function(){
								location.reload();
							}).catch(function(err){
								$scope.ix.submitting = false;
							}).finally(function(){
								$rootScope.$emit('toogleSpinner', false);
							})
						}
					}
				});
			} else {
				$rootScope.$emit('toogleSpinner', false);
				console.log('Browser incompatible')
			}
		});
    }

    $scope.$on('gzzLocation_details', function(event, args) {
        $scope.details = args;
        $scope.autocomplete = args.formatted_address;
    });

	window.s = $scope;
});
