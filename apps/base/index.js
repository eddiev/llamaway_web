var mustache = require('mustache');
var path = require('path');
var q = require('q');
var readFile = require('fs-readfile-promise');

var utils = require('../../scripts/utils');

var i18nObj = require('./i18n.json');
var templatePath = path.resolve(__dirname,'./template.html');

exports.render = function(req,res){
	var promises = [];
		promises.push(readFile(templatePath));	

	return q.all(promises).then(function(responses){
		var template = responses[0].toString();
		

		var data = {
			text:utils.i18n(i18nObj)
		}

		return {
			title:'Title',
			html:mustache.render(template,data)
		}
	}).catch(function(){
		console.log(arguments)
	});
}