exports.event = function(cat,ac,label, value){
	var args =  Array.prototype.slice.call(arguments);
	if(window.dataLayer){
		pushDataLayer(args);
	}else{
		var loadInterval = setInterval(function(){ 
			if(window.dataLayer){
				pushDataLayer(args);	
			}
		}, 500);		
	}
	if(window.ga){
		window.ga.apply(window.ga,['send', 'event'].concat(args));
	} else {
		var gaLoadInterval = setInterval(function(){ 
			if(window.ga){
				window.ga.apply(window.ga,['send', 'event'].concat(args));
				clearInterval(gaLoadInterval)
			}
		}, 500);
	}
}

exports.pageview = function(){
	var args =  Array.prototype.slice.call(arguments);

	if(window.ga){
        window.ga('send', 'pageview', {'page': location.pathname + location.search  + location.hash});
	} else {
		var gaLoadInterval = setInterval(function(){ 
			if(window.ga){
				window.ga('send', 'pageview', {'page': location.pathname + location.search  + location.hash});
				clearInterval(gaLoadInterval)
			}
		}, 500);
	}
}

var pushDataLayer = function(args) {
    var objEvent = {}
    if (args[1]) {
        objEvent.event = args[1];
        if (args[2]) {
            objEvent.action = args[2];
        }
        window.dataLayer.push(objEvent);
    }
}