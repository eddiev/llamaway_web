require('../../public/libs/ngstorage/ngStorage');

var llamaApi = require('../../helpers/api');
var directives = require('../../scripts/llamawayDirectives');
var services = require('../../scripts/llamawayServices');
var gaModule = require('../../helpers/ga/ng.module');
var autocompleteModule = require('../../helpers/autocomplete/ng.module');
var cookie = require('cookies-js');
var q = require('q');
var llamaLocation = require('../../helpers/location/ng.module');
var store = require('store2');
var ga = require('../../helpers/ga');

module.exports = angular.module('mainLayout',[directives.name,
										      services.name,
										      gaModule.name,
										      autocompleteModule.name,
										      window.appName,
										      'ngStorage',
										      'ngCookies',
										      'ngAnimate',
										      llamaLocation.name])

.controller('MainLayoutCtrl', function($scope,$cookies, $rootScope, gzzRoute,
											$q){

	llamaApi.loadQ($q)

	var mainSpinner = document.querySelector('.mainSpinner')
	llamaApi.host = window.env.api_host;

	$scope.data = {
		searchQuery: ""
	}

	$scope.ix = {
		modal:false
	}

	$rootScope.$on('toogleSpinner', function(event, args) {
		if(args){
			mainSpinner.style.display = 'block';
		} else {
			mainSpinner.style.display = 'none';
		}
	});

    var checkSession = function() {
        return llamaApi.user.getCurrent().catch(function() {
            return false;
        });
    }

    checkSession().then(function(user) {
        if (user && user.companyId) {
    		llamaApi.notification.getUnread().then(function(notifications) {
	            if(notifications && notifications.length > 0){
	            	var now = Date.now();
	            	notifications.forEach(function(current){
	            		var date1 = new Date(current.created_at);
						var timeDiff = Math.abs(now - date1.getTime());
						current.contactDateDays = Math.abs(timeDiff / (1000 * 3600 * 24));
						if(current.contactDateDays >= 30){
							current.contactDateDays = Math.round(current.contactDateDays / 30);
							current.contactDateLabel = current.contactDateDays == 1 ? 'mes' : 'meses';
						}else{
							if(current.contactDateDays >= 7){
								current.contactDateDays = Math.round(current.contactDateDays / 7);
								current.contactDateLabel = current.contactDateDays == 1 ? 'semana' : 'semanas';
							}else{
								current.contactDateDays = Math.round(current.contactDateDays);
								if(current.contactDateDays == 0){
									current.contactDateLabel = 'hoy';
								}else{
									current.contactDateLabel = current.contactDateDays == 1 ? 'día' : 'días';
								}
							}
						}
	            	});
	            	$scope.notifications = notifications;
	            }
	        });
        }
    });

    $scope.toggleNotifications = function(){
		$scope.showNotif = $scope.showNotif ? false : true;
		$scope.ix.modal = $scope.ix.modal == 'notification' ? null : 'notification';
    	if($scope.notifications && $scope.notifications.length > 0){
			sessionStorage.removeItem('ntf');
    	}
    	if(!$scope.showNotif){
			$scope.notifications = null;
    	}
    }

    $scope.showPackages = function(id){
    	$scope.toggleNotifications();
    	if(id){
    		window.location.href = "/mi-cuenta#/mis-paquetes/" + id;
    	}else{
    		window.location.href = "/mi-cuenta#/mis-paquetes";
    	}
    	$scope.ix.modal=null;
    }

    $scope.showLeads = function(id){
    	$scope.toggleNotifications();
    	if(id){
    		window.location.href = "/mi-cuenta#/mis-consultas/" + id;
    	}else{
    		window.location.href = "/mi-cuenta#/mis-consultas";
    	}
    	$scope.ix.modal=null;
    }

	$scope.onSelect = function(item){
		$scope.__dontWatch = true;
		$scope.data.searchQuery = item.name;
		location.href = item.link;
	}
	
	$scope.$watch('data.searchQuery', function(newVal) {
        if ($scope.__dontWatch === false) {
            if (newVal && newVal.length > 1) {
                var query = {
                    name: newVal,
                    limit: 5
                }
                q.all([llamaApi.company.searchAdventure(query),
                    llamaApi.company.searchCompany(query)
                ]).then(function(responses) {
                    (function() {
                        var list = [];
                        if (responses[0].results.length > 0) {
                            responses[0].results.forEach(function(current) {
                                current.link = "/aventura/" + current.safeUrlName + "_" + current._id;
                            });
                            list.push({
                                label: 'Aventuras',
                                elements: responses[0].results
                            });
                        }
                        if (responses[1].results.length > 0) {
                            responses[1].results.forEach(function(current) {
                                current.link = "/e/" + current.pageName;
                            });
                            list.push({
                                label: 'Guias',
                                elements: responses[1].results
                            });
                        }
                        $scope.instantSearchResult = list;
                        $scope.$apply();
                    })();
                });
            } else {
                $scope.instantSearchResult = [];
            }
        }
        $scope.__dontWatch = false;
    });

	$scope.search = function(){
        var query = $scope.data.searchQuery.trim();
        ga.event('User', 'SearchHeader', query);
        gzzRoute.search(query);
	}
	
	$scope.logout = function(){
		llamaApi.user.logout();
		location.href = '/';
	}
	
	$scope.login = function(){
		store('afterLoginRedirect', location.href);
		location.href = '/ingreso';
	}

	userMenu();

	function userMenu(){
		llamaApi.user.getCurrent().then(function(user){
			$scope.userLoaded = true;
			$scope.user = user;
		});
	}

	$rootScope.$on('showLoggedMenu',function(){
		userMenu();
	});

	$scope.$on('showMenu',function(){
		$scope.ix.modal='menu';
	});

	$scope.$on('showSearchBox',function(){
		$scope.ix.modal='searchBox';
	});

	$scope.$watch('ix.modal',function(newVal,oldVal){
		if(newVal!=oldVal && $scope.ix.modal=='searchBox'){
			$scope.ix.focusSearchBox=true;
		}
	})

	$scope.submit = function(e){
		e.preventDefault();
		var query = $scope.data.searchQuery.trim();
		ga.event('User', 'Search', 'Menu', query);
		gzzRoute.search(query);
	}

	/*
	var deltaScrollY = 0;
	var lastScrollY = 0;
	var nav = document.querySelector('.top-nav');
	window.onscroll = function() {
		deltaScrollY++;
		if(deltaScrollY >20) {
			deltaScrollY = 0;
			if(req.device && req.device.isMobile){
				if(window.scrollY > lastScrollY){
					nav.style.position = 'absolute';
				} else {
					nav.style.position = 'fixed';
				}
			}
			lastScrollY = window.scrollY;
    	}
	};
	*/

	var canonicalUrl;
	$rootScope.$on('setCanonicalUrl', function(event, args) {
		if(args && args != ''){
			if(!canonicalUrl){
				var link = document.createElement('link');
				link.setAttribute('rel', 'canonical');
				link.setAttribute('href', args);
				canonicalUrl = link;
				document.querySelector('HEAD').appendChild(link);
			}else{
				canonicalUrl.setAttribute('href', args);
			}
		} else {
			if(canonicalUrl){
				document.querySelector('HEAD').removeChild(canonicalUrl);
			}
		}
	});
	
	window.ml = $scope;
})
.filter('log',function(){
	return function(input){
		console.log(input)
	}
});
