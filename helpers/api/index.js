var request = require('request');
var store = require('store2');
var q = require('q');
var url = require('url');
var isNode = require('detect-node');
var cookie = require('cookies-js');
var isDefined = function(ref){ return (typeof ref != 'undefined'); }
// window.env.api_host
// prod api
var prodApiHost = 'https://llamaway-api.herokuapp.com';

var client = {
	host:isNode ? process.env.API_HOST || prodApiHost  : window.env.api_host || prodApiHost
}

client.loadQ = function ($q) {
	if($q){
		q = $q;
	}
}

var apiUrl = function(path){
	return url.resolve(client.host,path);
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

(function startUTI(){
	var uti = store('uti');
	if(!uti){
		store('uti',guid());
	}
})();

var prequest = function(conf){
	var access_token = store('access_token');
	var defaults = {};

	if(access_token){
		defaults = {
			json:true,
			headers: {
				'Authorization':'Bearer '+access_token,
				'uti':store('uti'),
				'X-Country':isNode?'':cookie.get('locale')
			}
		}
	}else{
		defaults = {
			json:true,
			headers: {
				'uti':store('uti')
			}
		}
	}


	var r = request.defaults(defaults);

	var deferred = q.defer();

	r(conf,function(err,res,body){
		// no uesar res.status, solo existe en la version browser
		if(!err && res && res.statusCode-200<100){
			deferred.resolve(body);
		}else{
			deferred.reject(body);
		}
	})
	return deferred.promise;
}

var api = {
	get:function(conf){
		return this.method('GET',conf);
	},
	post:function(conf){
		return this.method('POST',conf);
	},
	put:function(conf){
		return this.method('PUT',conf);
	},
	del:function(conf){
		return this.method('DELETE',conf);
	},
	method:function(method,conf){
		conf.method=method;
		conf.json= isDefined(conf.json)? conf.json : true;
		conf.uri = apiUrl(conf.uri);
		return prequest(conf);
	}
}


client.test = function(){
	api.get({uri:'/health_check'}).then(function(res){
		console.log(res);
	})
}

client.user = {
	signinReturnUrl:function(){
		var url = '';
		if(isDefined(location)){
			url = location.origin+ '/loginCallback';
		}else{
			url = 'https://www.llamaway.com/loginCallback';
		}
		return url;	
	},
	getToken:function(params){
		return api.post({
			uri:'/users/token',
			body:params
		})
	},
	loginWithEmailAndPassword:function(params,redirectURL){
		var that = this;
		
		return this.getToken(params).then(function(res){
			location.href =  apiUrl('/auth/token/'+res.token+'?returnTo='+that.signinReturnUrl());
		});
	},
	loginWithFacebook:function(redirectURL, company){
		redirectURL = redirectURL || location.href;
		
		store('afterLoginRedirect',redirectURL);
		if(company){
			store('afterLoginCompany', company);
		}
		location.href =  apiUrl('/auth/facebook?sl='+ company || false +'&returnTo='+this.signinReturnUrl());
		
	},
	recoverPassword:function(email){
		var redirectURL = location.origin + '/mi-cuenta#/mis-datos';
		store('afterLoginRedirect',redirectURL);

		return api.post({
			uri:'/users/'+email+'/recoverPassword?returnTo='+this.signinReturnUrl(),
			body:{}
		});	
	},
	logout:function(){
		store.remove('access_token');
		store.remove('cachedUser');
		localStorage.removeItem('access_token');
		sessionStorage.removeItem('cachedUser');
		sessionStorage.removeItem('ntf');
	},
	getCurrent:function(){
		var deferred = q.defer();
		if(store.session('cachedUser')){
			deferred.resolve(store.session('cachedUser'));
		}else{
			var date = new Date();
			var options = {}
			api.get({
				uri:'/users/me', 
				qs:options
			}).then(function(res){
				store.session('cachedUser',res);
				deferred.resolve(res);
			}).catch(function(err){
				deferred.reject(err);
			});
		}
		return deferred.promise;
	},
	updateInfo:function(newInfo){
		store.remove('cachedUser');
		sessionStorage.removeItem('cachedUser');
		return api.put({
			uri:'/users/me',
			body:newInfo
		});	
	},
	updatePassword:function(password){
		return api.put({
			uri:'/users/password',
			body:{'pd':password}
		});	
	},
	signup:function(user){
		return api.post({
			uri:'/users/',
			body:user
		});	
	},
	signupAndLogin:function(user){
		var deferred = q.defer();
		api.post({
			uri:'/users/experiment',
			body:user
		}).then(function(res){
			store('access_token', res.token)
			deferred.resolve(res.user);
		}).catch(function(err){
			deferred.reject(err);
		});
		return deferred.promise;
	},
	sendValidationEmail:function() {
		var deferred = q.defer();
		api.post({
			uri:'/users/emailValidation',
			body:{}
		}).then(function(res){
			store('access_token', res.token)
			deferred.resolve(res.user);
		}).catch(function(err){
			deferred.reject(err);
		});
		return deferred.promise;		
	},
	getUserByCompanyId:function (companyId) {
		var deferred = q.defer();
		api.get({
			uri: '/users/' + companyId,
			body: {}
		}).then(function(res){
			deferred.resolve(res);
		}).catch(function(err){
			deferred.reject(err);
		});

		return deferred.promise;
	}
}

client.company = {
	get:function(params){
		var query = {};
		return api.get({
			uri:'/companies',
			qs:params||{}
		});
	},
	create:function(company){
		return api.post({
			uri:'/companies/',
			body:company
		});	
	},
	getById:function(id, myCompany){
		if(!myCompany){
			return api.get({
				uri:'/companies/'+id
			});
		} else {
				return api.get({
				uri:'/companies/'+id+'/me'
			});
		}

	},
	getByPageName:function(pageName){
		return api.get({
			uri:'/companies/byPageName/'+pageName
		});
	},
	updateOrCreateAdventure:function(companyId, adventure){
		if(adventure._id){
			return api.put({
				uri:'/companies/'+companyId+'/adventures/'+adventure._id,
				body:adventure
			});	
		}else{
			var formData = new FormData();
			formData.append('profile_picture', adventure.profile_picture);
			formData.append('name', adventure.name);
			formData.append('activityType', adventure.activityType);
			formData.append('description', adventure.description);
			formData.append('golocation', JSON.stringify(adventure.golocation));
			formData.append('place', adventure.place);
			formData.append('takeDays', adventure.takeDays);
			formData.append('duration', adventure.duration);
			formData.append('difficulty', adventure.difficulty);
			formData.append('price', adventure.price);
			formData.append('withoutPrice', adventure.withoutPrice);
			formData.append('startDate', adventure.startDate);
			formData.append('finishDate', adventure.finishDate);
			formData.append('currency', adventure.currency);
			formData.append('payMethod', adventure.payMethod);
			return api.post({
				uri:'/companies/'+companyId+'/adventures',
				body:formData,
				json:false
			}).then(function(res){
				return JSON.parse(res);
			});
		}
	},
	getAdventuresById:function(companyId){
		return api.get({
			uri:'/companies/'+companyId+'/adventures'
		});
	},
	getAdventureById:function(adventureId){
		return api.get({
			uri:'/adventures/'+adventureId
		});
	},
	guessLogo:function(name){
		return api.get({
			uri:'/companies/guessLogo',
			qs:{
				name:name||''
			}
		});
	},
	updateCompanyProfilePicture:function(companyId, file, replacePicture){
		if(companyId && file){
			var formData = new FormData();
			formData.append('profile_picture', file);
			formData.append('replacePicture', true);
			
			return api.put({
				uri:'/companies/'+companyId+'/profile_picture/',
				body:formData,
				json:false
			}).then(function(res){
				return JSON.parse(res);
			});
		}else{
			return false;
		}
	},
	getProfilePictureUrl: function(companyId, adventureId){
		return apiUrl('/companies/'+ companyId +'/adventures/'+ adventureId +'/profile_picture/');
	},
	createAdventureProfilePicture:function(companyId, adventureId, file){
		if(companyId && file){
			var formData = new FormData();
			formData.append('profile_picture', file);
			
			return api.post({
				uri:'/companies/'+companyId+'/adventures/'+adventureId+'/profile_picture/',
				body:formData,
				json:false
			}).then(function(res){
				return JSON.parse(res);
			});
		}else{
			return false;
		}
	},
	deleteAdventureProfilePicture:function(companyId, adventureId, pictureId){
		if(companyId && adventureId && pictureId){
			return api.del({
				uri:'/companies/'+companyId+'/adventures/'+adventureId+'/profile_picture/'+pictureId,
				json:false
			}).then(function(res){
				return JSON.parse(res);
			});
		}else{
			return false;
		}
	},
	search:function(q,filters){
		var qs=filters||{};
		qs.q = q;
		
		return api.get({
			uri:'/companies/search',
			qs:qs
		});
	},
	searchAdventure:function(q){
		var qs=q||{};
		
		return api.get({
			uri:'/adventures/search',
			qs:qs
		});
	},
	searchCompany:function(q){
		var qs=q||{};
		
		return api.get({
			uri:'/companies/search',
			qs:qs
		});
	},
	removeAdventure:function (adventure) {
		return api.del({
			uri:'/companies/'+adventure.companyId+'/adventures/'+adventure._id,
			body:adventure
		});
	},
	updatePage:function(newDataPage){
		return api.put({
			uri:'/companies/page',
			body:newDataPage
		});	
	},
	verifyPageName: function(pageName){
		return api.get({
			uri:'/companies/verifyPageName',
			qs:{'pageName':pageName}
		});
	},
	getCompanyById: function (companyId) {
		return api.get({
			uri: '/companies/' + companyId
		});
	}
}

client.lead = {
	createLead: function(data){
		if(data){
			return api.post({
				uri:'/leads/'+data.companyId,
				body:data
			});	
		}
	},
	getById: function (companyId) {
		return api.get({
			uri: '/leads/' + companyId
		});
	},
	unlock: function (leadId) {
		return api.put({
			uri:'/leads/unlock/' + leadId,
			body:{}
		});	
	},
	getLast: function(){
		var deferred = q.defer();
		var packages = sessionStorage.getItem('ppkg');
    	if(packages){
			deferred.resolve(JSON.parse(pending));
    	}else{
    		api.get({
				uri:'/leads/pending'
			}).then(function(res){
				sessionStorage.setItem('ppkg', res);
				deferred.resolve(res);
			}).catch(function(err){
				deferred.reject(err);
			});
    	}
		return deferred.promise;
	}
}

client.package = {
	getTemplates:function(){
		return api.get({
			uri:'/packages/templates'
		});	
	},
	buyPackage: function(templateId, params){
		return api.post({
			uri:'/packages/'+templateId,
			body:params
		});	
	},
	getAvailable: function(){
		return api.get({
			uri:'/packages/available'
		});	
	},
	getPackage: function(id){
		return api.get({
			uri:'/packages/' + id
		});
	},
	getPackages: function(id){
		return api.get({
			uri:'/packages'
		});
	},
	getMPButton: function(id){
		return api.get({
			uri:'/packages/mpbutton/' + id
		});
	},
	intentionPaymentMP: function(id){
		return api.put({
			uri:'/packages/mp/intention',
			body:{
				packageId:id
			}
		});	
	},
	cancel: function(id){
		return api.post({
			uri:'/packages/cancel/'+id,
			body:{}
		});	
	}
}

client.adventure = {
	getTopPlaces:function(){
		return api.get({
			uri:'/adventures/topPlaces'
		});
	},
	getTop:function(){
		return api.get({
			uri:'/adventures/top'
		});
	},
	getSimilars:function(id){
		return api.get({
			uri:'/adventures/similar/'+id
		});
	}
}

client.notification = {
	getUnread:function(){
		var deferred = q.defer();
		var notifications = sessionStorage.getItem('ntf');
    	if(notifications){
			deferred.resolve(JSON.parse(notifications));
    	}else{
    		api.get({
				uri:'/notifications/unread'
			}).then(function(res){
				if(res && res.length > 0){
					sessionStorage.setItem('ntf', JSON.stringify(res));
				}
				deferred.resolve(res);
			}).catch(function(err){
				deferred.reject(err);
			});
    	}
		return deferred.promise;
	}
}

client.notFoundPersons = {
	get:function(){
		return api.get({
			uri:'/helpers/missingperson'
		})
	}
}

module.exports = client;