var directives = require('../../scripts/llamawayDirectives');
var services = require('../../scripts/llamawayServices');
var llamaApi = require('../../helpers/api');
var llamaLocation = require('../../helpers/location/ng.module');
var geoLocation = require('../../helpers/location');
var utils = require('../../scripts/utils');
var q = require('q');

angular.module('newAdventure',[
	directives.name,
	services.name,
	llamaLocation.name,
	'angularTrix',
	'rzModule',
	'isteven-multi-select'
])

.controller('NewAdventureCtrl', ['$scope', '$timeout', 'gzzRoute', '$q', '$rootScope', function(
									$scope,
									$timeout,
									gzzRoute,
									$q,
									$rootScope){
	llamaApi.loadQ($q)

	var imageSpinner = document.querySelector('.imageSpinner');
	var logoImage = document.querySelector('.logoImage');

	$scope.ix = {
		existsPageName: false
	}

	$scope.$watch("data.adventure.withoutPrice" , function(newVal){
		$scope.currencySlider.options.disabled = newVal ? true : false;
		if(newVal !== undefined){
			switch(newVal){
				case 1:
					$scope.data.adventure.price = null;
					$scope.data.adventure.currency = null;
				break;
				case 2:
					$scope.data.adventure.price = 0;
					$scope.data.adventure.currency = null;
					$scope.data.adventure.payMethod = null;
				break;
			}
		}else{
			$scope.data.adventure.withoutPrice = 0;
		}
	});

	llamaApi.user.getCurrent().then(function(){
		llamaApi.company.getById($scope.user.companyId, true).then(function(company) {
			if(company.pageName){
				$scope.data.company = company;
				$scope.ix.existsPageName = true;
			}
		});
	}).catch(function(){
		gzzRoute.loginAndComeBack();
	});

	$scope.multiselect = {
		payMethods : [
		  {id: 1, name: 'Efectivo'},
		  {id: 2, name: 'Tarjeta de débito'},
		  {id: 3, name: 'Tarjeta de crédito'},
		  {id: 4, name: 'Mercadopago/Transferencias'}
		],
		localLang: {
	    	selectAll: "Tick all",
		    selectNone: "Tick none",
		    reset: "Undo all",
		    search: "Type here to search...",
		    nothingSelected : "Seleccioná una o varias"
	    }
	}
	//document.querySelector('.multiSelect button').textContent = '';

	$scope.takeDaysSlider = {
  		options: {
    		stepsArray: [
		    	{value: 0, legend: 'Horas'},
		      	{value: 1, legend: 'Días'}
		    ],
	    	showTicksValues: true,
	    	getPointerColor: function(value) {
            	return '#6A2871';
        	}
  		}
	}

	$scope.difficultySlider = {
  		options: {
    		stepsArray: [
		    	{value: 0, legend: 'Fácil'},
		      	{value: 1, legend: 'Normal'},
		      	{value: 2, legend: 'Difícil'},
		      	{value: 3, legend: 'Experto'}
		    ],
	    	showTicksValues: true,
	    	showSelectionBar: true,
	    	getPointerColor: function(value) {
            	return '#6A2871';
        	},
        	getSelectionBarColor: function(value) {
	            if (value == 1){
	                return 'green';
	            }
	            if (value == 2){
	                return 'orange';
	            }
	            if (value == 3){
	                return 'red';
	            }
	        }
  		}
	}

	$scope.currencySlider = {
  		options: {
    		stepsArray: [
		    	{value: 0, legend: 'Peso Argentino'},
		      	{value: 1, legend: 'Dólares'}
		    ],
	    	showTicksValues: true,
	    	getPointerColor: function(value) {
            	return '#6A2871';
        	}
  		}
	}

	$scope.priceSlider = {
  		options: {
    		stepsArray: [
		    	{value: 0, legend: 'Con precio'},
		      	{value: 1, legend: 'Sin Precio'},
		      	{value: 2, legend: 'Gratuita'}
		    ],
	    	showTicksValues: true,
	    	getPointerColor: function(value) {
            	return '#6A2871';
        	}
  		}
	}

	$scope.createAdventure = function(){
		$scope.ix.submitting = true;

		llamaApi.user.getCurrent().then(function(user){
			if(user){ // No estoy logueado
				$rootScope.$emit('toogleSpinner', true);

				$scope.data.isInvalidAdventureName = false;
				$scope.data.isInvalidPlaceName = false;
				$scope.isValidLocation = $scope.isAddressComponentsComplete($scope.golocation);
				$scope.$broadcast('validateAll', 'createAdventure' ,function(valid){
					if(valid && isValidAdventureName($scope.data.adventure.name) && isValidPlaceName($scope.data.adventure.place)){
						$scope.data.adventure._geoloc = $scope.golocation.location;
						$scope.data.adventure.golocation = $scope.golocation;

						if(window.lazyGoogleMap){
							lazyGoogleMap($scope.golocation.location);
						}

						if($scope.data.adventure.activityType && $scope.data.adventure.activityType.length){
							$scope.data.adventure.activityType = JSON.stringify($scope.data.adventure.activityType);
						}

						if($scope.data.adventure.payMethod && $scope.data.adventure.payMethod.length){
							$scope.data.adventure.payMethod = JSON.stringify($scope.data.adventure.payMethod);
						}

						updateCompany().then(function(){
							$scope.ix.existsPageName = true;

							var cleanAdventure = getParsedAdventure($scope.data.adventure);
							llamaApi.company.updateOrCreateAdventure(user.companyId, cleanAdventure).then(function(adv){
								
								sessionStorage.setItem("llama-av", $scope.data.adventure.name);
								sessionStorage.setItem('adv', 's');
								location.href = '/cargar-imagenes/'+adv.safeUrlName+'_'+adv._id;

							}).catch(function(err){
								$scope.ix.submitting = false;
							}).finally(function(){
								$rootScope.$emit('toogleSpinner', false);
							});
						}).catch(function(err){
							$scope.$broadcast('validateOne', function(){});
						});
					}else{
						$scope.$broadcast('validateOne', function(){});
						$rootScope.$emit('toogleSpinner', false);
						$scope.ix.submitting = false;
						if(window.device.desktop){
							document.querySelector(".errorToast").style.display = 'block';
							setTimeout(function(){
								$scope.closeErrorModal();
							}, 3000);
						}
					}
				});
			}
		});
	}

	var updateCompany = function(){
		var defered = q.defer();
		if($scope.ix.existsPageName){
			defered.resolve();
		}else{
			llamaApi.company.updatePage({
				name: $scope.data.company.name,
				pageName: $scope.data.company.pageName,
				description: $scope.data.company.description
			}).then(function(newCompany){
				defered.resolve();
			}).catch(function(){
				defered.reject();
			});
		}
		return defered.promise;
	}

	$scope.data = {
		validation:{},
		genericErrorMessage:'Completá este dato',
		isInvalidAdventureName: false,
		isInvalidPlaceName: false,
		adventure:{
		}
	}

	//Inicializo datos de los sliders:
	$scope.data.adventure.takeDays = 0;
	$scope.data.adventure.difficulty = 1;
	$scope.data.adventure.duration = 2;
	$scope.data.adventure.currency = 0;

	function isValidAdventureName(name){
		if(!name.match(/[^0-9a-zA-Z-áéíóúüñ&% \:\.\'\,]/gi)){
			$scope.data.isInvalidAdventureName = false;
			return true;
		} else {
			$scope.data.isInvalidAdventureName = true;
			return false;
		}
	}

	function isValidPlaceName(name){
		if(!name.match(/[^0-9a-zA-Z-áéíóúüñ&% \:\.\'\,]/gi)){
			$scope.data.isInvalidPlaceName = false;
			return true;
		} else {
			$scope.data.isInvalidPlaceName = true;
			return false;
		}
	}

	/*Trix*/
	$scope.trixChange= function(){
		console.log('Chequear que no haya links')
	}

	$scope.trixAttachmentAdd= function(e){
		console.log('Chequear que no haya archivos')
	}

	$scope.goToAdventure = function(){
		location.href = '/aventura/'+ $scope.data.createdAdventure.safeUrlName + '_' + $scope.data.createdAdventure._id;
	}

	/*************************************DESKTOP**********************************************/


	var lastSection;
	$scope.showEdit = function(section){
		lastSection = section;
		$scope.ix[section] = !$scope.ix[section];
		document.querySelector('.modal').style.display = 'block';
		$timeout(function() {
		    $scope.$broadcast('rzSliderForceRender');
		}, 100);
	}

	$scope.closeModal = function(){
		$scope.ix[lastSection] = false;
		$scope.ix.show ='create';
		document.querySelector('.modal').style.display = 'none';
	}

	$scope.saveData = function(){
		for (var key in $scope.data.adventure) {
			if(key == 'activityType'){
				addClassActivityType();

				$scope.urlImageSprite = 'https://res.cloudinary.com/llamaway/image/upload/v1473633444/adventure_types_detail_tdkqrs.png';

			}
			if(!Array.isArray($scope.data.adventure[key]) || Array.isArray($scope.data.adventure[key]) && $scope.data.adventure[key].length > 0){
				$scope.fake[key] = $scope.data.adventure[key];
			}
		}

		if($scope.golocation){
			$scope.fake.golocation.formatted_address = utils.getFormattedAddress($scope.golocation);
		}

		if($scope.fake.profile_picture.length > 0){
			if($scope.data.adventure.profile_picture && $scope.data.adventure.profile_picture.length > 0) {
				$scope.ix.imagesLoaded = $scope.data.adventure.profile_picture.length;
			}
			var swiper = new Swiper('.swiper-container', {
		        pagination: '.swiper-pagination',
		        nextButton: '.swiper-button-next',
		        prevButton: '.swiper-button-prev',
		        slidesPerView: 1,
		        paginationClickable: true,
		        centeredSlides:true,
		        lazyLoading:true,
		        observer: true
		    });
		}

		for (var key in $scope.data.company) {
			$scope.fake.company[key] = $scope.data.company[key];
		}

	    if(window.device && window.device.mobile){
			$scope.fake = utils.getDescriptionFromLookups($scope.fake);
		} else {
			$scope.fake = utils.getDescriptionFromLookupsDesktop($scope.fake);
		}

		$scope.closeModal();
	}
	var addClassActivityType = function(){
		for(var i=0;i< $scope.data.adventure.activityType.length; i++){
			switch($scope.data.adventure.activityType[i].id) {
    			case 1: //'Acúatica'
        			$scope.data.adventure.activityType[i].class = 'sprite-acuatico';
        		break;
			    case 2://'Avistaje de aves':
			        $scope.data.adventure.activityType[i].class = 'sprite-avistaje';
			    break;
    			case 3://'Cabalgatas':
        			$scope.data.adventure.activityType[i].class = 'sprite-cabalgata';
        		break;
			    case 4://'Camping':
			        $scope.data.adventure.activityType[i].class = 'sprite-camping';
			    break;
    			case 5://'Ciclismo':
        			$scope.data.adventure.activityType[i].class = 'sprite-ciclismo';
        		break;
			    case 6://'Deportes Extremos':
			        $scope.data.adventure.activityType[i].class = 'sprite-extremo';
			    break;
    			case 7://'Deportes Invernales':
        			$scope.data.adventure.activityType[i].class = 'sprite-invernal';
        		break;
			    case 8://'Expediciones/Travesías':
			        $scope.data.adventure.activityType[i].class = 'sprite-expedicion';
			    break;
			    case 9://'Montañismo y Trekking':
			        $scope.data.adventure.activityType[i].class = 'sprite-montanismo';
			    break;
    			case 10://'Pesca':
        			$scope.data.adventure.activityType[i].class = 'sprite-pesca';
        		break;
			    case 11://'Tours/Otros':
			        $scope.data.adventure.activityType[i].class = 'sprite-otros';
			    break;
			}
		}
	}

    if(window.device && window.device.desktop){
		$scope.fake = {
			profile_picture: [{url: 'https://res.cloudinary.com/llamaway/image/upload/v1473640550/Llamaway_Upload_Photo_g9ivry.jpg'}],
			name: 'La mejor aventura de la Argentina',
			golocation: {formatted_address:'Obelisco - Avenida 9 de Julio, Buenos Aires, Ciudad Autónoma de Buenos Aires'},
			activityType:[{
			    "name": "Acúatica",
			    "id": 1
			}],
	        "currency": 0,
			"price": 1234,
			"difficulty": 1,
			"duration": 2,
			"place": "Bahia Blanca",
			"takeDays": false,
			"place": "sdfsdfsdfsdf",
			"description": "<div><p><strong>Llamaway</strong> es el primer sitio destinado a los amantes de la aventura. En Llamaway podrán registrarse tanto viajeros, como guías y empresas. Nuestra misión es que cada vez más gente se anime a la aventura, y que cada vez sea más fácil para los guías encontrar a esa gente. Creemos que no necesitas viajar para vivir una aventura, con Llamaway vas a poder encontrar aventuras cerca de tu casa. Y si te vas de viaje, vas a poder encontrar que hay para hacer en ese lugar mucho más fácil y rápido.</p></div>",
			"name": "La mejor aventura de la Argentina",
			"payMethod": [{
		      "name": "Tarjeta de crédito",
		      "id": 3
		    }],
		    "company": {
				"name": "Nombre de su empresa",
		    	"description": "Aquí se mostrará la descripción de su empresa.",
		    },
		    "companyProfile_picture": "https://res.cloudinary.com/llamaway/image/upload/v1457909333/Llamaway_Avatar_01.jpg"
		}

    	if(window.device && window.device.mobile){
			$scope.fake = utils.getDescriptionFromLookups($scope.fake);
		} else {
			$scope.fake = utils.getDescriptionFromLookupsDesktop($scope.fake);
		}

		var activityTypes = $scope.data.adventure.activityType || $scope.fake.activityType;

		$scope.urlImageSprite = 'https://res.cloudinary.com/llamaway/image/upload/v1473633444/adventure_types_detail_tdkqrs.png';
		

	    var lazyGoogleMap = function (location){
	        if(location && location.lat){
	            var googleMapsTries = 0;
	            var activateGoogleMaps = setInterval(function(){
	                googleMapsTries++;
	                if(window.google && window.google.maps){
	                    var mapDiv = document.querySelector('.map');

	                    var map = new google.maps.Map(mapDiv, {
	                            center: {'lat': parseFloat(location.lat), 'lng': parseFloat(location.lng)},
	                            zoom: 14,
	                            disableDoubleClickZoom: false
	                    });

	                    var marker = new google.maps.Marker({
	                        position: {'lat': parseFloat(location.lat), 'lng': parseFloat(location.lng)},
	                        map: map
                        });

	                    clearInterval(activateGoogleMaps)
	                } else {
	                    if(googleMapsTries>3){
	                        clearInterval(activateGoogleMaps)
	                    }
	                }
	            }, 3000);
	        }
	    }

    	lazyGoogleMap($scope.fake.location);

		$scope.closeErrorModal = function(){
			document.querySelector('.errorToast').style.display = 'none';
		}

	    $scope.ix.show ='image';
    }

/********************************COMPANY*********************************/
	$scope.changeCompanyName = function(){
		if($scope.data.company.name){
			$scope.data.company.name = $scope.data.company.name.replace(/[^0-9a-zA-Z-áéíóúñ&% \:\.\'\,]/gi, '')
			$scope.data.company.pageName = $scope.data.company.name;
			$scope.changePageName();
		}
	}

	$scope.changePageName = function(){
		if($scope.data.company.pageName){
			var pageName = $scope.data.company.pageName.toLowerCase();
			pageName = pageName.replace(/[\b&% \.\'\,]/g, '-')
			.replace(/á/g, 'a')
			.replace(/é/g, 'e')
			.replace(/í/g, 'i')
			.replace(/ó/g, 'o')
			.replace(/ú/g, 'u')
			.replace(/ñ/g, 'n')
			.replace(/[^0-9a-z\-]/gi, '');

			while(pageName.match(/--/gi)){
				pageName = pageName.replace(/--/g, '-');
			}

			$scope.data.company.pageName = pageName;
		}
	}

	$scope.verifyPageName = function(){
		if($scope.data.company.pageName){
			llamaApi.company.verifyPageName($scope.data.company.pageName).then(function(res){
				if(res.exists == false){
					$scope.ix.existingPageName = false;
				}else{
					$scope.ix.existingPageName = true;
				}
			}).catch(function(){
				$scope.ix.existingPageName = true;
			});
		}
	}
	function isValidCompanyName(companyName){
		if(companyName && !companyName.match(/[^0-9a-zA-Z-áéíóúñ&% \:\.\'\,]/gi)){
			$scope.ix.isInvalidName = false;
			return true;
		} else {
			$scope.ix.isInvalidName = true;
			return false;
		}
	}

	function isValidPageName(pageName){
		if(pageName && !pageName.match(/[^0-9a-zA-Z-áéíóúñ&% \:\.\'\,]/gi) && pageName.length > 3){
				$scope.data.isInvalidPageName = false;
				return true;
		} else {
			$scope.data.isInvalidPageName = true;
			return false;
		}
	}

	function getParsedAdventure(adventure){
		var temp = {};
		temp = adventure;  

		if(temp.activityType && temp.activityType.length>0){
			for(var i=0;i<temp.activityType.length;i++){
				delete temp.activityType[i]['$$hashKey'];
				delete temp.activityType[i]['class'];  
			}
		}

		if(temp.payMethod && temp.payMethod.length>0){
			for(var i=0;i<temp.payMethod.length;i++){
				delete temp.payMethod[i]['$$hashKey'];
				delete temp.payMethod[i]['class'];  
			}
		}
		return temp;
	}

	window.s = $scope;
}]);