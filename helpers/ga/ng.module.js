var ga = require('./');

module.exports = angular.module('ga',[])
.directive('gaClick', function(){
    return {            
        restrict: "A",
        link: function (scope, element, attrs, ctrls) {
            element.on('click',function (argument) {
                var args = attrs['gaClick'].split(',');
                ga.event.apply(ga,args);
            });
        }
    };
})
.directive('gaSubmit', function(){
    return {            
        restrict: "A",
        link: function (scope, element, attrs, ctrls) {
            element.on('submit',function (argument) {
                var args = attrs['gaSubmit'].split(',');
                ga.event.apply(ga,args);
            });
        }
    };
})