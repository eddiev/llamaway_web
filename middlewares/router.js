var express = require('express');
var path = require('path');
var rootDir = path.dirname(require.main.filename);
var mainLayout = require(path.resolve(rootDir+'/apps/mainLayout'));
var router = express.Router();
var oneDay = 86400000;
var robots = require('robots.txt')
var sitemap = require('./sitemap');

router.get('/robots.txt',function(req, res, next) {
	if(req.hostname != 'www.llamaway.com'){
		var robotFunction = robots(path.resolve(rootDir+'/robotsStaging.txt'));
		robotFunction(req, res, next);
	} else {
		var robotFunction = robots(path.resolve(rootDir+'/robotsProd.txt'));
		robotFunction(req, res, next);
	}
});

router.get('/paquetes',function(req, res, next) {
	mainLayout.loadApp(req, res,'packages').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	});
});

router.get('/confirmarPaquete/:packageId',function(req, res, next) {
	mainLayout.loadApp(req, res,'packagesConfirm').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	});
});

router.get('/estadoDePaquete/:state',function(req, res, next) {
	mainLayout.loadApp(req, res,'callbackMercadoPago').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		console.log('Error al cargar app callbackMercadoPago');
		next(err);
	})
});

router.get('/e/:pageName',function(req, res, next) {
	mainLayout.loadApp(req, res,'profile').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	})
});

router.get('/',function(req, res, next) {
	var options = { hideHeader: true };
	mainLayout.loadApp(req,res,'home',options).then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	})
});

router.get('/llamashop',function(req, res, next) {
	mainLayout.loadApp(req,res,'llamashop').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	})
});

router.get('/ingreso',function(req, res, next) {
	mainLayout.loadApp(req,res,'signInUp').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	});
});

router.get('/preguntas-frecuentes',function(req, res, next) {
	mainLayout.loadApp(req,res,'faqs').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	});
});

router.get('/politica-privacidad',function(req, res, next) {
	mainLayout.loadApp(req,res,'privacyNotice').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	});
});

router.get('/terminos-condiciones',function(req, res, next) {
	mainLayout.loadApp(req,res,'terms').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	});
});

router.get('/mi-cuenta',function(req, res, next) {
	mainLayout.loadApp(req, res,'myAccount').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	});
});

router.get('/nueva-aventura',function(req, res, next) {
	mainLayout.loadApp(req,res,'newAdventure').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	});
});

router.get('/editar-aventura/*_:adventureId',function(req, res, next) {
	mainLayout.loadApp(req,res,'editAdventure').then(function(render){
		res.send(render.html);
	}).catch(function(err){
		next(err);
	});
});

router.get('/cargar-imagenes/*_:adventureId',function(req, res, next) {
	mainLayout.loadApp(req,res,'loadImages').then(function(render){
		res.send(render.html);
	}).catch(function(e){
		next(e);
	})
});

router.get('/loginCallback',function(req, res, next) {
	var app = require(path.resolve(rootDir+'/apps/loginCallback'));

	app.render(req,res).then(function(render){
		res.send(render.html);
	}).catch(function(e){
		next(e);
	});
});
router.get('/aventura/*_:adventureId/',function(req, res, next) {
	mainLayout.loadApp(req,res,'adventure').then(function(render){
		res.send(render.html);
	}).catch(function(e){
		next(e);
	})
});


/****************BUSCADOR*****************/
router.get('/buscar-aventura/:query?',function(req, res, next) {
	mainLayout.loadApp(req,res,'searchResult').then(function(render){
		res.send(render.html);
	}).catch(function(){
		next(e);
	})
});

router.get('/actividades/:activity',function(req, res, next) {
	req.query = {
		name: req.params.activity
	}
	mainLayout.loadApp(req,res,'searchResult').then(function(render){
		res.send(render.html);
	}).catch(function(e){
		next(e);
	})
});

router.get('/aventuras-en-:location',function(req, res, next) {
	mainLayout.loadApp(req,res,'searchResult').then(function(render){
		res.send(render.html);
	}).catch(function(e){
		next(e);
	})
});

router.get('/sitemap.xml', sitemap);

// static files
router.use(express.static(rootDir + '/public', { maxAge: oneDay }));

//404
router.use(function(req,res,next){
	show404(req,res,next);
});
//404
router.use(function(err, req,res,next){
	console.log("[!-- Error:");
	console.log(err);
	console.log("--]");
	show404(req,res,next);
});

var show404 = function(req,res,next){
	mainLayout.loadApp(req,res,'notFound').then(function(render){
		res.status(404).send(render.html);
	});
}

module.exports = router;