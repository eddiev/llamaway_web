var mustache = require('mustache');
var readFile = require('fs-readfile-promise');
var q = require('q');
var path = require('path');
var utils = require('../../scripts/utils');
var i18nObj = require('./i18n.json');
var api = require('../../helpers/api');

var resolvePictureProfilesURL = function(results){
	results.forEach(function(adv){
		adv.profile_picture = {
			url:utils.getAdventureProfileThumbnailSearch(adv, true)
		}
	}); 
}

exports.render = function(req,res){
	var query = req.query;

	if(req.query.pag){
		query.page = query.pag-1;
	}

	var templateFileName = 'desktop.template.html';
	if(req.device && req.device.isMobile){
		templateFileName = 'mobile.template.html';
	}
	
	var templatePath = path.resolve(__dirname, templateFileName);

	var promises = [readFile(templatePath)]

	return q.all(promises).then(function(responses){
		var template = responses[0];

		var i18n = utils.i18n(i18nObj, req.env.lang);

		var data = {
			text:i18n,
			query:JSON.stringify(query),
			activityName: req.params.activity
		}
		
		return {
			'html':mustache.render(template.toString(),data)
		};
	}).catch(function(err){
		console.log('Search Result error', err);
	})
		
}
