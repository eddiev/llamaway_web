var readFile = require('fs-readfile-promise');
var path = require('path');
var q = require('q');
var mustache = require('mustache');
var utils = require('../../scripts/utils');
var i18nObj = require('./i18n.json');


exports.render = function(req,res){
	var promises = []
	promises.push(readFile(path.resolve(__dirname,'./template.html')));
	promises.push(readFile(path.resolve(__dirname,'./signin.template.html')));

	var templateFileName = 'desktop.signup.template.html';
	if(req.device && req.device.isMobile){
		templateFileName = 'signup.template.html';
	}

	promises.push(readFile(path.resolve(__dirname,'./' + templateFileName)));
	promises.push(readFile(path.resolve(__dirname,'./recover.password.template.html')));
	promises.push(readFile(path.resolve(__dirname,'./existing.user.template.html')));
	promises.push(readFile(path.resolve(__dirname,'./validate.user.template.html')));

	return q.all(promises).then(function(responses){
		var mainTpl = responses[0].toString();
		var signinTpl = responses[1].toString();
		var signupTpl = responses[2].toString();
		var recoverPasswordTpl = responses[3].toString();
		var existingUserTpl = responses[4].toString();
		var validateUserTpl = responses[5].toString();

		var data = {
			text:utils.i18n(i18nObj)
		}

		var partials = {
			signinTpl:signinTpl,
			signupTpl:signupTpl,
			recoverPasswordTpl:recoverPasswordTpl,
			existingUserTpl:existingUserTpl,
			validateUserTpl:validateUserTpl
		}

		return {
			title:'Ingresar a Llamaway',
			html:mustache.render(mainTpl,data,partials)
		};
	});
}
