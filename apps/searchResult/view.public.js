var directives = require('../../scripts/llamawayDirectives');
var services = require('../../scripts/llamawayServices');
var llamaApi = require('../../helpers/api');
var ga = require('../../helpers/ga');
var utils = require('../../scripts/utils');

angular.module('searchResult',[
	services.name,
	directives.name,
	'ngRoute',
	'rzModule'
])
.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when("/:section",{})
	.otherwise( { redirectTo: "/" });
	$locationProvider.hashPrefix('!'); //crawling
})
.controller('SearchResultCtrl', function($scope,$rootScope,$localStorage,$route,
										$q, $timeout){

	llamaApi.loadQ($q)

	$scope.ix = {
		hasResults: false,
		showWithoutResults: false,
		viewListMode:false,
		viewFilter: false,
		showRedirect: false,
		showWithoutResultsTopAdventures: false
	};

	$scope.$on('$routeChangeSuccess', function (ev, current, prev) {
		parseURL();
	});

	var parseURL = function(){
		var url = location.hash;
		var changeURL = false;
		var pathname =  window.location.pathname;

		if(url.indexOf('pag') > -1){
			var pageIndex = url.indexOf('pag') +4;
			if(url.indexOf('&') > -1){
				$scope.pages.currentPage = Number(url.substr(pageIndex,  url.indexOf('&')-pageIndex));
			}else{
				$scope.pages.currentPage = Number(url.substr(pageIndex,  url.length));
			}
		}else{
			$scope.pages.currentPage = 1;
		}

		$scope.activityFilter = [];
		$scope.difficultyFilter = [];
		$scope.locationFilter = [];
		$scope.durationFilter = '';
		$scope.priceFilter = '';
		$scope.regionFilter = '';
		$scope.searchType = 'query';
		$scope.topPlacesFilter = null;

		var indexGeoloc = window.location.hash.indexOf('geoloc');
		if(indexGeoloc > -1){
			$scope.searchType = 'geoloc';
		}

		switch($scope.searchType){
			case 'geoloc':
				var subUrl = url.substring(indexGeoloc, url.length);
				var geolocFilter;
				if(subUrl.indexOf('--') > -1){
					geolocFilter = subUrl.substring(0, subUrl.indexOf('--'));
				}else{
					geolocFilter = subUrl;
				}

				geolocFilter = geolocFilter.split('_');
				if(geolocFilter.length == 3){
					var lat = geolocFilter[1];
					var lng = geolocFilter[2].indexOf('--') > -1 ? geolocFilter[2].substr(0, geolocFilter[2].indexOf('--')) : geolocFilter[2];
					$scope.geoloc =  lat+','+ lng;
				}
			break;
			case 'query':
			break;
		}

		if(url.indexOf('filtros')>-1){
			if(url.indexOf('filtros-')>-1){
				var lastQuery = url.indexOf('--')>-1 ? url.indexOf('--') : url.length;
				var firstIndex = url.indexOf('filtros-')+8;
				$scope.name= decodeURI(url.substr(firstIndex, lastQuery-firstIndex));
			}
			if(url.indexOf('--')>-1){
				var sarasa = url.split('--');
				for (var i = 1; i < sarasa.length; i++) {
					var filters = sarasa[i].split('-');
						switch(filters[0]){
							case 'actividad':
							case 'activity':
								if(filters.length > 1){
									for (var j = 1; j < filters.length; j++) {
										$scope.activityFilter.push(utils.getActivityByAttr('name', filters[j]).id);
									};
								}else{
									url = url.replace("--"+filters[0], "");
									changeURL = true;
								}
							break;
							case 'provincia':
							case 'state':
								if(filters.length > 1){
									for (var e = 1; e < filters.length; e++) {
										$scope.locationFilter.push(decodeURI(filters[e]));
									};
								}else{
									url = url.replace("--"+filters[0], "");
									changeURL = true;
								}
							break;
							case 'dificultad':
							case 'difficulty':
								if(filters.length > 1){
									for (var j = 1; j < filters.length; j++) {
										var dificulty = utils.getDifficulty('name', filters[j]);
										$scope.difficultyFilter.push(dificulty.id);
									};
								}else{
									url = url.replace("--"+filters[0], "");
									changeURL = true;
								}
							break;
							case 'duracion':
							case 'duration':
								var takeDays = filters[1] == 'horas' ? 0 : 1;
								var min = filters[2] ? filters[2] : 1;
								var max = filters[3];
								if(!max){
									if(takeDays){
										max = 30;
									}else{
										max = 24;
									}
								}
								$scope.durationFilter = takeDays +','+ min +','+ max;
							break;
							case 'precio':
							case 'price':
								$scope.priceFilter = filters[1];
								if(filters.length > 2){
									$scope.priceFilter += ','+ filters[2] +',';
									$scope.priceFilter += filters[3] ? filters[3] : 1;
									$scope.priceFilter += ','+ filters[4];
								}
							break;
							case 'top_lugares':
							case 'top_places':
								$scope.topPlacesFilter = 'top_places';
							break;
							case 'distance':
							case 'distancia':
								$scope.ix.geolocMore = true;
							break;
							case 'region':
								if(filters.length > 0){
									var region = utils.getRegionByAttr('name',filters[1]);
									$scope.regionFilter = region;
									$scope.searchType = 'region';
								}else{
									url = url.replace("--"+filters[0], "");
									changeURL = true;
								}
							break;
						}
				};
			}else{
				if(url.indexOf("filtros-") == -1){
					changeURL = true;
					url = url.replace("filtros", "");
				}
			}
		}
		if(changeURL){
			changeURL = false;
			location.hash = url;
		}else{
	    	$scope.goSearch();
		}

		if($scope.ix.showWithoutResultsTopAdventures && location.hash.indexOf('top_lugares') == -1){
			$scope.ix.showWithoutResultsTopAdventures = false;
		}

		generateKeyword();
	}

	var generateKeyword = function(){
		$scope.breadcrumbs = [];
		var hash = location.hash;
		var filters = hash.split('--');
		filters.forEach(function(current){
			if(current.indexOf('actividad') > -1){
				$scope.activityFilter.forEach(function(currentFilter){
					var activity = utils.getActivityByAttr('id', currentFilter);
					var url = location.hash;
					if(url.indexOf(activity.name)>-1){
						url = location.pathname + url.substr(0, url.indexOf(activity.name) + activity.name.length);
					}else{
						url = location.pathname + '#filtros--provincia-' + activity.name;
					}
					var breadcrumb = {
						label: activity.label,
						url: url
					}
					$scope.breadcrumbs.push(breadcrumb);
				});
			}
			if(current.indexOf('provincia') > -1){
				$scope.locationFilter.forEach(function(currentFilter){
					var locationFilter = encodeURI(currentFilter);
					var url = location.hash;
					if(url.indexOf(locationFilter)>-1){
						url = location.pathname + url.substr(0, url.indexOf(locationFilter) + locationFilter.length);
					}else{
						url = location.pathname + '#filtros--provincia-' + locationFilter;
					}
					var breadcrumb = {
						label: currentFilter,
						url: url
					}
					$scope.breadcrumbs.push(breadcrumb);
				});
			}
			if(current.indexOf('dificultad') > -1){
				$scope.difficultyFilter.forEach(function(currentFilter){
					var difficulty = utils.getDifficulty('id', currentFilter);
					var url = location.hash;
					if(url.indexOf(difficulty.name)>-1){
						url = location.pathname + url.substr(0, url.indexOf(difficulty.name)+difficulty.name.length);
					}else{
						url = location.pathname + '#filtros--dificultad-' + difficulty.name;
					}
					var breadcrumb = {
						label: difficulty.label,
						url: url
					}
					$scope.breadcrumbs.push(breadcrumb);
				});
			}
			if(current.indexOf('duracion') > -1 && $scope.durationFilter){
				var value = $scope.durationFilter.split(',');
				var takeDays = value[0] == 0 ? 'horas' : 'días';
				var durationName = 'duracion-' + takeDays +'-'+ value[1] +'-'+ value[2];
				var url = location.hash;
				if(url.indexOf(durationName)>-1){
					url = location.pathname + url.substr(0, url.indexOf(durationName)+durationName.length);
				}else{
					url = location.pathname + '#filtros--' + durationName;
				}
				var breadcrumb = {
					label: value[1] +' a '+ value[2] +' '+ takeDays,
					url: url
				}
				$scope.breadcrumbs.push(breadcrumb);
			}
			if(current.indexOf('precio') > -1 && $scope.priceFilter){
				var value = $scope.priceFilter.split(',');
				var priceLabel = value[0] == 'conprecio' ? 'Precio de '+ value[2] +' a '+ value[3] +' '+ value[1] : 'Gratuita';
				var priceName = 'precio-' + value[0] +'-'+ value[1] +'-'+ value[2] +'-'+ value[3];
				var url = location.hash;
				if(url.indexOf(priceName)>-1){
					url = location.pathname + url.substr(0, url.indexOf(priceName)+priceName.length);
				}else{
					url = location.pathname + '#filtros--'+priceName;
				}
				var breadcrumb = {
					label: priceLabel,
					url: url
				}
				$scope.breadcrumbs.push(breadcrumb);
			}
		});
	}

	$scope.searchQuery = function(){
		var url = location.hash;
		if(url.indexOf('filtros') > -1){
			var filtroIndex = url.indexOf('filtros');
			var suburl;
			var subur2;
			if(url.indexOf('filtros-') > -1){
				suburl =  url.substr(0, filtroIndex +7);
				if($scope.name && $scope.name.length == 0){
					suburl += '';
				}
				if(url.indexOf('--') > -1){
					subur2 =  url.substr(url.indexOf('--'), url.length);
				}else{
					subur2 = '';
				}
			}else{
				suburl =  url.substr(0, filtroIndex +7) + '-';
				subur2 =  url.substr(filtroIndex+7, url.length);
			}
			url = suburl + $scope.name + subur2;
		}else{
			url = 'filtros-' + $scope.name;
		}
		 location.hash = url;
	}

	$scope.goSearch = function(page) {
		var page = $scope.pages.currentPage -1;
        var query = {
            limit: 30,
            page: page || 0
        }
        var appliedAnyFilter = false;

		query.name = $scope.name || '';

        switch($scope.searchType){
        	case 'geoloc':
        		if($scope.ix.geolocMore){
        			query.radius = 40000;
        		}else{
        			query.radius = 20000;
        		}
        		query.geoloc = $scope.geoloc;
        	break;
        }


        if($scope.activityFilter && $scope.activityFilter.length > 0){
            query.activity = $scope.activityFilter.join();
            appliedAnyFilter = true;
        }

        if($scope.locationFilter && $scope.locationFilter.length > 0){
            query.location = $scope.locationFilter.join();
            appliedAnyFilter = true;
        }

        if($scope.difficultyFilter && $scope.difficultyFilter.length > 0){
            query.difficulty = $scope.difficultyFilter.join();
            appliedAnyFilter = true;
        }

        if($scope.durationFilter){
            query.duration = $scope.durationFilter;
            appliedAnyFilter = true;
        }

        if($scope.priceFilter){
            query.price = $scope.priceFilter;
            appliedAnyFilter = true;
        }

        if($scope.topPlacesFilter){
            query.top_places = 1;
            appliedAnyFilter = true;
        }

        if($scope.regionFilter){
            query.location = $scope.regionFilter.provinces.join();
            appliedAnyFilter = true;
        }

        //Traiga la cantidad justa de información para el resultado de búsqueda.
        query.withoutDescription = true;

		$rootScope.$emit('toogleSpinner', true);
        window.scrollTo(0,0);
        $scope.pages.numberPages = 0;
		$scope.pages.currentPage = 1;
		$scope.pages.itemsForPage = 0;
		$scope.ix.showWithoutResults = false;

		$scope.data.adventures = {}

        llamaApi.company.searchAdventure(query).then(function(adventures) {
			if(adventures.count > 0){
				adventures.facets.selectedFacets = {
					activity: $scope.activityFilter,
					location: $scope.locationFilter,
					difficulty: $scope.difficultyFilter,
					region: $scope.regionFilter
				}


				$scope.data.adventures = adventures;

				$scope.pages.numberPages =  $scope.data.adventures.nbPages;
				$scope.pages.currentPage =  $scope.data.adventures.page + 1;
				$scope.pages.itemsForPage = $scope.data.adventures.hitsPerPage;

				addClassActivityType();

				$scope.ix.hasResults = true;

				///ssponsor ASTA
				if($scope.regionFilter){
					var exists = $scope.regionFilter.name == 'cuyo';
					if(exists){
						if($scope.data.adventures.results && $scope.data.adventures.results.length>0){ //Region de cuyo
							$scope.data.adventures.results.splice(2, 0, getSponsoredItem());
						}
					}
				}

				var title = "Buscar aventuras | Llamaway";
				var description = "Disfrutá todas las aventuras que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
				if(query.name){
					title = "Buscar aventuras con " + query.name + " | Llamaway";
					description = "Disfrutá todas las aventuras de "+ query.name+" que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";	
				}

				if(location.hash){
					if(location.hash.indexOf("acuaticas") != -1){
						title = "Buscar aventuras acuáticas | Llamaway";
						description = "Disfrutá todas las aventuras acuáticas que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} else if(location.hash.indexOf("avistajes") != -1){
						title = "Buscar aventuras de avistajes | Llamaway";
						description = "Disfrutá todas las aventuras de avistajes que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} else if(location.hash.indexOf("cabalgatas") != -1){
						title = "Buscar aventuras de cabalgatas | Llamaway";
						description = "Disfrutá todas las aventuras de cabalgatas que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} else if(location.hash.indexOf("camping") != -1){
						title = "Buscar aventuras de camping | Llamaway";
						description = "Disfrutá todas las aventuras de camping que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} else if(location.hash.indexOf("ciclismo") != -1){
						title = "Buscar aventuras de ciclismo | Llamaway";
						description = "Disfrutá todas las aventuras de ciclismo que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} else if(location.hash.indexOf("deportes_extremos") != -1){
						title = "Buscar aventuras de deportes extremos | Llamaway";
						description = "Disfrutá todas las aventuras de deportes extremos que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} else if(location.hash.indexOf("deportes_invernales") != -1){
						title = "Buscar aventuras de deportes invernales | Llamaway";
						description = "Disfrutá todas las aventuras de deportes invernales que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} else if(location.hash.indexOf("expediciones_travesias") != -1){
						title = "Buscar aventuras de expediciones y travesías | Llamaway";
						description = "Disfrutá todas las aventuras de expediciones y travesías que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} else if(location.hash.indexOf("trekking") != -1){
						title = "Buscar aventuras de montañismo y trekking | Llamaway";
						description = "Disfrutá todas las aventuras de montañismo y trekking que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} else if(location.hash.indexOf("pesca") != -1){
						title = "Buscar aventuras de pesca | Llamaway";
						description = "Disfrutá todas las aventuras de pesca que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} else if(location.hash.indexOf("otros") != -1){
						title = "Buscar aventuras de tours u otros | Llamaway";
						description = "Disfrutá todas las aventuras de tours u otros que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					} 
				}

				switch($scope.searchType){
					case 'region':
						if($scope.regionFilter){
							title = "Aventuras en "+ $scope.regionFilter.label +" | Llamaway";
							description = "Disfrutá todas las aventuras en "+ $scope.regionFilter.label +" que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
						}
					break;
					case 'geoloc':
						title = "Aventuras cerca tuyo | Llamaway";
						description = "Disfrutá todas las aventuras cerca tuyo que te ofrecemos para planificar y desatar tu espiritu aventurero al máximo!";
					break;
				}
				utils.changeSEO(title, description);
				//Traigo imágenes transformadas en menor o mejor calidad. Dependiendo del dispositivo
	            if(window.innerWidth <= 800){
	                $scope.data.adventures.results = utils.getProfilePicturesTransformations(adventures.results, true);
	            } else {
	                $scope.data.adventures.results = utils.getProfilePicturesTransformations(adventures.results, false);
	            }

	            if(appliedAnyFilter){
		            const similarProps = ['_id', 'activityType','companyId', 'difficulty', 'duration', 'price', 'golocation',
		            					  'currency', 'location','name','profile_picture','safeUrlName','takeDays']
		            const similarAdventures = $scope.data.adventures.results.reduce((similarArray, item) => {
		            	if(!item.sponsored){
		            		const similar = {};
			            	similarProps.forEach(function(prop){
			            		similar[prop] = item[prop];
			            	})
			            	similarArray.push(similar);
		            	}
		            	return similarArray;
		            }, []);
		            sessionStorage.setItem('similarAdventures', JSON.stringify(similarAdventures));
	            }else{
	            	sessionStorage.removeItem('similarAdventures');
	            }

			}else{
				if($scope.data.adventures && $scope.data.adventures.count > 0){
					$scope.data.adventures.results = [];
					$scope.data.adventures.count = 0;
					$scope.data.adventures.facets = adventures.facets;
				}
				$scope.ix.hasResults = false;
				$scope.ix.showWithoutResults = true;

				if($scope.searchType == 'query'){
	                $scope.ix.showWithoutResultsTopAdventures = true;

		            $scope.activityFilter = [];
		            $scope.locationFilter = [];
		            $scope.difficultyFilter = [];
		            $scope.durationFilter = "";
		            $scope.priceFilter = "";
		            $scope.name = '';
		            $scope.regionFilter = '';
	                window.location.hash = "#filtros--top_lugares";
				}

				if($scope.searchType == 'geoloc'){
	                $scope.ix.geolocMore = true;
	                window.location.hash = "#filtros--geoloc_"+$scope.geoloc.replace(',', '_')+"--distancia-400";
				}
			}

            $rootScope.$emit('toogleSpinner', false);
        });
    }

	$scope.ix.geolocMore = false;

//*********************************Paginado****************************************//


	$scope.pages = {
		currentPage: 1,
		numberPages: 0,
		itemsForPage: 20
	}

	$scope.navigatePage = function(direction){
		var page = $scope.pages.currentPage + (1 * direction);
		ga.event('User', 'SearchResult', 'page', $scope.pages.currentPage);
		if(location.hash.indexOf('pag') > -1){
			location.hash = location.hash.replace(/pag=\d/, 'pag='+page);
		}else{
			if(window.location.hash.length > 2){
				location.hash = '#pag='+ page + '&' + location.hash.substr(2);
			}else{
				location.hash +=  'pag='+ page;
			}
		}
	}

	function addClassActivityType(){
		var tempAventures = $scope.data.adventures.results;
		var activity;
		for(var j=0;j<tempAventures.length; j++){
			for(var i=0;i<tempAventures[j].activityType.length; i++){
				activity = utils.getActivityByAttr('id', tempAventures[j].activityType[i].id)
				tempAventures[j].activityType[i].class = activity.class;
			}
		}
		
	    setTimeout(function() {
            var allActivities = document.querySelectorAll('.activity-sprite');
            for(var i=0; i<allActivities.length; i++){
                allActivities[i].style.backgroundImage = "url('https://res.cloudinary.com/llamaway/image/upload/v1475123550/adventure_types_uhc2su.png')";
            }
        }, 500);
	}

	function getSponsoredItem(){
		var astaItem = {
			'sponsored':true,
			'name': 'Asociación San Juan Turismo Activo',
			'location':{
				'address': 'San Juan, Argentina'
			},
			'imageBorder':true,
			'externalLinkDesktop':'http://turismoactivosanjuan.org',
			'externalLinkMobile':'https://play.google.com/store/apps/details?id=aserto.app.asta',
			'image':'https://res.cloudinary.com/llamaway/image/upload/v1475127664/astaHQ_pebdnn.png'
		}

		return astaItem;
	}

	$scope.showSponsorRedirect = function(link){
		$scope.ix.showRedirect = true;
		$scope.externalLink = link;
	}

	$scope.acceptSponsorRedirect = function(){
		ga.event('User', 'sponsorRedirect', 'Accept');
		$scope.ix.showRedirect = false;
	}

	$scope.cancelSponsorRedirect = function(){
		ga.event('User', 'sponsorRedirect', 'Close');
		$scope.ix.showRedirect = false;
	}

	$scope.showMobileFilters = function(show){
		document.querySelector('.top-nav').style.display = show?'none':'block';
		document.querySelector('.header-container').style.display = show?'none':'flex';
        document.querySelector('.adventures-container').style.display = show?'none':'block';
        document.querySelector('.pages').style.display = show?'none':'block';

        document.querySelector('.filter-container').style.display = show?'block':'none';
	}

	$scope.clearSearch = function(){
		window.location.hash = window.location.hash.replace(/--top_places|--top_lugares/, '');
	}

	window.s = $scope;
});