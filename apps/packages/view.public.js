var llamaApi = require('../../helpers/api');
var ga = require('../../helpers/ga');

angular.module('packages',[]).controller('PackageCtrl', function($scope, $window){
	
	$scope.data = {};
	$scope.ix = {
		section:'hire'
	};

    $scope.adventureName = sessionStorage.getItem("llama-av");
    sessionStorage.removeItem("llama-av");
    $scope.adventureState = sessionStorage.getItem('adv');
    sessionStorage.removeItem("adv");

    var flag = sessionStorage.getItem('slpkg');
    if(flag){
    	$scope.flag = flag;
    }

 	llamaApi.user.getCurrent().then(function(user){
 		if(!user.companyId){
 			location.href = "/";	
 		}
 	}).catch(function(){
 		location.href = "/";
 	});

 	llamaApi.package.getAvailable().then(function(availablePackage){
 		$scope.data.package = availablePackage;
 		$scope.$apply();
 	});

	$scope.contratar = function(buyPackage){
		ga.event('User', 'Packages', "Confirm");
		llamaApi.package.buyPackage(buyPackage._id, {time:0}).then(function(newPackage){
			window.location.href = "/confirmarPaquete/" + newPackage._id;
		});
	}

	$scope.notNow = function(){
		ga.event('User', 'Packages', "Cancel");
		if($scope.adventureState){
			$scope.ix.section = 'thanks';
			$scope.ix.thanks = 'adventure';
		}else{
			location.href = 'mi-cuenta#/mis-aventuras';
		}
	}

	ga.event('User', 'Packages', "Show");

	window.s = $scope;
});