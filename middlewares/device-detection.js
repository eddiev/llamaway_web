var MobileDetect = require('mobile-detect');
var path = require('path');
var appDir = path.dirname(require.main.filename);

module.exports = function(req,res,next){
	var md = new MobileDetect(req.headers['user-agent']);
	
	req.device = {
		isMobile: false
	}

	if(md.mobile()){
		req.device = { 
			isMobile:true,
			name:md.mobile(),
		};
	}else{
		req.device = { 
			isMobile:false,
			name:'desktop',
		};
	}

	next();
}