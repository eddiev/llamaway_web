var merge = require('merge');
var $q = require('q');

var listActivityType = [
  {id: 1, name: 'acuaticas', label: 'Acúaticas', class:'sprite-acuatico'},
  {id: 2, name: 'avistajes', label: 'Avistajes', class:'sprite-avistaje'},
  {id: 3, name: 'cabalgatas', label: 'Cabalgatas', class:'sprite-cabalgata'},
  {id: 4, name: 'camping', label: 'Camping', class:'sprite-camping'},
  {id: 5, name: 'ciclismo', label: 'Ciclismo', class:'sprite-ciclismo'},
  {id: 6, name: 'deportes_extremos', label: 'Deportes Extremos', class:'sprite-extremo'},
  {id: 7, name: 'deportes_invernales', label: 'Deportes Invernales', class:'sprite-invernal'},
  {id: 8, name: 'expediciones_travesias', label: 'Expediciones/Travesías', class:'sprite-expedicion'},
  {id: 9, name: 'montañismo_trekking', label: 'Montañismo y Trekking', class:'sprite-montanismo'},
  {id: 10, name: 'pesca', label: 'Pesca', class:'sprite-pesca'},
  {id: 11, name: 'otros', label: 'Tours/Otros', class:'sprite-otros'}
];

exports.getActivityByAttr = function(attr, value){
	var activity;
	listActivityType.some(function(current){
		if(current[attr] == decodeURI(value)){
			activity = current
			return true;
		}
		return false;
	});
	return activity;
}

var listRegion = [
  {id: 1, name: 'patagonia', label: 'Patagonia', provinces: ['Tierra del fuego', 'Santa Cruz', 'Chubut', 'Río Negro', 'Neuquén']},
  {id: 2, name: 'norte', label: 'Norte', provinces:['Tucumán', 'Salta', 'Jujuy', 'Santiago del Estero', 'Catamarca']},
  {id: 3, name: 'cuyo', label: 'Cuyo', provinces:['La Rioja', 'San Juan', 'Mendoza', 'San Luis']},
  {id: 4, name: 'litoral', label: 'Litoral', provinces:['Misiónes', 'Corrientes',  'Entre Ríos', 'Santa Fe', 'Chaco']},
  {id: 5, name: 'centro', label: 'Centro', provinces:['Córdoba']},
  {id: 6, name: 'buenos_aires', label: 'Buenos Aires', provinces:['Buenos Aires']},
];

exports.getRegionByAttr = function(attr, value){
	var region;
	listRegion.some(function(current){
		if(current[attr] == decodeURI(value)){
			region = current
			return true;
		}
		return false;
	});
	return region;
}

exports.getRegionByProvince = function(province){
	var region;
	listRegion.some(function(currentRegion){
		var exists = currentRegion.provinces.some(function(currentProvince){
			if(currentProvince == decodeURI(province)){
				region = currentRegion;
				return true;
			}
			return false;
		});
		return exists;
	});
	return region;
}

function getLlamita(){
	var llamita = Math.floor((Math.random() * 3) + 1);
	switch(llamita) {
    case 1:
        return 'https://res.cloudinary.com/llamaway/image/upload/v1474339752/Llamaway_Avatar_01_tspbd2.jpg'
        break;
    case 2:
        return 'https://res.cloudinary.com/llamaway/image/upload/v1474339752/Llamaway_Avatar_02_xulw7s.jpg'
        break;
    default:
        return 'https://res.cloudinary.com/llamaway/image/upload/v1474339752/Llamaway_Avatar_03_bxmloa.jpg'
	} 
}

function getLlamitaSearchResult(){
	var llamita = Math.floor((Math.random() * 3) + 1);
	switch(llamita) {
    case 1:
        return 'https://res.cloudinary.com/llamaway/image/upload/v1457909333/Llamaway_Avatar_01.jpg'
        break;
    case 2:
        return 'https://res.cloudinary.com/llamaway/image/upload/v1457909333/Llamaway_Avatar_02.jpg'
        break;
    default:
        return 'https://res.cloudinary.com/llamaway/image/upload/v1457909334/Llamaway_Avatar_03.jpg'
	} 
}

exports.i18n = function(obj, language, country) {
    if (obj) {
        return merge(obj[language], obj[country] || {});
    } else {
        return {};
    }

}

exports.cloudinaryParams = function(url,params){
	// Por si tengo corrupta la url de la imagen.
	if(params && url){
		url = url.substr(0, url.lastIndexOf('.')) + '.jpeg'; //Ojo si no tengo extension!
		return url.replace('/upload/','/upload/'+params+'/');
	}
}

exports.getProfilePicturesTransformations = function(adventures, isMobile){
	var scale;
	if(isMobile){
		scale = 'c_scale,q_auto:eco,w_400';
	} else {
		scale = 'c_scale,q_auto:eco,w_850';
	}
	if(adventures && adventures.length > 0){
		//Listado o búsqueda
		for(var i=0;i<adventures.length; i++){
			if(adventures[i].profile_picture){
				if(adventures[i].profile_picture[0]){
					adventures[i].profile_picture[0].url = this.cloudinaryParams(adventures[i].profile_picture[0].url,'c_scale,q_auto:eco,w_400')
				} else {
					if(adventures[i].profile_picture.url){
						adventures[i].profile_picture.url = this.cloudinaryParams(adventures[i].profile_picture.url,'c_scale,q_auto:eco,w_400')
					}else{
						adventures[i].profile_picture = {
							url: this.cloudinaryParams(getLlamita(), scale)
						}
					}
				}
			}
		}
	} else {
		//Detalle de una aventura
		if(adventures.profile_picture){
			for(var i=0;i<adventures.profile_picture.length; i++){
				adventures.profile_picture[i].url = this.cloudinaryParams(adventures.profile_picture[i].url, scale)
			}
		}
	}
	return adventures;
}

exports.getLeadsTransformationsAdventure = function(adventure){
	var scale = 'c_scale,q_auto:eco,w_400';

	//Listado o búsqueda
	if(adventure.profile_picture){
		if(adventure.profile_picture[0]){
			adventure.profile_picture[0].url = this.cloudinaryParams(adventure.profile_picture[0].url,'c_scale,q_auto:eco,w_400')
		} else {
			if(adventure.profile_picture.url){
				adventure.profile_picture.url = this.cloudinaryParams(adventure.profile_picture.url,'c_scale,q_auto:eco,w_400')
			}else{
				adventure.profile_picture = {
					url: this.cloudinaryParams(getLlamitaSearchResult(), scale)
				}
			}
		}
	}
	return adventure;
}

exports.getLeadsTransformationsCompany = function(company){
	var scale = 'c_scale,q_auto:eco,w_200';

	//Listado o búsqueda
	if(company.profile_picture){
		if(company.profile_picture[0]){
			company.profile_picture[0].url = this.cloudinaryParams(company.profile_picture[0].url,'c_scale,q_auto:eco,w_200')
		} else {
			if(company.profile_picture.url){
				company.profile_picture.url = this.cloudinaryParams(company.profile_picture.url,'c_scale,q_auto:eco,w_200')
			}else{
				company.profile_picture = {
					url: this.cloudinaryParams(getLlamitaSearchResult(), scale)
				}
			}
		}
	}
	return company;
}

exports.getLeadsTransformationsAdventures = function(company){
	var scale = 'c_scale,q_auto:eco,w_400';

	//Listado o búsqueda
	if(company.profile_picture){
		if(company.profile_picture[0]){
			company.profile_picture[0].url = this.cloudinaryParams(company.profile_picture[0].url,'c_scale,q_auto:eco,w_400')
		} else {
			if(company.profile_picture.url){
				company.profile_picture.url = this.cloudinaryParams(company.profile_picture.url,'c_scale,q_auto:eco,w_400')
			}else{
				company.profile_picture = {
					url: this.cloudinaryParams(getLlamitaSearchResult(), scale)
				}
			}
		}
	}
	return company;
}

exports.getCompanyProfileThumbnail = function(company, hasManyPictures){
	// Por ahora es una foto. Ver como resolver el array
	var url = '';
	if(hasManyPictures){
		if(company.profile_picture && company.profile_picture.length > 0){
			url = company.profile_picture[0].url;
		} else {
			url = getLlamita();
		}
	} else {
		if(company.profile_picture){
			url = company.profile_picture.url;
		} else {
			url = getLlamita();
		}
	}

	return url;										
}

exports.getAdventureProfileThumbnailSearch = function(adv, hasManyPictures){
	// Por ahora es una foto. Ver como resolver el array
	var url = '';
	if(hasManyPictures){
		if(adv.profile_picture && adv.profile_picture.length > 0){
			url = adv.profile_picture[0].url;
		} else {
			url = getLlamita();
		}
	} else {
		if(adv.profile_picture){
			url = adv.profile_picture.url;
		} else {
			url = getLlamita();
		}
	}

	return url;										
}

exports.getCompanyProfileThumbnailSearch = function(company, hasManyPictures){
	// Por ahora es una foto. Ver como resolver el array
	var url = '';
	if(hasManyPictures){
		if(company.profile_picture && company.profile_picture.length > 0){
			url = company.profile_picture[0].url;
		} else {
			url = getLlamitaSearchResult();
		}
	} else {
		if(company.profile_picture){
			url = company.profile_picture.url;
		} else {
			url = getLlamitaSearchResult();
		}
	}

	return url;										
}

exports.getDescriptionFromLookups = function(adventure, isMobile){
	if(adventure.duration != undefined && adventure.duration != ""){
		switch(adventure.duration) {
		    case 0:
		        adventure.duration = '1';
		        break;
		    case 1:
		        adventure.duration = '2';
		        break;
		    case 2:
		        adventure.duration = '3';
		        break;
		    case 3:
		        adventure.duration = '6';
		        break;
		    case 4:
		        adventure.duration = '8';
		        break;
		    case 5:
		        adventure.duration = '12';
		        break;
		    case 6:
		        adventure.duration = '18';
		        break;
		    case 7:
		        adventure.duration = '24';
		        break;
		}

		if(adventure.takeDays){
			adventure.duration += ' días';
		} else {
			adventure.duration += ' horas';
		}
	}

	if(adventure.difficulty != undefined && adventure.duration != ""){
		switch(adventure.difficulty) {
		    case 0:
		        adventure.difficulty = 'Fácil';
		        break;
		    case 1:
		        adventure.difficulty = 'Normal';
		        break;
		    case 2:
		        adventure.difficulty = 'Difícil';
		        break;
		    case 3:
		        adventure.difficulty = 'Experto';
		        break;
		}
	}

	if(adventure.currency != undefined && adventure.duration != ""){
		switch(adventure.currency) {
		    case 0:
		        adventure.currency = 0;
		        break;
		    case 1:
		        adventure.currency = 1;
		        break;
		}
	}

	return adventure;
}

var difficultyList = [{
	id: 0,
	name:'facil',
	label: 'Fácil'
},{
	id: 1,
	name:'normal',
	label: 'Normal'
},{
	id: 2,
	name:'dificil',
	label: 'Difícil'
},{
	id: 3,
	name:'experto',
	label: 'Experto'
}]

exports.getDifficulty = function(attr, id){
	var difficulty; 
	difficultyList.some(function(current){
		if(id == current[attr]){
			difficulty = current;
			return true; 
		}
		return false;
	});
	return difficulty;
}
exports.getDescriptionFromLookupsDesktop = function(adventure){
	if(adventure.duration != undefined && adventure.duration != ""){
		if(adventure.takeDays){
			adventure.duration += ' días';
		} else {
			adventure.duration += ' horas';
		}
	}

	if(adventure.difficulty != undefined && adventure.duration != ""){
		switch(adventure.difficulty) {
		    case 0:
		        adventure.difficulty = 'Fácil';
		        break;
		    case 1:
		        adventure.difficulty = 'Normal';
		        break;
		    case 2:
		        adventure.difficulty = 'Difícil';
		        break;
		    case 3:
		        adventure.difficulty = 'Experto';
		        break;
		}
	}

	if(adventure.currency != undefined && adventure.duration != ""){
		switch(adventure.currency) {
		    case 0:
		        adventure.currency = 0;
		        break;
		    case 1:
		        adventure.currency = 1;
		        break;
		}
	}

	return adventure;
}

exports.changeSEO = function(title, metaDescription){
    document.querySelector('title').textContent = title;
    document.querySelector('[name="description"]').content = metaDescription;
    document.querySelector('[property="og:title"]').textContent = title;
    document.querySelector('[property="og:description"]').content = metaDescription;
}

exports.getProvincesForRegion = function(region){
    var regionObj = this.getRegionByAttr('name', region);
    if(regionObj){
    	return regionObj.provinces;
    }else{
    	return;
    }
}

exports.getFormattedAddress = function (golocation) {
    var loc = '';

    if (golocation && golocation.address_components !== undefined) {
        if ((golocation.address_components.hasOwnProperty("administrative_area_level_1") === true) && 
           (golocation.address_components.hasOwnProperty("administrative_area_level_2") === true)) {
 
            if(golocation.address_components.locality){
                loc += golocation.address_components.locality + ', ';
            }
 
            if(golocation.address_components.administrative_area_level_2){
                loc += golocation.address_components.administrative_area_level_2 + ', ';
            }

            if(golocation.address_components.administrative_area_level_1){
                loc += golocation.address_components.administrative_area_level_1;
            }

            if (golocation.address_components.hasOwnProperty("country") === true) {
                loc += ', ' + golocation.address_components.country;
            }

            return loc;
        }
    }
    
    return loc;
}

