require('newrelic');

var express = require('express');
var cookieParser = require('cookie-parser');
var compression = require('compression')
var https = require('https');
var fs = require('fs');
var argv = require('minimist')(process.argv.slice(2));

var router = require('./middlewares/router');
var language = require('./middlewares/language');
var deviceDetection = require('./middlewares/device-detection');
var expressUtils = require('./middlewares/express-utils');

var api = require('./helpers/api');

var app = express();
var port = process.env.PORT || 8081;
var httpsPort = 3009;

app.set('trust proxy',true);

api.host = (argv.devAPI && 'http://localhost:3042') || (argv.dev && 'https://llamaway-api-staging.herokuapp.com') || api.host;

console.log('\napi.host',api.host);

var fingerprint = JSON.parse(fs.readFileSync('fingerprint.json', "utf8")).fingerprint || 0;

if(argv.dev){
	app.use(require('connect-livereload')());
}else{
	app.use(compression());
}


var gtmId = process.env.GOOGLE_TAG_MANAGER_ID || 'GTM-PNPNDJ';
var googleAnalyticsId = process.env.GOOGLE_ANALYTICS_ID || '';

//conf
app.use(function(req,res,next){
	req.env = {
		name:argv.dev?'development':'production',
		production:argv.dev?false:true,
		development:argv.dev?true:false,
		devAPI:argv.devAPI||false,
		api_host:api.host,
		finger_print: fingerprint,
		url: 'www.llamaway.com',
		gtmId:gtmId,
		googleAnalyticsId:googleAnalyticsId
	}
	next();
});

app.use(require('prerender-node').set('prerenderToken', 'BdC8Ppo3Xul5wy5PCHJE'));
app.use(cookieParser());
app.use(deviceDetection);
app.use(expressUtils);

app.use(language);
app.use(router);

app.listen(port,function () {
	console.log('\n');
	console.log('running on  http://localhost:' + port);
	console.log('running on  https://localhost:' + httpsPort);
	console.log('\n');
});
