var directives = require('../../scripts/llamawayDirectives');
var services = require('../../scripts/llamawayServices');
var llamaApi = require('../../helpers/api');
var llamaLocation = require('../../helpers/location/ng.module');
var geoLocation = require('../../helpers/location');
var utils = require('../../scripts/utils');

angular.module('editAdventure',[
	directives.name,
	services.name,
	llamaLocation.name,
	'angularTrix',
	'ngRoute',
	'isteven-multi-select',
	'rzModule'
])
.config(function($routeProvider) {

	$routeProvider
	.when("/:section",{})
	.otherwise( { redirectTo: "/mis-datos" });

})
.controller('EditAdventureCtrl', function($scope,
							          $timeout,
							          $route,
									  $location,
									  $window,
									  $routeParams,
									  $rootScope,
									  gzzRoute,
									  $q){

	llamaApi.loadQ($q)

	var imageSpinner = document.querySelector('.imageSpinner');
	var logoImage = document.querySelector('.logoImage');

	$scope.data = {
		validation:{},
		genericErrorMessage:'Completá este dato',
		isInvalidAdventureName: false,
		isInvalidPlaceName: false,
		adventure:{

		}
	}

	$scope.multiselect = {
		listActivityType : [
		  {id: 1, name: 'Acúatica'},
		  {id: 2, name: 'Avistaje de aves'},
		  {id: 3, name: 'Cabalgatas'},
		  {id: 4, name: 'Camping'},
		  {id: 5, name: 'Ciclismo'},
		  {id: 6, name: 'Deportes Extremos'},
		  {id: 7, name: 'Deportes Invernales'},
		  {id: 8, name: 'Expediciones/Travesías'},
		  {id: 9, name: 'Montañismo y Trekking'},
		  {id: 10, name: 'Pesca'},
		  {id: 11, name: 'Tours/Otros'}
		],
		payMethods : [
		  {id: 1, name: 'Efectivo'},
		  {id: 2, name: 'Tarjeta de débito'},
		  {id: 3, name: 'Tarjeta de crédito'},
		  {id: 4, name: 'Mercadopago/Transferencias'}
		],
		localLang: {
	    	selectAll: "Tick all",
		    selectNone: "Tick none",
		    reset: "Undo all",
		    search: "Type here to search...",
		    nothingSelected : "Seleccioná una o varias"
	    }
	}

	$scope.takeDaysSlider = {
  		options: {
    		stepsArray: [
		    	{value: 0, legend: 'Horas'},
		      	{value: 1, legend: 'Días'}
		    ],
	    	showTicksValues: true,
	    	getPointerColor: function(value) {
            	return '#6A2871';
        	}
  		}
	}

	$scope.difficultySlider = {
  		options: {
    		stepsArray: [
		    	{value: 0, legend: 'Fácil'},
		      	{value: 1, legend: 'Normal'},
		      	{value: 2, legend: 'Difícil'},
		      	{value: 3, legend: 'Experto'}
		    ],
	    	showTicksValues: true,
	    	showSelectionBar: true,
	    	getPointerColor: function(value) {
            	return '#6A2871';
        	},
        	getSelectionBarColor: function(value) {
	            if (value == 1){
	                return 'green';
	            }
	            if (value == 2){
	                return 'orange';
	            }
	            if (value == 3){
	                return 'red';
	            }
	        }
  		}
	}

	$scope.currencySlider = {
  		options: {
    		stepsArray: [
		    	{value: 0, legend: 'Peso Argentino'},
		      	{value: 1, legend: 'Dólares'}
		    ],
	    	showTicksValues: true,
	    	getPointerColor: function(value) {
            	return '#6A2871';
        	}
  		}
	}

	$scope.priceSlider = {
  		options: {
    		stepsArray: [
		    	{value: 0, legend: 'Con precio'},
		      	{value: 1, legend: 'Sin Precio'},
		      	{value: 2, legend: 'Gratuita'}
		    ],
	    	showTicksValues: true,
	    	getPointerColor: function(value) {
            	return '#6A2871';
        	}
  		}
	}

	var init = {
		'editar-aventura': function() {
			$rootScope.$emit('toogleSpinner', true);

			checkSession().then(function(user) {
				if(user) {
					llamaApi.company.getAdventureById($scope.data.adventure._id).then(function (adventure) {

						$scope.data.adventure = adventure;
						$scope.autocomplete = adventure.golocation.formatted_address;
						$scope.golocation = adventure.golocation;

						$scope.data.adventure.activityType.forEach(function(item) {
						    $scope.multiselect.listActivityType.forEach(function(itemList){
						    	if(itemList.id == item.id){
						    		itemList.check = true;
						    		return true;
						    	}
						    	return false;
						    });
						});

						if($scope.data.adventure.payMethod){
							$scope.data.adventure.payMethod.forEach(function(item) {
							    $scope.multiselect.payMethods.forEach(function(itemList){
							    	if(itemList.id == item.id){
							    		itemList.check = true;
							    		return true;
							    	}
							    	return false;
							    });
							});
						}

						if($scope.data.adventure.price || $scope.data.adventure.price == 0){
							if($scope.data.adventure.price == 0){
								$scope.data.adventure.withoutPrice = 2;
							}else{
								$scope.data.adventure.withoutPrice = 0;
							}
						}else{
							$scope.data.adventure.withoutPrice = 1;
						}

						$scope.data.profile_picture_url = $scope.data.adventure.profile_picture.url;
						
					});
				}
			}).catch(function() {
				gzzRoute.loginAndComeBack();
			}).finally(function(){
				$rootScope.$emit('toogleSpinner', false);
			});
		}
	}


	$scope.editAdventure = function(){
		$scope.ix.submitting = true;

		checkSession().then(function(user){
			if(user){ // No estoy logueado
				$scope.data.isInvalidAdventureName = false;
				$scope.data.isInvalidPlaceName = false;
				$scope.isValidLocation = $scope.isAddressComponentsComplete($scope.golocation);
				$scope.$broadcast('validateAll', 'editAdventure' ,function(valid){
					if(valid && isValidAdventureName($scope.data.adventure.name) && isValidPlaceName($scope.data.adventure.place)){
						$scope.data.adventure._geoloc = $scope.golocation.location;
						$scope.data.adventure.golocation = $scope.golocation;
						if(window.lazyGoogleMap){
							lazyGoogleMap($scope.golocation.location);
						}
						llamaApi.company.updateOrCreateAdventure(user.companyId, $scope.data.adventure).then(function(adv){
							$scope.ix.show = 'thanks';
							$scope.ix.submitting = false;
						}).catch(function(err){
							console.log(err);
							$scope.ix.submitting = false;
						});
					}else{
						$scope.$broadcast('validateOne', function(){});
						$scope.ix.submitting = false;
					}
				});
			}
		});
	}

	$scope.$watch("data.adventure.withoutPrice" , function(newVal){
		$scope.currencySlider.options.disabled = newVal ? true : false;
		if(newVal !== undefined){
			switch(newVal){
				case 1:
					$scope.data.adventure.price = null;
					$scope.data.adventure.currency = null;
				break;
				case 2:
					$scope.data.adventure.price = 0;
					$scope.data.adventure.currency = null;
					$scope.data.adventure.payMethod = null;
				break;
			}
		}else{
			$scope.data.adventure.withoutPrice = 0;
		}
	});

	//Inicializo datos de los sliders:
	$scope.data.adventure.takeDays = 0;
	$scope.data.adventure.difficulty = 1;
	$scope.data.adventure.duration = 2;
	$scope.data.adventure.currency = 0;

	var checkSession = function(){
		return llamaApi.user.getCurrent().catch(function(){
			return false;
		});
	}

	function isValidAdventureName(name){
		if(!name.match(/[^0-9a-zA-Z-áéíóúüñ&% \:\.\'\,]/gi)){
			$scope.data.isInvalidAdventureName = false;
			return true;
		} else {
			$scope.data.isInvalidAdventureName = true;
			return false;
		}
	}

	function isValidPlaceName(name){
		if(!name.match(/[^0-9a-zA-Z-áéíóúüñ&% \:\.\'\,]/gi)){
			$scope.data.isInvalidPlaceName = false;
			return true;
		} else {
			$scope.data.isInvalidPlaceName = true;
			return false;
		}
	}

	/*Trix*/
	$scope.trixChange= function(){
		console.log('Chequear que no haya links')
	}

	$scope.trixAttachmentAdd= function(e){
		console.log('Chequear que no haya archivos')
	}

	$scope.goToAdventure = function(){
		location.href = '/aventura/'+ $scope.data.adventure.safeUrlName + '_' + $scope.data.adventure._id;
	}

	$scope.reloadApp = function(){
		location.reload();
	}

	$scope.$on('$routeChangeSuccess', function (ev, current, prev) {
		checkSession().then(function(user){
			if(user){
				$scope.data.userLoaded=true;
				init['editar-aventura']();
			} else {
				$scope.data.userLoaded=false;
				gzzRoute.loginAndComeBack();
			}
		});

	});

    $scope.finish = function(){
		location.href = "/paquetes";
    }

    if(window.device && window.device.desktop){
    	$scope.$watch('data.adventure', function(newVal){
    		if(newVal && newVal.name){
				$scope.viewAdventure = {}
    			updateViewAdventure();
    		}
    	});

		var updateViewAdventure = function(){
			for(var key in $scope.data.adventure){
				$scope.viewAdventure[key] = $scope.data.adventure[key];
			}

		    if(window.device && window.device.mobile){
				$scope.viewAdventure = utils.getDescriptionFromLookups($scope.viewAdventure);
			} else {
				$scope.viewAdventure = utils.getDescriptionFromLookupsDesktop($scope.viewAdventure);
			}

			$scope.formatted_address = utils.getFormattedAddress($scope.golocation);

			if($scope.viewAdventure.profile_picture.length == 0){
				$scope.viewAdventure.profile_picture = [{'url': utils.getAdventureProfileThumbnailSearch($scope.viewAdventure, true)}];
				var swiper = new Swiper('.swiper-container', {
			        pagination: '.swiper-pagination',
			        nextButton: '.swiper-button-next',
			        prevButton: '.swiper-button-prev',
			        slidesPerView: 1,
			        paginationClickable: false,
			        centeredSlides:true,
			        lazyLoading:true
			    });
			}else{
    			if(!$scope.viewAdventure.profile_picture || $scope.viewAdventure.profile_picture.length == 1){
					document.querySelector(".swiper-pagination").style.display = 'none';
					document.querySelector(".swiper-button-next").style.display = 'none';
					document.querySelector(".swiper-button-prev").style.display = 'none';
				}
				var swiper = new Swiper('.swiper-container', {
			        pagination: '.swiper-pagination',
			        nextButton: '.swiper-button-next',
			        prevButton: '.swiper-button-prev',
			        slidesPerView: 1,
			        paginationClickable: true,
			        centeredSlides:true,
			        lazyLoading:true,
			        observer: true
			    });
			}
			lazyGoogleMap($scope.viewAdventure.golocation.location);
			addClassActivityType();
		}

    	$scope.saveData = function(){
			updateViewAdventure();
			document.querySelector(".errorToast").style.display = 'none';
			checkSession().then(function(user){
				if(user){ // No estoy logueado
					$scope.data.isInvalidAdventureName = false;
					$scope.data.isInvalidPlaceName = false;
					$scope.isValidLocation = $scope.isAddressComponentsComplete($scope.golocation);
					$scope.$broadcast('validateAll', 'editAdventure' ,function(valid){
						if(valid && isValidAdventureName($scope.data.adventure.name) && isValidPlaceName($scope.data.adventure.place)){
							$scope.data.adventure._geoloc = $scope.golocation.location;
							$scope.data.adventure.golocation = $scope.golocation;
							lazyGoogleMap($scope.golocation.location);
							llamaApi.company.updateOrCreateAdventure(user.companyId, $scope.data.adventure).then(function(adv){
							});
						}else{
							document.querySelector(".errorToast").style.display = 'block';
							setTimeout(function(){
								$scope.closeErrorModal();
							}, 3000);
						}
					});
				}
			});
			$scope.closeModal();
		}

    	var lastSection;
		$scope.showEdit = function(section){

			//Tengo que redibujar para que me tome los datos del modelo
			$timeout(function () {
        		$scope.$broadcast('rzSliderForceRender');
    		}, 100);

			lastSection = section;
			$scope.ix[section] = !$scope.ix[section];
			document.querySelector('.modal').style.display = 'block';
		}

		$scope.closeModal = function(){
			$scope.ix[lastSection] = false;
			$scope.ix.show ='create';
			document.querySelector('.modal').style.display = 'none';
		}

		$scope.closeErrorModal = function(){
			document.querySelector('.errorToast').style.display = 'none';
		}

		function lazyGoogleMap(location){
	        if(location && location.lat){
	            var googleMapsTries = 0;
	            var activateGoogleMaps = setInterval(function(){
	                googleMapsTries++;
	                if(window.google && window.google.maps){
	                    var mapDiv = document.querySelector('.map');

	                    var map = new google.maps.Map(mapDiv, {
	                            center: {'lat': parseFloat(location.lat), 'lng': parseFloat(location.lng)},
	                            zoom: 14
	                    });

	                    var marker = new google.maps.Marker({
	                        position: {'lat': parseFloat(location.lat), 'lng': parseFloat(location.lng)},
	                        map: map
	                        });


	                    clearInterval(activateGoogleMaps)
	                } else {
	                    if(googleMapsTries>3){
	                        clearInterval(activateGoogleMaps)
	                    }
	                }
	            }, 3000);
	        }
	    }
    }

    $scope.goToLoadImages = function(){
    	location.href = '/cargar-imagenes/'+$scope.data.adventure.safeUrlName+'_'+$scope.data.adventure._id;
    }

	var addClassActivityType = function(){
		for(var i=0;i< $scope.viewAdventure.activityType.length; i++){
			switch($scope.viewAdventure.activityType[i].id) {
    			case 1: //'Acúatica'
        			$scope.viewAdventure.activityType[i].class = 'sprite-acuatico';
        		break;
			    case 2://'Avistaje de aves':
			        $scope.viewAdventure.activityType[i].class = 'sprite-avistaje';
			    break;
    			case 3://'Cabalgatas':
        			$scope.viewAdventure.activityType[i].class = 'sprite-cabalgata';
        		break;
			    case 4://'Camping':
			        $scope.viewAdventure.activityType[i].class = 'sprite-camping';
			    break;
    			case 5://'Ciclismo':
        			$scope.viewAdventure.activityType[i].class = 'sprite-ciclismo';
        		break;
			    case 6://'Deportes Extremos':
			        $scope.viewAdventure.activityType[i].class = 'sprite-extremo';
			    break;
    			case 7://'Deportes Invernales':
        			$scope.viewAdventure.activityType[i].class = 'sprite-invernal';
        		break;
			    case 8://'Expediciones/Travesías':
			        $scope.viewAdventure.activityType[i].class = 'sprite-expedicion';
			    break;
			    case 9://'Montañismo y Trekking':
			        $scope.viewAdventure.activityType[i].class = 'sprite-montanismo';
			    break;
    			case 10://'Pesca':
        			$scope.viewAdventure.activityType[i].class = 'sprite-pesca';
        		break;
			    case 11://'Tours/Otros':
			        $scope.viewAdventure.activityType[i].class = 'sprite-otros';
			    break;
			}
		}

		$scope.urlImageSprite = 'https://res.cloudinary.com/llamaway/image/upload/v1473633444/adventure_types_detail_tdkqrs.png';
		
	}

	window.s = $scope;
});