var directives = require('../../scripts/llamawayDirectives');
var llamaApi = require('../../helpers/api');
var autocompleteModule = require('../../helpers/autocomplete/ng.module');
var cookie = require('cookies-js');
var q = require('q');
var ga = require('../../helpers/ga');
var llamaLocation = require('../../helpers/location/ng.module');
var utils = require('../../scripts/utils');

angular.module('home', [
        directives.name,
        autocompleteModule.name])
    .controller('HomeCtrl', function($scope, 
        $rootScope,
        $timeout,
        gzzRoute,
        $q) {

        llamaApi.loadQ($q)

        var searchTimeOut;

        $scope.ix = {};
        $scope.ix.isCompany = false;

        llamaApi.user.getCurrent().then(function(user) {
            if (user.companyId) {
                $scope.ix.isCompany = true;
            } else {
                $scope.ix.isCompany = false;
            }

            var userSignedUp = localStorage.getItem('reg');
            switch(userSignedUp) {
                case 'emp':
                    ga.event('User', 'CreateOrganization', user.companyId);
                    localStorage.removeItem('reg');
                    break;
                case 'empProvider':
                    ga.event('User', 'CreateOrganizationProvider', user.companyId);
                    localStorage.removeItem('reg');
                    break;
                case 'user':
                    ga.event('User', 'CreateLlamita', user._id);
                    localStorage.removeItem('reg');
                    break;
                case 'userProvider':
                    ga.event('User', 'CreateLlamitaProvider', user._id);
                    localStorage.removeItem('reg');
                    break;
            } 

            $scope.ix.userLoaded = true;
        }).catch(function() {
            $scope.ix.userLoaded = false;
        }).finally(function() {});

        $scope.myLocation = function() {
            $scope.data.searchQuery = 'mi location';
            goSearch();
        }
        var goSearch = function() {
            var query = $scope.data.searchQuery.trim();
            ga.event('User', 'Search', query);
            gzzRoute.search(query);
        }
        $scope.submit = function(e) {
            e.preventDefault();
            goSearch();
        }

        //Hay que resolver por api y por provincia. Hay que dedicarle más tiempo
        $scope.searchRegion = function(region){
            var regionQuery = '';
            switch(region) {
                case 'patagonia':
                    regionQuery = 'tierra fuego cruz chubut negro neuquen';
                break;
                case 'norte':
                    regionQuery = 'tucuman salta jujuy estero catamarca';
                break;
                //OJO CON ESTE!!! Harcodeado el item de ASTA en la vista de searchResult
                case 'cuyo':
                    regionQuery = 'rioja juan mendoza luis';
                break;   
                case 'litoral':
                    regionQuery = 'misiones corrientes entre rios fe chaco';
                break;   
                case 'centro':
                    regionQuery = 'cordoba';
                break;   
                case 'buenos_aires':
                    regionQuery = 'buenos aires';
                break;   
            }

            ga.event('User', 'Search', regionQuery);
            gzzRoute.searchRegion(region);
        }

        $scope.onSelect = function(item) {
            ga.event('User', 'SelectSearchItem', item.name);
            $scope.__dontWatch = true;
            $scope.data.searchQuery = item.name;
            location.href = item.link;
        }

        $scope.$watch('data.searchQuery', function(newVal) {
            if ($scope.__dontWatch === false) {
                if (newVal && newVal.length > 1) {
                    var query = {
                        name: newVal,
                        limit: 5
                    }
                    q.all([llamaApi.company.searchAdventure(query)
                    ]).then(function(responses) {
                        (function() {
                            var list = [];
                            if (responses[0].results.length > 0) {
                                responses[0].results.forEach(function(current) {
                                    current.link = "/aventura/" + current.safeUrlName + "_" + current._id;
                                });
                                list.push({
                                    label: 'Aventuras',
                                    elements: responses[0].results
                                });
                            }
                            $scope.instantSearchResult = list;
                            $scope.$apply();
                        })();
                    });
                } else {
                    $scope.instantSearchResult = [];
                }
            }
            $scope.__dontWatch = false;
        });

        $scope.data = {
            genericErrorMessage: 'Completá este dato',
            searchQuery: '',
            adventure: {
                _id: "5744bfc9f890732808f8038b",
                ownerId: "5733ca835b6192e81287cfbd",
                companyId: "5733ca845b6192e81287cfc0",
                difficulty: 2,
                duration: 1,
                takeDays: true,
                place: "rocanrolen",
                name: "Cabalgatennnnn",
                profile_picture: {
                    url: "https://res.cloudinary.com/dhp5jgxth/image/upload/v1465774719/adventure_profile_pictures/k6equnwscrnsei71vwvk.png",
                    imageId: "5744c025f890732808f8038e",
                    _id: '5744c025f890732808f8038f'
                },
                location: {
                    address: "Buenos Aires, CABA, Argentina",
                    coords: {
                        lat: "-34.6036844",
                        lng: "-58.381559100000004"
                    }
                },
                currency: 0,
                price: 463634
            }
        }

        llamaApi.adventure.getTopPlaces().then(function(places){
            places.forEach(function(current){
                current.location.address = current.location.address.replace(/\,\s/g, '_').replace(/\s/g, '-');
            });
            $scope.data.topPlaces = places;
            llamaApi.adventure.getTop().then(function(adventures){
                //Traigo imágenes transformadas en menor o mejor calidad. Dependiendo del dispositivo
                if(window.innerWidth <= 800){
                    adventures = utils.getProfilePicturesTransformations(adventures, true);
                } else {
                    adventures = utils.getProfilePicturesTransformations(adventures, false);
                }

                addClassActivityType(adventures);
                $scope.data.topAdventures = adventures;
            });
        });

        function addClassActivityType(adv){
            var tempAventures = adv;
            for(var j=0;j<tempAventures.length; j++){
                for(var i=0;i<tempAventures[j].activityType.length; i++){
                    switch(tempAventures[j].activityType[i].id) {
                        case 1: //'Acúatica'
                            tempAventures[j].activityType[i].class = 'sprite-acuatico';
                        break;
                        case 2://'Avistaje de aves':
                            tempAventures[j].activityType[i].class = 'sprite-avistaje';
                        break;
                        case 3://'Cabalgatas':
                            tempAventures[j].activityType[i].class = 'sprite-cabalgata';
                        break;
                        case 4://'Camping':
                            tempAventures[j].activityType[i].class = 'sprite-camping';
                        break;
                        case 5://'Ciclismo':
                            tempAventures[j].activityType[i].class = 'sprite-ciclismo';
                        break;
                        case 6://'Deportes Extremos':
                            tempAventures[j].activityType[i].class = 'sprite-extremo';
                        break;
                        case 7://'Deportes Invernales':
                            tempAventures[j].activityType[i].class = 'sprite-invernal';
                        break;
                        case 8://'Expediciones/Travesías':
                            tempAventures[j].activityType[i].class = 'sprite-expedicion';
                        break;
                        case 9://'Montañismo y Trekking':
                            tempAventures[j].activityType[i].class = 'sprite-montanismo';
                        break;
                        case 10://'Pesca':
                            tempAventures[j].activityType[i].class = 'sprite-pesca';
                        break;
                        case 11://'Tours/Otros':
                            tempAventures[j].activityType[i].class = 'sprite-otros';
                        break;
                    }
                }
            }

            setTimeout(function() {
                var allActivities = document.querySelectorAll('.activity-sprite');
                for(var i=0; i<allActivities.length; i++){
                    allActivities[i].style.backgroundImage = "url('https://res.cloudinary.com/llamaway/image/upload/v1475123550/adventure_types_uhc2su.png')";
                }
               
            }, 500);
        }

        setCanonicalUrl(window.location.origin);
        function setCanonicalUrl (url){
            $rootScope.$emit('setCanonicalUrl', url);
        }
        window.s = $scope;
    });
