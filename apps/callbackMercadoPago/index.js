var mustache = require('mustache');
var readFile = require('fs-readfile-promise');
var path = require('path');
var utils = require('../../scripts/utils');
var i18nObj = require('./i18n.json');

exports.render = function(req, res) {

    //https://www.success.com/?collection_id=2110833&collection_status=approved&preference_id=96347226-182e5cec-a594-45b7-9425-bd94cebd27dc&external_reference=581c78248a1e130003c32d56&payment_type=credit_card&merchant_order_id=null

    var query = req.query;

    var templateFileName = 'desktop.template.html';
    if (req.device && req.device.isMobile) {
        templateFileName = 'mobile.template.html';
    }

    var templatePath = path.resolve(__dirname, templateFileName);

    return readFile(templatePath).then(function(buffer) {
        var data = {
            'text': utils.i18n(i18nObj),
            'packageId': query.external_reference
        }

        return {
            'title': 'Llamaway - Animate a la aventura - Encontra tu aventura',
            'metadescription': 'El sitio web de Turismo Aventura donde vas a poder encontrar experiencias y actividades para hacer en todo el país',
            html: mustache.render(buffer.toString(), data)
        }
    });
}