var directives = require('../../scripts/llamawayDirectives');
require('../../public/libs/ngstorage/ngStorage');

var llamaApi = require('../../helpers/api');
var store = require('store2');
var gaModule = require('../../helpers/ga/ng.module');
var ga = require('../../helpers/ga');
var utils = require('../../scripts/utils');

angular.module('adventure',[
	'ngStorage',
	'ngRoute',
	directives.name
])
.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	$routeProvider
	.when("/:section/:id?/:action?/:actionId?",{})

	$locationProvider.hashPrefix('!'); //crawling
}])
.controller('AdventureCtrl', function($scope,
										$timeout,
										$localStorage,
										$route,
										$routeParams,
										$rootScope,
										$window,
										gzzRoute,
										$q){

	llamaApi.loadQ($q)

	$scope.ix = {
		showform: false,
		showNearbyAdventures: false
	}

	

	$scope.data = {
		genericErrorMessage:'Completá este dato',
		invalidFormat: 'Ingresá un email válido',
		info: {},
		contact: {},
		company_formatted_address: ''
	}

	$scope.user = {}

	$scope.$watch('data.adventure', function(newVal){
		if(newVal){
			if(!newVal.profile_picture || newVal.profile_picture.length == 1){
				document.querySelector(".swiper-pagination").style.display = 'none';
				document.querySelector(".swiper-button-next").style.display = 'none';
				document.querySelector(".swiper-button-prev").style.display = 'none';
			}
			llamaApi.company.getById(newVal.companyId).then(function(company) {
				if (company) {
					var url = utils.getCompanyProfileThumbnailSearch(company, false);
					if(!company.profile_picture){
						company.profile_picture = {}
					}
					company.profile_picture.url = url;
					$scope.data.info.company = company;
					$scope.data.company_formatted_address = utils.getFormattedAddress(company.golocation);
				}
			});
			setCanonicalUrl(window.location.origin + '/aventura/'+ $scope.data.adventure.safeUrlName + '_' + $scope.data.adventure._id);

			similarAdventures();

			lazyGoogleMap();
		}
	});

	$scope.showContactForm = function(){
		$scope.description = '';
		$scope.ix.showform = true;
		ga.event('User', 'ContactFormAdventure', 'Show');
	}

	$scope.cancelContactForm = function(){
		$scope.ix.showform = false;
		ga.event('User', 'ContactFormAdventure', 'Close');
	}

	$scope.sendContactForm = function(){
		$scope.ix.submitting = 'info';
		$scope.$broadcast('validateAll', 'info' ,function(valid) {
			if(valid) {
				checkSession().then(function(user) {
				  if (user === false) {
						// El usuario no existe, lo creo y envío el email de notificación
						llamaApi.user.signupAndLogin({
							'email': $scope.user.email,
							'password': '',
						}).then(function(newUser) {
							$scope.user = newUser;
							ga.event('User', 'CreateLlamitaAdventure', newUser._id);
							SendContactAndCreateLead();
						}).catch(function (err) {
							$rootScope.$emit('toogleSpinner', false);
							if(err.statusCode == 409){
								// Redirigo al login
								goToLogin();
							}
							endedSubmit();
						});
					} else {
						SendContactAndCreateLead();
					}
				});
			} else {
				endedSubmit();
			}
		});
	}

	$scope.goToAdventure = function(){
		location.href = '/aventura/'+ $scope.data.createdAdventure.safeUrlName + '_' + $scope.data.createdAdventure._id;
	}

	$scope.goToHome = function(){
		location.href = '/';
	}

	$scope.goToNearbyAdventures = function(){
		const adventureTypeList = [];
		let administrativeAreaLevel1 = '';
		
		if($scope.data.adventure.golocation && $scope.data.adventure.golocation.address_components){
			administrativeAreaLevel1 = $scope.data.adventure.golocation.address_components.administrative_area_level_1;
		} 

		$scope.data.adventure.activityType.forEach(function(activity){
			adventureTypeList.push(utils.getActivityByAttr('id', activity.id).name);
		})

		location.href = createNearbyAdventureUrl(adventureTypeList, administrativeAreaLevel1);
	}

	const createNearbyAdventureUrl = function(adventureTypeList, administrativeAreaLevel1){
		//No side effects
		let tempArray = adventureTypeList.slice(0,adventureTypeList.length);
		if(tempArray.length>4){
			tempArray = tempArray.slice(0,4);
		}

		if(administrativeAreaLevel1){
			return '/buscar-aventura#!/'+'filtros--actividad-'+tempArray.join('-')+'--'+'provincia-'+administrativeAreaLevel1;
		} else {
			return '/buscar-aventura#!/'+'filtros--actividad-'+tempArray.join('-');
		}
	}

	var loginError = function(error){
		if(error.statusCode == 401){
			gzzRoute.loginAndComeBack();
		}
	}

	var endedSubmit = function endedSubmit() {
		$rootScope.$emit('toogleSpinner', false);
		$scope.$broadcast('validateOne', function () {});
		$scope.ix.submitting = false;
	};

	var checkSession = function(){
		return llamaApi.user.getCurrent().catch(function(){
			return false;	
		});
	}

	var SendContactAndCreateLead = function () {
		llamaApi.lead.createLead({
		    'companyId': $scope.data.adventure.companyId,
		    'adventureId': $scope.data.adventure._id,
		    'email': $scope.user.email,
		    'contactName': $scope.data.contact.name,
		    'phone': $scope.data.contact.phone,
		    'description': $scope.data.contact.description
		}).then(function() {
			ga.event('User', 'ContactFormAdventure', 'Success');

			$window.scrollTo(0,0);
			$scope.ix.submitting = false;
			$scope.ix.showform = false;
			$scope.ix.show = 'thanks'; 
			$scope.ix.showNearbyAdventures = true; 

			endedSubmit();							
		}).catch(function(err){
			$scope.$broadcast('validateOne', function(){});
			endedSubmit();
		});
	}

	var goToLogin = function(){
		location.href = '/ingreso#/login';
	}

	var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 1,
        paginationClickable: true,
        centeredSlides:true,
        lazyLoading:true
    });

    function lazyGoogleMap(){
    	if($scope.data.adventure.golocation && $scope.data.adventure.golocation.location.lat){
			var googleMapsTries = 0;
			var activateGoogleMaps = setInterval(function(){ 
				googleMapsTries++;
				if(window.google && window.google.maps){
					var mapDiv = document.querySelector('.map');

					var map = new google.maps.Map(mapDiv, {
							center: {'lat': parseFloat($scope.data.adventure.golocation.location.lat), 'lng': parseFloat($scope.data.adventure.golocation.location.lng)},
							zoom: 14
					});

					var marker = new google.maps.Marker({
						position: {'lat': parseFloat($scope.data.adventure.golocation.location.lat), 'lng': parseFloat($scope.data.adventure.golocation.location.lng)},
						map: map
						});


					clearInterval(activateGoogleMaps)
				} else {
					if(googleMapsTries>3){
						clearInterval(activateGoogleMaps)
					}
				}
			}, 3000);
		}
    }

    $scope.facebookShare = function(e){
        FB.ui({
          method: 'share',
          href: location.href,
        }, function(response){
            if(response){
                checkSession().then(function(user){
                    if(user){
                        ga.event('User', 'FacebookShare', user._id);
                    }
                });
            }
        });
    }

    $scope.twitterShare = function(comment){
        var url = 'https://www.llamaway.com';
        var text = $scope.data.adventure.name;
        if($scope.data.info.company.name){
        	text += ' en ' + $scope.data.info.company.name;
        }
        text = text.substring(0, 143) + " en Llamaway"
        window.open("http://twitter.com/intent/tweet?text="+ encodeURI(text) +"&url=" + location.href, "");
        ga.event('User', 'TwitterShare', user._id);
    }

    function setCanonicalUrl (url){
        $rootScope.$emit('setCanonicalUrl', url);
    }

	checkSession().then(function(user){
		if(user){
			$scope.user = user;
		}
	});


	const similarAdventures = () => {
		populateSimilarAdventures();
		clearSimilarAdventuresStorage();
		addClassActivityType($scope.similarAdventures);
	}

	const populateSimilarAdventures = () => {
		const similarArray = JSON.parse(sessionStorage.getItem('similarAdventures'));
		if(similarArray){
			$scope.similarAdventures = similarArray.filter(quitCurrentAdventure);
		}else{
			$timeout(function() {
				console.log($scope.data.adventure._id);
				llamaApi.adventure.getSimilars($scope.data.adventure._id).then(function(adventures){
					adventures.forEach(function(current){
						if(Array.isArray(current.profile_picture) ){
							var url = utils.getAdventureProfileThumbnailSearch(current, true);
							if(current.profile_picture.length > 0){
								current.profile_picture[0]['url'] = url;
							}else{
								current.profile_picture = [{'url': url}];
							}
						}else{
							var url = utils.getAdventureProfileThumbnailSearch(current, false);
							current.profile_picture.url = url;
						}
					});
					$scope.similarAdventures = adventures.filter(quitCurrentAdventure);
					clearSimilarAdventuresStorage();
					addClassActivityType($scope.similarAdventures);
				});
			}, 3000);
		}
	}
	const quitCurrentAdventure = (item) => $scope.data.adventure._id !== item._id;
	const clearSimilarAdventuresStorage = () => sessionStorage.removeItem('similarAdventures');
	const addClassActivityType = (similarAdventures) => {
		let activity;
		if(similarAdventures){
			for(var j=0;j<similarAdventures.length; j++){
				for(var i=0;i<similarAdventures[j].activityType.length; i++){
					activity = utils.getActivityByAttr('id', similarAdventures[j].activityType[i].id)
					similarAdventures[j].activityType[i].class = activity.class;
				}
			}
			
		    setTimeout(function() {
		        const allActivities = document.querySelectorAll('.activity-sprite-similar');
		        for(var i=0; i<allActivities.length; i++){
		            allActivities[i].style.backgroundImage = "url('https://res.cloudinary.com/llamaway/image/upload/v1475123550/adventure_types_uhc2su.png')";
		        }
		    }, 500);
		}
	}

	window.s = $scope;
});