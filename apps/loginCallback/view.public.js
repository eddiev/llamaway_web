var qs = require('querystringify');
var store = require('store2');
var llamaApi = require('../../helpers/api');
var params = qs.parse(location.search);
var ga = require('../../helpers/ga');

var endLogin = function(){
	var afterLoginRedirect = store('afterLoginRedirect');
	store.remove('afterLoginRedirect');
	location.href = afterLoginRedirect || '/';
}

store('access_token', params.token)

llamaApi.host = window.env.api_host;

if(store('afterLoginCompany')){

	var company = JSON.stringify(store('afterLoginCompany'));
	store.remove('afterLoginCompany');

	llamaApi.company.create({
		'name':company.name || '',
		'contactName':company.contactName || '',
		'location':company.location || '',
		'phone':company.phone || '',
	}).then(function(newCompany){
		localStorage.setItem('reg', 'empProvider');	
		endLogin();
	}).catch(function(err){
		console.log('error', err);
		location.href="/";
	});
}else{
	if( params.userCreated && params.userCreated._id){
		localStorage.setItem('reg', 'userProvider');
	}
	endLogin();
}