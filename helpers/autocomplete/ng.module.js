module.exports = angular.module('autocomplete',[])
.directive('gzzAutocomplete', function () {
    return {
        restrict: 'EA',
        transclude:true,
        template:'<ng-transclude></ng-transclude>',
        link:function(scope,el,attrs){
            scope.currentList = -1;
            scope.$watch(attrs.gzzAutocomplete,function(newVal){
                scope.lists = newVal;
                scope.selectedIndex = -1;
                scope.selectedList = -1;
                if(scope.lists){
                     scope.currentList = 0;
                }
            });

            el.find('input').on('keydown',function(e){
                var cb = scope[attrs['gzzOnSelect']];
                if(e.keyIdentifier == 'Up'){
                    e.preventDefault();

                    if(scope.selectedIndex > 0){
                        scope.selectedIndex--;
                    }else{
                        if(scope.currentList > 0){
                            scope.currentList--;
                            scope.selectedIndex = scope.lists[scope.currentList].elements.length -1;
                        }
                    }
                }else if(e.keyIdentifier == 'Down'){
                    e.preventDefault();
                    if(scope.selectedIndex < scope.lists[scope.currentList].elements.length-1){
                        scope.selectedIndex++;
                    }else{
                        if(scope.selectedIndex == scope.lists[scope.currentList].elements.length -1
                            && scope.currentList < scope.lists.length-1){
                            scope.currentList++;
                            scope.selectedIndex=0;
                        }
                    }

                }else if(e.keyIdentifier == 'U+001B'){
                    scope.selectedIndex=-1;     
                    scope.lists=null;
                }else if(e.keyIdentifier == 'Enter' && scope.selectedIndex>-1){
                    cb(scope.lists[scope.currentList].elements[scope.selectedIndex]);
                    delete scope.lists;
                    e.preventDefault();
                }
                scope.$apply();
            })
        }
    }
});