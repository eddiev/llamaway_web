var directives = require('../../scripts/llamawayDirectives');
var utils = require('../../scripts/utils');
var llamaApi = require('../../helpers/api');
var cookie = require('cookies-js');
var ga = require('../../helpers/ga');

var resolvePictureProfilesURL = function(results){
	results.forEach(function(adv){
		adv.profile_picture = {
			url:utils.getCompanyProfileThumbnail(adv, true)
		}
	})
}
angular.module('profile',[
	'ngStorage',
	'ngRoute',
	directives.name,
	'ngMap'
])
.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	$routeProvider
	.when("/:section/:id?/:action?/:actionId?",{})

	$locationProvider.hashPrefix('!'); //crawling
}])
.controller('ProfileCtrl', function($scope,
										$timeout,
										$localStorage,
										$route,
										$routeParams,
										$rootScope,
										$window,
										gzzRoute,
										NgMap,
										$q){

	llamaApi.loadQ($q)
	window.s = $scope;
	var searchTimeOut;
	
	$scope.ix = {};

	$scope.data = {
		genericErrorMessage:'Completá este dato',
		searchQuery:'',
		info: {}
	}

	$scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7JjfMgbUyZmzHQxVxZr0IZkt4cND_ySw";

	NgMap.getMap().then(function(map) {
		map.getCenter();
  	});

	$scope.$watch('data.companyId', function(newValue){
		llamaApi.company.getAdventuresById(newValue).then(function(aventures){
			if(aventures.results.length > 0){
				resolvePictureProfilesURL(aventures.results);
				addClassActivityType(aventures.results);
				$scope.data.adventures = aventures.results;
			}
		});
		setCanonicalUrl(window.location.origin + '/e/'+ $scope.data.company.pageName);
	});

	$scope.showContactForm = function(){
		$scope.description = '';
		ga.event('User', 'ContactFormProfile', 'Show');
		$scope.ix.showform = true;
	}

	$scope.cancelContactForm = function(){
		$scope.ix.showform = false;
		ga.event('User', 'ContactFormProfile', 'Close');
	}

	$scope.sendContactForm = function(){
		$scope.ix.submitting = 'info';
		$scope.ix.infoUpdated = false;
		$scope.$broadcast('validateAll', 'info' ,function(valid) {
			if(valid) {
				checkSession().then(function(user) {
				  if (user === false) {
						// El usuario no existe, lo creo y envío el email de notificación
						llamaApi.user.signupAndLogin({
							'email': $scope.user.email,
							'password': '',
						}).then(function(newUser) {
							$scope.user = newUser;
							ga.event('User', 'CreateLlamitaProfile', newUser._id);
							SendContactAndCreateLead();
						}).catch(function (err) {
							$rootScope.$emit('toogleSpinner', false);
							if(err.statusCode == 409){
								// Redirigo al login
								goToLogin();
							}
							endedSubmit();
						});
					} else {
						SendContactAndCreateLead();
					}
				});
			} else {
				endedSubmit();
			}
		});
	}

	var showSuccess = function(view){
		$scope.ix[view] = true;
		setTimeout(function(){
			$scope.ix[view] = false;
		}, 3000);
	}

	$scope.goToAdventure = function(){
		location.href = '/aventura/'+ $scope.data.createdAdventure.safeUrlName + '_' + $scope.data.createdAdventure._id;
	}

	var loginError = function(error){
		if(error.statusCode == 401){
			gzzRoute.loginAndComeBack();
		}
	}

	var endedSubmit = function endedSubmit() {
		$rootScope.$emit('toogleSpinner', false);
		$scope.$broadcast('validateOne', function () {});
		$scope.ix.submitting = false;
	};

	var checkSession = function(){
		return llamaApi.user.getCurrent().catch(function(){
			return false;	
		});
	}

	var SendContactAndCreateLead = function (user) {
		llamaApi.lead.createLead({
		    'companyId': $scope.data.companyId,
		    'email': $scope.user.email,
		    'contactName': $scope.data.contact.name,
		    'phone': $scope.data.contact.phone,
		    'description': $scope.data.contact.description
		}).then(function() {
			ga.event('User', 'ContactFormProfile', 'Success');

			$window.scrollTo(0,0);
			$scope.ix.submitting = false;
			$scope.ix.showform = false;
			$scope.ix.show = 'thanks'; 
			showSuccess('infoUpdated');

			endedSubmit();							
		}).catch(function(err){
			$scope.$broadcast('validateOne', function(){});
			endedSubmit();
		});
	}

	function addClassActivityType(adv){
        var tempAventures = adv;
        for(var j=0;j<tempAventures.length; j++){
            for(var i=0;i<tempAventures[j].activityType.length; i++){
                switch(tempAventures[j].activityType[i].id) {
                    case 1: //'Acúatica'
                        tempAventures[j].activityType[i].class = 'sprite-acuatico';
                    break;
                    case 2://'Avistaje de aves':
                        tempAventures[j].activityType[i].class = 'sprite-avistaje';
                    break;
                    case 3://'Cabalgatas':
                        tempAventures[j].activityType[i].class = 'sprite-cabalgata';
                    break;
                    case 4://'Camping':
                        tempAventures[j].activityType[i].class = 'sprite-camping';
                    break;
                    case 5://'Ciclismo':
                        tempAventures[j].activityType[i].class = 'sprite-ciclismo';
                    break;
                    case 6://'Deportes Extremos':
                        tempAventures[j].activityType[i].class = 'sprite-extremo';
                    break;
                    case 7://'Deportes Invernales':
                        tempAventures[j].activityType[i].class = 'sprite-invernal';
                    break;
                    case 8://'Expediciones/Travesías':
                        tempAventures[j].activityType[i].class = 'sprite-expedicion';
                    break;
                    case 9://'Montañismo y Trekking':
                        tempAventures[j].activityType[i].class = 'sprite-montanismo';
                    break;
                    case 10://'Pesca':
                        tempAventures[j].activityType[i].class = 'sprite-pesca';
                    break;
                    case 11://'Tours/Otros':
                        tempAventures[j].activityType[i].class = 'sprite-otros';
                    break;
                }
            }
        }
	}

	$scope.$on('$routeChangeSuccess', function (ev, current, prev) {
		checkSession().then(function(user){
			if(user){
				$scope.user = user;
			}
		});
	});

    $scope.facebookShare = function(e){
        FB.ui({
          method: 'share',
          href: location.href,
        }, function(response){
            if(response){
                checkSession().then(function(user){
                    if(user){
                        ga.event('User', 'FacebookShare', user._id);
                    }
                });
            }
        });
    }

    $scope.twitterShare = function(comment){
        var url = 'https://www.llamaway.com';
        var text = 'Las mejores aventuras de ' + $scope.data.company.name + " en Llamaway"
        window.open("http://twitter.com/intent/tweet?text="+ encodeURI(text) +"&url=" + location.href, "");
        ga.event('User', 'TwitterShare', user._id);
    }

    function setCanonicalUrl (url){
        $rootScope.$emit('setCanonicalUrl', url);
    }
});