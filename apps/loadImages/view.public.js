var directives = require('../../scripts/llamawayDirectives');
var services = require('../../scripts/llamawayServices');
var llamaApi = require('../../helpers/api');
var utils = require('../../scripts/utils');

angular.module('loadImages',[
	directives.name,
	services.name,
	'ngFileUpload',
	'ngImgCrop',
	'ngRoute'
])
.config(function($routeProvider) {
	$routeProvider
	.when("/:section",{})
	.otherwise( { redirectTo: "/" });
})
.controller('LoadImagesCtrl', function($scope,
							          $timeout,
							          $route,
									  $window,
									  $routeParams,
									  $rootScope,
									  gzzRoute,
									  $q,
									  Upload){

	llamaApi.loadQ($q)

	var imageSpinner = document.querySelector('.imageSpinner');

	$scope.data = {

	}

	$scope.$on('$routeChangeSuccess', function (ev, current, prev) {
		checkSession().then(function(user){
			if(!user){
				gzzRoute.loginAndComeBack();
			} else {
				llamaApi.company.getAdventureById($scope.data.adventure._id).then(function (adventure) {
					$scope.data.adventure = adventure;
				});
			}
		});
	});


	var checkSession = function(){
		return llamaApi.user.getCurrent().catch(function(){
			return false;
		});
	}

	function getDivisorRatio(width, height){
		var aspectRatio = 1.7;
		var divisor = 2;
		var originalWidth = width;
		var originalHeight = height;

		while (width > 1000 || height >1000) {
    		width = (originalWidth/divisor)*aspectRatio;
    		height = (originalHeight/divisor)*aspectRatio;
			divisor++;
		}
		return divisor-1;
	}

	//Para que se actualice con cada movimiento
	$scope.blockingObject = {'block':true};

	var radialProgress =  angular.element(document.querySelector('.radial-progress'));
    var setProgressCircle = function(number) {
        radialProgress.attr('data-progress',number);
        $scope.progressPercentage = number;
    }

	var uploadFile = function(companyId, adventureId, file){
		$scope.progressPercentage = 0;
		setProgressCircle(0);
		var token = localStorage.getItem('access_token');
		var uti = localStorage.getItem('uti');

		if(token){
			token = token.replace(/\"/g, "");
		}

		if(uti){
			uti = uti.replace(/\"/g, "");
		}

		var startUpload = new Date();
		$scope.low = false;
		Upload.upload({
            url: llamaApi.company.getProfilePictureUrl(companyId, adventureId),
            data: {profile_picture: file},
            json:true,
			headers: {
				'Authorization':'Bearer '+ token,
				'uti': uti,
				'X-Country':''
			}
        }).then(function (resp) {
        	$scope.data.adventure = resp.data;
			$scope.cleanImage();
			document.querySelector(".loadingSpinner").style.display = 'none';
			$scope.progressPercentage = 0;
			setProgressCircle(0);
			$scope.low = false;
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        	var actualTime = new Date();
        	var diff = actualTime - startUpload;
			var sizeRest = evt.total - evt.loaded;
			var timeRest = sizeRest * diff / evt.loaded;

        	timeRest = timeRest / 1000;
			if(!$scope.low && timeRest > 60){
				$scope.low = true;
			}

        	$scope.restTime = Number.isNaN(timeRest) ? 0 : timeRest;
            setProgressCircle($scope.progressPercentage);
        }).catch(function(err){
			$scope.progressPercentage = 0;
			setProgressCircle(0);
			$scope.low = false;
			$scope.ix.submitting = false;
			document.querySelector(".loadingSpinner").style.display = 'none';
		});
	}

	/*Magia, refactorizarla bien*/
	$scope.upload = function (name) {
		$scope.blockingObject.render(function(dataUrl){
			if(Upload.isResizeSupported()){
        		document.querySelector(".loadingSpinner").style.display = 'block';
				var rawFile = Upload.dataUrltoBlob(dataUrl, name);
				Upload.imageDimensions(rawFile).then(function(dimensions){
					if(dimensions.width > 1000 || dimensions.height >1000){
						var divisor = getDivisorRatio(dimensions.width, dimensions.height);

						Upload.resize(rawFile, 1000, 1000, 1, 'image/jpeg', divisor+':1', false, function (width, height) {
							if(dimensions.width > 1000 || dimensions.height > 1000){
								return true;
							} else {
								return false;
							}
						}).then(function(resizedFile){
							if($scope.user.companyId && $scope.data.adventure._id && resizedFile){	
								uploadFile($scope.user.companyId, $scope.data.adventure._id, resizedFile);
							}
						});
					} else {
						if($scope.user.companyId && $scope.data.adventure._id && rawFile){
							uploadFile($scope.user.companyId, $scope.data.adventure._id, rawFile);
						}
					}
				});
			} else {
				console.log('Browser incompatible')
			}
		});
    }

    $scope.deleteImage = function(imageId){
		if(imageId){	

			$rootScope.$emit('toogleSpinner', true);
			
			llamaApi.company.deleteAdventureProfilePicture(
				$scope.user.companyId,
				$scope.data.adventure._id,
				imageId)
			.then(function(res){
				llamaApi.company.getAdventureById($scope.data.adventure._id).then(function (adventure) {
					$scope.data.adventure = adventure;
				});
				$scope.cleanImage();
			}).catch(function(err){
				$scope.ix.submitting = false;
			}).finally(function(){
				$rootScope.$emit('toogleSpinner', false);
			})
		}

    }

    $scope.cleanImage = function(){
    	$scope.picFile = "";
		$scope.croppedDataUrl = "";
    }

    $scope.finish = function(){
    	if(sessionStorage.getItem("llama-av")){
    		location.href = '/paquetes';
    	} else {
    		location.href = '/editar-aventura/'+$scope.data.adventure.safeUrlName+'_'+$scope.data.adventure._id;
    	}
    }

	window.s = $scope;
});