var Dropzone = require('dropzone');
var services = require('./llamawayServices');
var utils = require('../scripts/utils');

var listActivityType = [
  {id: 1, name: 'acuaticas', label: 'Acúaticas'},
  {id: 2, name: 'avistajes', label: 'Avistajes'},
  {id: 3, name: 'cabalgatas', label: 'Cabalgatas'},
  {id: 4, name: 'camping', label: 'Camping'},
  {id: 5, name: 'ciclismo', label: 'Ciclismo'},
  {id: 6, name: 'deportes_extremos', label: 'Deportes Extremos'},
  {id: 7, name: 'deportes_invernales', label: 'Deportes Invernales'},
  {id: 8, name: 'expediciones_travesias', label: 'Expediciones/Travesías'},
  {id: 9, name: 'montañismo_trekking', label: 'Montañismo y Trekking'},
  {id: 10, name: 'pesca', label: 'Pesca'},
  {id: 11, name: 'otros', label: 'Tours/Otros'}
];

var payMethods = [
    {id: 1, name: 'Efectivo'},
    {id: 2, name: 'Tarjeta de débito'},
    {id: 3, name: 'Tarjeta de crédito'},
    {id: 4, name: 'Mercadopago/Transferencias'}
];

var difficultyList = [{
    id: 0,
    name:'facil',
    label: 'Fácil'
},{
    id: 1,
    name:'normal',
    label: 'Normal'
},{
    id: 2,
    name:'dificil',
    label: 'Difícil'
},{
    id: 3,
    name:'experto',
    label: 'Experto'
}];

var difficultyList = [{
    id: 0,
    name:'facil',
    label: 'Fácil'
},{
    id: 1,
    name:'normal',
    label: 'Normal'
},{
    id: 2,
    name:'dificil',
    label: 'Difícil'
},{
    id: 3,
    name:'experto',
    label: 'Experto'
}];

module.exports = angular.module('llamawayDirectives',[services.name])
.directive('gzzModal', function ( $compile) {

    return {
        restrict: 'A',
        scope:{
            show:'=?show',
            close:'=?close',
            closeAvailable:'=?closeAvailable'
        },
        link:function(scope, element, attrs, ngModelCtrl) {
            scope.$watch('show',function(show){
                if(show && show==true){
                    element.addClass('show');
                }else{
                    element.removeClass('show');
                }
            });

            scope.closeOverlay = function(){
                if(typeof(scope.closeAvailable) == 'undefined' || scope.closeAvailable == true){
                    scope.show = false;
                    if(scope.close){
                        scope.close();
                    }
                }
            }

            var overlay = angular.element('<div class="modalOverlay" data-ng-click="closeOverlay()">');
            $compile(overlay)(scope);
            element.append(overlay);
        }
    }
})
.directive('activityNameById', function(){
  return {
    restrict: "A",
    scope:{
        activityId:'=activityId'
    },
    template: '{{activityName}}',
    link: function (scope) {
      scope.activityName = '';
      listActivityType.some(function(currentActivity){
        if(currentActivity.id == scope.activityId){
          scope.activityName = currentActivity.name;
          return true
        }
        return false;
      });
    }
  }
})
.directive('activityList', function(){
  return {
    restrict: "E",
    transclude: true,
    scope:{
        ngModel:'=?ngModel',
        localLang:'=?localLang'
    },
    template: '<div isteven-multi-select'+
                    ' input-model="listActivity"'+
                    ' output-model="ngModel"'+
                    ' output-properties="id label"'+
                    ' button-label="<[ label ]>"'+
                    ' item-label="<[ label ]>"'+
                    ' tick-property="check"'+
                    ' helper-elements=""'+
                    ' translation="localLang">'+
            '</div>',
    link: function (scope) {
      scope.listActivity = listActivityType;
    }
  }
})
.directive('payMethodNameById', function(){
  return {
    restrict: "A",
    scope:{
        payMethodId:'=payMethodId'
    },
    template: '{{payMethodName}}',
    link: function (scope) {
      scope.payMethodName = '';
      payMethods.some(function(item){
        if(item.id == scope.payMethodId){
          scope.payMethodName = item.name;
          return true
        }
        return false;
      });
    }
  }
})
.directive('gzzMinHeightScreen', function(){
    return {
        restrict: "A",
        scope:{
            height:'=height'
        },
        link: function (scope, element, attrs, ctrls) {
            scope.$watch('height',function(height){
                console.log(height);
                var value = window.innerHeight;
                if(height){
                    if(height.search('%') > -1){
                        value = height.replace('%', '');
                        value = Number(value);
                        if(isNaN(value)){
                            value = window.innerHeight;
                        }else{
                            value = window.innerHeight * (value /100);
                        }
                    }
                }
                element.css('min-height',value + 'px');
                //element.css('max-height',value + 'px');
            });

        }
    };
})
.directive('gzzFocusClass', function(){
    return {
        restrict: "A",
        link: function (scope, element, attrs, ctrls) {
            element.find('input').on('focus',function(){ element.addClass('focus'); })
            element.find('input').on('blur',function(){ element.removeClass('focus'); })

            element.find('select').on('focus',function(){ element.addClass('focus'); })
            element.find('select').on('blur',function(){ element.removeClass('focus'); })

            element.find('textarea').on('focus',function(){ element.addClass('focus'); })
            element.find('textarea').on('blur',function(){ element.removeClass('focus'); })
        }
    };
}).directive('gzzValidate', ['$timeout',
    function ($timeout) {
        return {
            restrict: "A",
            link: function (scope, element, attrs, ctrls) {
                var els = element[0].querySelectorAll('[gzz-valid]');
                //W3C
                els = Array.prototype.slice.call(els);
                if(els.length == 0){
                    els = element[0].querySelectorAll('[data-gzz-valid]');
                }
                var className = 'on-error';
                var scroll = !angular.isDefined(attrs['gzzDisableScrollToInvalid']);

                var validate = function(el){
                    //W3C
                    var attr = el.getAttribute('gzz-valid') || el.getAttribute('data-gzz-valid');
                    var valid = scope.$eval(attr);
                    if(!valid){
                        el.classList.add(className);
                        // console.log(attr,valid,el);
                        /****Marca el error en el div que machee - Edicion y creacion de aventura*****/
                        var pError = el.getAttribute('data-parentError');
                        if(pError){
                            var errorParent = document.querySelector('.' + pError);
                            if(errorParent){
                                errorParent.classList.add('errorParent');
                            }
                        }
                        /********/
                    }else{
                        // console.log(attr,valid,el);
                        el.classList.remove(className);
                    }

                    el.addEventListener('mousedown',function(){
                        // console.log(attr,valid,el);
                        $timeout(function(){
                            el.classList.remove(className);
                            var elsParent = document.querySelector('.errorParent');
                            if(elsParent){
                                elsParent.classList.remove('errorParent');
                            }
                        },300)
                    })

                    return valid;
                }

                var scrollToEl = function(el){
                    // estso porque cuando hace scroll se cruza con el mousedown
                    $timeout(function(){
                        var offset = 56;
                        var elemTop = el.getBoundingClientRect().top;

                        window.scrollBy(0, elemTop - offset);
                    },100)
                }

                scope.$on('validateAll',function(e,formName, cb){
                    if(typeof formName !== 'string'){
                        cb = formName
                        formName = null;
                    }

                    if(!formName || formName === attrs['gzzValidate']){
                        var invalid = null;

                        for (var i = 0; i < els.length; i++) {
                            if(!validate(els[i]) && !invalid){
                                /****Marca el error en el div que machee - Edicion y creacion de aventura*****/
                                var pError = els[i].getAttribute('data-parentError');
                                if(pError){
                                    var el = document.querySelector('.' + pError);
                                    if(el){
                                        el.classList.add('errorParent');
                                    }
                                }
                                /********/
                                invalid = true;
                                if(scroll){
                                    scrollToEl(els[i]);
                                }
                            }
                        };

                        cb(!invalid);
                    }

                });
                scope.$on('validateOne',function(e, cb){
                    var invalid = null;

                    for (var i = 0; i < els.length; i++) {
                        if(!validate(els[i]) && !invalid){
                            if(scroll){
                                scrollToEl(els[i]);
                            }
                            cb(false);
                            return;
                        }
                    };

                    cb(true);
                });
                scope.$on('validateOneVisible',function(e, cb){
                    var invalid = null;

                    for (var i = 0; i < els.length; i++) {
                        var isHidden = false;
                        for (var j = 0; j < els[i].classList.length; j++) {
                            if(els[i].classList[j] == "ng-hide"){
                                isHidden = true;
                            }
                        }
                        if(!isHidden && !validate(els[i]) && !invalid){
                            if(scroll){
                                scrollToEl(els[i]);
                            }
                            cb(false);
                            return;
                        }
                    };

                    cb(true);
                });
                scope.$on('restartValidation',function(e,formName){
                    if(!formName || formName === attrs['gzzValidate']){
                        for (var i = 0; i < els.length; i++) {
                            els[i].classList.remove('on-error');
                        };
                        var elsParent = document.querySelector('.errorParent');
                        if(elsParent){
                            elsParent.classList.remove('errorParent');
                        }
                    }
                });

            }
        };
    }
])
.directive('yearSelector', ['$timeout',
    function ($timeout) {
        return {
            restrict: "A",
            transclude: true,
            scope:{
                topYear:'=top',
                bottomYear:'=bottom'
            },
            template:'<option ng-repeat="year in years" ng-value="{{year}}">{{year}}</option>',
            link: function (scope, element, attrs, ctrls) {
                var generateOptions = function(){
                    var years = [];
                    var topYear = (scope.topYear && parseInt(scope.topYear)) || (new Date()).getFullYear();
                    var bottomYear = scope.bottomYear || (topYear-100);

                    for(var i = topYear;i >= bottomYear;i--){
                        years.push(i);
                    }

                    scope.years = years;
                    // console.log(scope.years)
                }
                scope.$watch('topYear',generateOptions)
                scope.$watch('bottomYear',generateOptions)

            }
        };
    }
])
.directive('gzzInputRadios', function () {

    return {
        restrict: 'E',
        transclude: true,
        scope:{
            bindModel:'=ngModel'
        },
        template: '<ul class="inputRadios">' +
            '<li ng-repeat="item in items">' +
            '<input type="radio" name="{{name}}" ng-model="data.model" ng-value="item">' +
            '</li>' +
        '</ul>',
        link:function(scope, element, attrs, ngModelCtrl) {
            var items = [];
            var from = 1;

            if(angular.isDefined(attrs.from)){
                from = parseInt(attrs.from);
            }

            for (var i = from; i <= parseInt(attrs.max); i++) {
                items.push(i);
            };

            scope.items = items;
            scope.name = attrs.ngModel;

            // this is because ng-repeat creates a new scope
            scope.$watch('bindModel',function(){
                scope.data = {
                    model:scope.bindModel
                };
            });

            scope.$watch('data.model',function(val){
                scope.bindModel = val;
            });
        }

    }

})
.directive('gzzCheckbox', function () {

    return {
        restrict: 'E',
        scope:{
            ngModel:'=?ngModel'
        },
        transclude: true,
        template:
            '<label>'+
            '<i class="fa"></i>'+
            '<input type="checkbox" ng-model="ngModel"/>'+
            '<span ng-transclude></span>'+
            '</label>',
        link:function(scope, element, attrs) {
            element.addClass('gzz-checkbox');

            scope.$watch('ngModel',function(){
                element[0].querySelector('i').classList.toggle('fa-circle-o',!scope.ngModel);
                element[0].querySelector('i').classList.toggle('fa-check-circle-o',scope.ngModel);

                element.toggleClass('checked',scope.ngModel || false)
            })
        }

    }

})
.directive('gzzRadio', function () {

    return {
        restrict: 'E',
        scope:{
            ngModel:'=ngModel',
            ngValue:'=ngValue'
        },
        transclude: true,
        template: '<input type="radio" id="{{id}}" ng-model="ngModel" ng-value="ngValue" />'+
            '<label for="{{id}}" ng-transclude></label>',
        link:function(scope, element, attrs) {
            element.on('click',function(){
                element[0].querySelector('input').click();
            })
            scope.$watch('ngModel',function(newValue, oldValue){
                if(scope.ngModel !== ''){
                    element.toggleClass('checked',scope.ngModel==scope.ngValue);
                } else {
                    element.toggleClass('checked', false);
                }
            })
            scope.id = Math.random().toString();
        }

    }

})
.directive('gzzStars', function () {

    return {
        restrict: 'E',
        transclude: true,
        scope:{
            max:'=max',
            value:'=value'
        },
        template: '<ul class="inputRadios">' +
            '<li ng-repeat="item in items track by $index">' +
                '<i class="fa fa-star star-base"></i>' +
                '<i class="fa star-fill" ng-class="classes[$index]"></i>' +
            '</li>' +
        '</ul>',
        link:function(scope, element, attrs, ngModelCtrl) {
            scope.$watch('value',function(){
                scope.classes = [];
                scope.items = new Array(scope.max);

                for (var i = 0; i < scope.items.length; i++) {
                    if(i+1 <= scope.value){
                        scope.classes[i] = 'fa-star';
                    }else if(i < scope.value && i+1 > scope.value){
                        scope.classes[i] = 'fa-star-half';
                    }else if(i+1 > scope.value){
                        scope.classes[i] = 'fa-star-o';
                    }
                };
            })
        }

    }

})
.directive('gzzDropzone', function () {
    return {
        restrict: 'E',
        scope:{
            ngModel:'=ngModel',
            ngTitle:'=ngTitle',
        },
    transclude: true,
        template:'<label class="form-field-button" data-ng-class="{\'on-error\': isError}">'+
          '<span class="field-label" data-ng-show="ngTitle">{{ngTitle}}</span>'+
          '<button class="button button-block">{{value}}</button>'+
          '<div class="error-message" data-ng-class="{\'gzzDropZoneError\':isError}">{{errorMessage}}</div>'+
        '</label>',
        link:function(scope, element, attrs, ngModelCtrl) {
          console.log('drop');
            var options = {
                url:'placeholder',
                autoProcessQueue:false,
                uploadMultiple:true,
                maxFiles:5,
                acceptedFiles:'image/jpeg,image/png,image/gif',
                dictInvalidFileType: 'El archivo posee una extension no valida. Solo se permiten imagenes',
                dictMaxFilesExceeded: 'Solo se permiten hasta 5 imagenes.',
                addRemoveLinks: true,
                dictRemoveFile: 'Eliminar',
            }
            scope.isError = false;
            scope.value = 'Elegir imagen';
            var myDropzone = new Dropzone(element[0].querySelector('button'), options);
            scope.ngModel = myDropzone;
            myDropzone.on("addedfile", function(file) {
              scope.value = '';
              scope.$apply();
            });
            myDropzone.on("removedfile", function(file) {
              if(this.files.length == 0){
                scope.value = 'Elegir imagen';
                scope.$apply();
              }
            });

            myDropzone.on("error", function(file, message) {
              scope.errorMessage = message;
              myDropzone.removeFile(file);
              scope.isError = true;
              scope.$apply();
              setTimeout(function() {
                scope.isError = false;
                scope.$apply();
              },6000);
            });
        }
    }

})
.directive('gzzRef',function($location,$timeout,gzzRoute) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.on('click', function() {
                gzzRoute.go(attrs.gzzRef,{scroll:attrs['gzzRefScroll']||''})
            });
        }
    }
})
.directive('gzzFocus', function ($timeout) {

    return {
        restrict: 'A',
        scope:{
            gzzFocus:'=gzzFocus'
        },
        link: function (scope, element, attrs, ctrls) {
            scope.$watch('gzzFocus',function(newValue,oldValue){

                if(newValue == true){
                    $timeout(function () {
                        element[0].focus();
                    },20);
                    element.on('blur',function(){
                        scope.gzzFocus = false;
                        scope.$digest();
                    })
                }
            });
        }
    };
})

/*!
 * author: Nestor Paez
 * version: 0.0.1
 * license: MIT
 * homepage: http://github.com/npaez/angular-cards
 */
.directive('cardHeader', function($compile) {
   return {
      restrict: 'E',
      scope: {
         imgSrc: '@'
      },
      link: function(scope, element, attrs) {
         if(attrs.imgSrc != undefined) {
            element.find('span').css('margin-left', '55px');
            element.find('h4').css('margin-left', '55px');
         } else {
            element.find('img').remove();
         };

         if(!attrs.cardTitle) {
            element.find('div').remove();
         } else {
            attrs.$observe('cardTitle', function(value){
               element.find('h4').text(value);
            });

            if(attrs.cardSubtitle != undefined) {
               attrs.$observe('cardSubtitle', function(value){
                  element.find('span').text(value);
               });
            };
         };
      },
      template: '<div class="card-header">'
               +   '<img class="img-src" ng-src="{{ imgSrc }}">'
               +   '<h4 class="card-title"></h4>'
               +   '<span class="card-subtitle"></span>'
               +'</div>'
   };
})
.directive('cardImg', function() {
   return {
      restrict: 'E',
      scope: {
         imgSrc: '@',
         imgTitle: '@'
      },
      link: function(scope, element, attrs) {
         /*!
          * If imgSrc is undefined, erase the div
          */
         if(!attrs.imgSrc) {
            element.find('span').remove();
            element.find('div').remove();
         };
      },
      template: '<div class="card-img">'
               +   '<img ng-src="{{ imgSrc }}">'
               +   '<span class="card-title">{{ imgTitle }}</span>'
               +'</div>'
   };
})
.directive('cardContent', function() {
   return {
      restrict: 'E',
      replace: true,
      link: function(scope, element, attrs) {
        scope.viewText = false;
         /*!
          * Sets the title
          */
         attrs.$observe('cardTitle', function(value){
            element[0].querySelector('.card-title').innerText = value;
         });

         attrs.$observe('cardSubContent', function(value){
            element[0].querySelector('.sub-content').innerHTML = value;
         });

         /*!
          * Sets the card text
          */
         attrs.$observe('cardText', function(value){
            if(value){
                element.find('p').html(value);
                element.find('p').css('display', 'block');
            }else{
                element.find('p').css('display', 'none');
            }
         });

         attrs.$observe('cardInner', function(value){
             element.find('div')[1].innerHTML = value;
         });

         if('cardIcon' in attrs) {
            attrs.$observe('cardIcon', function(value){
                element.find('i').html(value);
                document.querySelector('.card-title').style.paddingLeft = "48px";
                document.querySelector('.card-title').style.marginTop = "1px";
                element.find('p').css('padding-left', '48px');
            });
         };
      },
      template: '<div class="card-content">'
               +   '<div class="icon-inner"><i></i></div>'
               +   '<span class="card-title"></span>'
               +   '<span class="sub-content"></span>'
               +   '<p class="card-text"></p>'
               +'</div>'
   };
})
.directive('cardCompanyContent', function() {
   return {
      restrict: 'E',
      replace: true,
      link: function(scope, element, attrs) {
        scope.viewText = false;
         /*!
          * Sets the title
          */
         attrs.$observe('cardTitle', function(value){
            element[0].querySelector('.card-title').innerText = value;
         });

         attrs.$observe('cardSubContent', function(value){
            element[0].querySelector('.sub-content').innerHTML = value;
         });

         /*!
          * Sets the card text
          */
         attrs.$observe('cardText', function(value){
            if(value){
                element.find('p').html(value);
                element.find('p').css('display', 'block');
            }else{
                element.find('p').css('display', 'none');
            }
         });

         attrs.$observe('cardInner', function(value){
             element.find('div')[1].innerHTML = value;
         });

         if('cardIcon' in attrs) {
            attrs.$observe('cardIcon', function(value){
                element.find('i').html(value);
                document.querySelector('.card-title').style.paddingLeft = "48px";
                document.querySelector('.card-title').style.marginTop = "1px";
                element.find('p').css('padding-left', '48px');
            });
         };
      },
      template: '<div class="card-content">'
               +   '<div class="icon-inner"><i></i></div>'
               +   '<span class="card-title"></span>'
               +   '<span class="sub-content"></span>'
               +   '<p class="card-company-text"></p>'
               +'</div>'
   };
})
.directive('cardButton', function($compile) {
   return {
      restrict: 'E',
      replace: true,
      scope: {
         btnTitle: '@',
         eventHandler: '&ngClick',
         btnSrc: '@'
      },
      link: function(scope, element, attrs) {
         if(!attrs.btnSrc) {
            /*!
             * Since its has no source, it assumes that you are using a function.
             */
            element.replaceWith($compile('<a ng-click="eventHandler()" class="clickleable">{{ btnTitle }}</a>')(scope));
         } else {
            /*!
             * In this case, you are using a common <a> tag
             */
            element.replaceWith($compile('<a ng-href="{{ btnSrc }}">{{ btnTitle }}</a>')(scope));
         };
      }
   };
})
.directive('cardAdventure', function() {
   return {
      restrict: 'E',
      replace: true,
      scope: {
         adventure: '=ngAdventure',
         isCompany: '=ngCompany',
         view: '=ngViewf',
         edit: '=ngEditf',
         remove: '=ngRemove',
         delete: '=ngDelete'
      },
      link: function(scope, element, attrs) {
         /*!
          * Sets the title
          */
        scope.$watch('adventure', function(newValue, oldValue) {
            if (newValue) {
                if(scope.isCompany){

                }else{
                    if (newValue.takeDays) {
                        newValue.time = 'Días';
                    } else {
                        newValue.time = 'Horas';
                    }

                    if (newValue.currency == 0) {
                        newValue.currencySymbol = '$';
                    } else {
                        newValue.currencySymbol = 'U$S';
                    }

                    switch (newValue.difficulty) {
                        case 0:
                            newValue.difficultyName = 'Fácil';
                            break;
                        case 1:
                            newValue.difficultyName = 'Normal';
                            break;
                        case 2:
                            newValue.difficultyName = 'Difícil';
                            break;
                        case 3:
                            newValue.difficultyName = 'Experto';
                            break;
                    }
                }
                var url = utils.getCompanyProfileThumbnail(newValue, true);
                if(!newValue.profile_picture){
                    newValue.profile_picture[0] = {}
                }
                newValue.profile_picture[0] = {'url': url};
            }
        });
      },
      template: '<div class="cardAdventure">'
               + '<a href="/aventura/{{adventure.safeUrlName}}_{{adventure._id}}" >'
               +   '<div class="card-img">'
               +      '<img ng-src="{{adventure.profile_picture.length ? adventure.profile_picture[0].url : adventure.profile_picture.url}}">'
               +   '</div>'
               +   '<div class="card-title">'
               +      '{{adventure.name}}'
               +   '</div>'
               +   '<ul data-ng-show="!isCompany">'
               +      '<li><i class="fa fa-map-marker"></i> {{adventure.location.address}}<span data-ng-show="adventure._rankingInfo.geoDistance"> - A {{adventure._rankingInfo.geoDistance  | distance}} de tu posición</span></li>'
               +      '<li><i class="fa fa-clock-o" aria-hidden="true"></i> {{adventure.duration}} {{adventure.time}}</li>'
               +      '<li><i class="fa fa-line-chart" aria-hidden="true"></i> {{adventure.difficultyName}}</li>'
               +      '<li class="price">{{adventure.currencySymbol}} {{adventure.price}}</li>'
               +   '</ul>'
               +  '</a>'
               +   '<card-action data-ng-show="view || edit || remove">'
               +      '<div><button data-ng-click="view(adventure)" data-ng-show="view">Ver</button></div>'
               +      '<div><button data-ng-click="edit(adventure)" data-ng-show="edit">Editar</button></div>'
               +      '<div><button data-ng-click="remove(adventure)" data-ng-show="remove">Eliminar</button></div>'
               +   '</card-action>'

               +   '<div gzz-modal="" class="modal" data-show="adventure.showRemove" class="confirm-removal">'
               +      '<div class="modalContent">'
               +          '<p class="space">¿Seguro que querés eliminar la aventura <strong>{{adventure.name}}?</strong></p>'
               +          '<div class="actions">'
               +              '<button class="button button-sm" data-ng-click="remove(adventure)">Cancelar</button>'
               +              '<button ga-click="User,DeleteReview" class="button button-sm button-primary" data-ng-click="delete(adventure)">Eliminar</button>'
               +          '</div>'
               +      '</div>'
               +  '</div>'
               +  '<div class="modal" data-ng-show="adventure.deleted" class="alert alert-neutral">'
               +     'Eliminaste la aventura {{adventure.name}}'
               +  '</div>'
               + '</div>'

   };
})

.directive('cardAdventureList', function() {
   return {
      restrict: 'E',
      replace: true,
      scope: {
         adventure: '=ngAdventure'
      },
      link: function(scope, element, attrs) {
         /*!
          * Sets the title
          */
        scope.$watch('adventure', function(newValue, oldValue) {
            if (newValue) {
                if (newValue.takeDays) {
                    newValue.time = 'Días';
                } else {
                    newValue.time = 'Horas';
                }
                if (newValue.currency == 0) {
                    newValue.currencySymbol = '$';
                } else {
                    newValue.currencySymbol = 'U$S';
                }

                switch (newValue.difficulty) {
                    case 0:
                        newValue.difficultyName = 'Fácil';
                        break;
                    case 1:
                        newValue.difficultyName = 'Fácil';
                        break;
                    case 2:
                        newValue.difficultyName = 'Difícil';
                        break;
                    case 3:
                        newValue.difficultyName = 'Experto';
                        break;
                }
            }
        });
      },
      template:   '<a href="/aventura/{{adventure.safeUrlName}}_{{adventure._id}}">'
            +           '<div class="card">'
            +               '<div class="aventure-detail">'
            +                  '<div class="logo">'
            +                      '<div class="logo-container">'
            +                          '<img src="{{adventure.profile_picture.length ? adventure.profile_picture[0].url : adventure.profile_picture.url}}" logo">'
            +                      '</div>'
            +                  '</div>'
            +                  '<div class="aventure-data-container">'
            +                      '<div class="wrapper-column">'
            +                          '<div class="name">'
            +                              '<b>{{adventure.name}}</b>'
            +                          '</div>'
            +                          '<div class="location">'
            +                              '<i class="fa fa-map-marker"></i>'
            +                              '<div class="label">{{adventure.location.address}}<span data-ng-show="adventure._rankingInfo.geoDistance"> - A {{adventure._rankingInfo.geoDistance  | distance}} de tu posición</span></div>'
            +                          '</div>'
            +                          '<div class="duration">'
            +                              '<i class="fa fa-clock-o" aria-hidden="true"></i>'
            +                              '<div class="label">{{adventure.duration}} {{adventure.time}}</div>'
            +                          '</div>'
            +                          '<div class="difficulty">'
            +                              '<i class="fa fa-line-chart" aria-hidden="true"></i>'
            +                              '<div class="label">{{adventure.difficultyName}}</div>'
            +                          '</div>'
            +                      '</div>'
            +                      '<div class="wrapper-column">'
            +                          '<div class="adprice">'
            +                              '<div>{{adventure.currencySymbol}} {{adventure.price}}</div>'
            +                          '</div>'
            +                      '</div>'
            +                  '</div>'
            +              '</div>'
            +          '</div>'
            +      '</a>'
   };
})

.directive('cardActivity', function() {
   return {
      restrict: 'E',
      replace: true,
      scope: {
         activities: '=?ngActivities',
         title: '=?ngTitle'
      },
      template: '<div class="cardActivity">'
               +   '<span class="card-title">{{title}}</span>'
               +   '<div class="activities">'
               +      '<div class="activity" data-ng-repeat="activity in activities">'
               +          '<div class="card-img-activity activity-{{activity.id}}" style="background:url(data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=)">'
               +          '</div>'
               +          '<div class="title">'
               +             '{{activity.name}}'
               +          '</div>'
               +   '   </div>'
               +   '</div>'
               +'</div>'
   };
})
.directive('listActivitiesImg', function() {
   return {
        restrict: 'E',
        replace: true,
        scope: {
            activities: '=?ngActivities',
        },
        template: '<div data-ng-repeat="activity in activities | limitTo:4" class="x-small-activity-{{activity.id}}" style="background:url(data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=)"></div>'
   };
})
.directive('cardCompany', function() {
   return {
      restrict: 'E',
      replace: true,
      scope: {
         company: '=ngCompany'
      },
      link: function(scope, element, attrs) {
                scope.$watch('company', function(newValue, oldValue) {
                    console.log(newValue);
                });

      },
      template: '<div class="main-company">'
               +'    <div class="logo">'
               +'        <img src="{{company.profile_picture.url}}">'
               +'    </div>'
               +'    <div class="name">'
               +'        <div class="title">'
               +'            {{company.name}}'
               +'        </div>'
               +'        <div class="place">'
               +'            <span class="icon material-icons md-18">place</span>'
               +'            {{company.location.address}}'
               +'        </div>'
               +'    </div>'
               +'    <p class="card-text">{{company.description}}</p>'
               +'</div>'
   };
}).filter('distance', function() {
    return function(input) {
        if (input >= 1000) {
            return (input / 1000).toFixed(2) + 'km';
        } else {
            return input + 'm';
        }
    }
})
.directive('tag', function() {
    return {
        restrict: 'E',
        scope: {
            ngTags: '=ngTags',
            ngClickTag: '=ngClickTag'

        },
        template: '<span class="tag" data-ng-repeat="item in ngTags.list | filter: {selected:true}" data-ng-click="ngClickTag(item, item.id)">' +
                '{{item.label}} x'+
            '</span>'
    }       
})

.directive('searchFacets', function($timeout) {
    return {
        restrict: 'E',
        scope: {
            ngIsAdventure: '=ngIsAdventure',
            ngFacets: '=ngFacets',
            ngFacetsStats: '=ngFacetsStats',
            ngFilters: '=ngFilters',
            ngType: '=ngType',
            ngQuery: '=ngQuery',
            ngShowFacets: '=ngShowFacets',
            ngCount: '=ngCount'
        },
        template:'<h2 data-ng-show="ngFacets">Filtros:</h2>'+
        '<div class="appliedFilters">'+
            '<span class="tag" data-ng-show="ngFilters.price && price.withPrice" data-ng-click="clearFilter(\'price\')">Desde {{currencySymbol}}{{price.labelFilterMin}} a {{currencySymbol}}{{price.labelFilterMax}} x</span>'+
            '<span class="tag" data-ng-show="ngFilters.price && !price.withPrice" data-ng-click="clearFilter(\'price\')">Gratuita x</span>'+
            '<span class="tag" data-ng-show="ngFilters.durationFilter" data-ng-click="clearFilter(\'duration\')">Desde {{duration.labelFilterMin}} a {{duration.labelFilterMax}} {{takeDaysSymbol}} x</span>'+
            '<span class="tag" data-ng-repeat="item in selectedFacet" data-ng-click="clearFilter(item.facetName, item.name)">' +
                '{{item.label}} x'+
            '</span>'+
            '<span class="tag" data-ng-show="ngFilters.topPlacesFilter" data-ng-click="clearFilter(\'top\')">Top Aventuras x</span>'+
        '</div>'+

        '<div data-ng-repeat="facet in facets" class="filters" data-ng-show="ngShowFacets">' +
            '<div data-ng-show="facet.type==\'facet\' && facet.list.length > 0">' +
                '<div class="header" data-ng-click="ix.showFilter = facet.name">'+
                    '<h3 class="list-heading">{{facet.label}}</h3>' +
                    '<div class="filter" data-ng-hide="ix.showFilter==facet.name">'+
                        '<i class="fa fa-angle-double-down" aria-hidden="true"></i>'+
                    '</div>'+
                    '<div class="filter" data-ng-show="ix.showFilter==facet.name">'+
                        '<i class="fa fa-angle-double-up" aria-hidden="true"></i>'+
                    '</div>'+
                '</div>'+
                '<ul class="listFilters" data-ng-class="{\'show\': ix.showFilter==facet.name}">'+
                    '<li data-ng-repeat="facetType in facet.list" data-ng-hide="">' +
                        '<a href="{{facetType.url}}" data-ng-class="{\'selected\':facetType.selected}" rel="nofollow">' +
                            '<span>{{facetType.label}}</span><span>{{facetType.count}}</span>' +
                        '</a>' +
                    '</li>' +
                '</ul>' +
            '</div>' +
        '</div>'+

        '<div class="maxMin" data-ng-show="ngShowFacets && duration">' +
            '<div class="header" data-ng-click="ix.showFilter = \'duration\'">'+
                '<h3 class="list-heading">{{duration.label}}</h3>'+
                '<div class="filter" data-ng-hide="ix.showFilter==\'duration\'">'+
                    '<i class="fa fa-angle-double-down" aria-hidden="true"></i>'+
                '</div>'+
                '<div class="filter" data-ng-show="ix.showFilter==\'duration\'">'+
                    '<i class="fa fa-angle-double-up" aria-hidden="true"></i>'+
                '</div>'+
            '</div>'+
            '<ul data-ng-class="{\'show\': ix.showFilter==\'duration\'}">'+
                '<li class="sliderContainer">'+
                    '<rzslider class="" rz-slider-model="takeDays" rz-slider-high="" rz-slider-options="duration.slider.options"></rzslider>'+
                '</li>'+
                '<li class="row">'+
                    '<label class="form-field-text min">'+
                        '<span class="field-label">Mínimo</span>'+
                        '<input type="number" data-ng-model="duration.min" min="{{minDuration}}" max="{{maxDuration-1}}" data-ng-max="maxDuration-1">'+
                    '</label>'+
                    '<label class="form-field-text max">'+
                        '<span class="field-label">Máximo</span>'+
                        '<input type="number" data-ng-model="duration.max" min="{{duration.min+1}}" max="{{maxDuration}}" data-ng-min="duration.min+1" data-ng-max="maxDuration">'+
                    '</label>'+
                    '<button class="button button-block button-secondary button-sm" data-ng-click="filterByDuration()">'+
                        '>'+
                    '</button>'+
                    '<div class="error-message duration">Debe tener un mínimo de 0 y un máximo de {{maxDuration}}</div>'+
                '</li>'+
            '</ul>'+
        '</div>'+

        '<div class="maxMin price" data-ng-show="ngShowFacets && price.typeSlider">' +
            '<div class="header" data-ng-click="ix.showFilter = \'type\'">'+
                '<h3 class="list-heading">{{price.typeSlider.label}}</h3>' +
                '<div class="filter" data-ng-hide="ix.showFilter==\'type\'">'+
                    '<i class="fa fa-angle-double-down" aria-hidden="true"></i>'+
                '</div>'+
                '<div class="filter" data-ng-show="ix.showFilter==\'type\'">'+
                    '<i class="fa fa-angle-double-up" aria-hidden="true"></i>'+
                '</div>'+
            '</div>'+
            '<ul data-ng-class="{\'show\': ix.showFilter==\'type\'}">'+
                '<li class="sliderContainer">'+
                    '<rzslider class="" rz-slider-model="price.withPrice" rz-slider-options="price.typeSlider.options"></rzslider>'+
                '</li>'+
            '</ul>'+
        '</div>'+

        '<div class="maxMin price" data-ng-show="ngShowFacets && price.withPrice">' +
            '<div class="header" data-ng-click="ix.showFilter = \'price\'">'+
                '<h3 class="list-heading">{{price.label}}</h3>' +
                '<div class="filter" data-ng-hide="ix.showFilter==\'price\'">'+
                    '<i class="fa fa-angle-double-down" aria-hidden="true"></i>'+
                '</div>'+
                '<div class="filter" data-ng-show="ix.showFilter==\'price\'">'+
                    '<i class="fa fa-angle-double-up" aria-hidden="true"></i>'+
                '</div>'+
            '</div>'+
            '<ul data-ng-class="{\'show\': ix.showFilter==\'price\'}">'+
                '<li class="sliderContainer" data-ng-show="price.withPrice">'+
                    '<rzslider class="" rz-slider-model="currency" rz-slider-options="price.currencySlider.options"></rzslider>'+
                '</li>'+
                '<li class="sliderContainer" data-ng-show="price.withPrice">'+
                    '<rzslider class="" rz-slider-model="price.min" rz-slider-high="price.max" rz-slider-options="price.priceSlider.options"></rzslider>'+
                '</li>'+
                '<li class="row">'+
                    '<label class="form-field-text min" data-ng-show="price.withPrice">'+
                        '<span class="field-label">Mínimo</span>'+
                        '<input type="number" data-ng-model="price.min" min="{{minPrice}}" max="{{price.max ? price.max-1 : 1}}" data-ng-max="price.max ? price.max-1 : 1">'+
                    '</label>'+
                    '<label class="form-field-text max" data-ng-show="price.withPrice">'+
                        '<span class="field-label">Máximo</span>'+
                        '<input type="number" data-ng-model="price.max" min="{{price.min ? price.min+1 : 2}}" max="{{maxPrice}}" data-ng-min="price.min ? price.min+1 : 2" data-ng-max="maxPrice" data-ng-maxlength="6" maxlength="6">'+
                    '</label>'+
                    '<button class="button button-block button-secondary button-sm" data-ng-click="filterByPrice()">'+
                        '>'+
                    '</button>'+
                    '<div class="error-message price">Debe tener un mínimo de {{minPrice}} y un máximo de {{maxPrice}}</div>'+
                '</li>'+
            '</ul>'+
        '</div>',
        link: function(scope, element, attrs, ctrls) {
            scope.ix = {
                showFilter: 'actividad'
            }

            scope.duration = {
                'min': 1,
                'name': 'duracion',
                'label': 'Duracion',
                'type': 'duration'
            };

            scope.price = {
                'min': 1,
                'name': 'precio',
                'label': 'Precio',
                'type': 0,
                'withPrice': 1,
                'withoutPrice': false,
                'free': false
            };

            scope.changeWithPrice = true;
            scope.changeDuration = true;

            scope.$watch('price.withPrice', function(newValue){
                if(!isNaN(newValue) && !scope.changeWithPrice){
                    var price = createFilterByPrice();
                    if(price){
                        window.location.hash = maxMin(price, 'precio');
                    }
                }else{
                    if(scope.changeWithPrice){
                        scope.changeWithPrice = false;
                    }
                }
            });

            scope.$watch('takeDays', function(newValue, oldValue) {
                if(!isNaN(newValue)){
                    if(newValue){
                        scope.maxDuration = 7;
                        scope.minDuration = 1;
                    }else{
                        scope.maxDuration = 24;
                        scope.minDuration = 0;
                    }
                    if(scope.changeDuration){
                        scope.duration.max = scope.maxDuration;
                        scope.duration.min = scope.minDuration;
                    }else{
                        scope.changeDuration = true;
                    }
                }
            });

            scope.$watch('ngFacetsStats', function(newValue, oldValue) {
                if(newValue){
                    if(newValue.price){
                        scope.maxPrice = newValue.price.max;
                        scope.minPrice = newValue.price.min != 0 ? newValue.price.min : 1;
                        scope.price.max = scope.maxPrice;
                        scope.price.min = scope.minPrice;
                    }
                }
            });

            var addSelectedFacet = function(name, facetActivity){
                var selected = {};
                Object.assign(selected, facetActivity);
                selected.facetName = name;
                scope.selectedFacet.push(selected);
            }

            scope.$watch('ngFilters', function(newValue, oldValue) {
                scope.selectedFacet = [];
                if(newValue){
                    if(newValue.durationFilter){
                        var splited = newValue.durationFilter.split(",");
                        if(splited.length == 3){
                            scope.changeDuration = false;
                            scope.takeDays = splited[0] == "0" ? 0 : 1;
                            if (scope.takeDays == 0) {
                                scope.takeDaysSymbol = 'hs';
                            } else {
                                scope.takeDaysSymbol = 'días';
                            }
                            scope.duration.min = Number(splited[1]);
                            scope.duration.max = Number(splited[2]);
                            scope.duration.labelFilterMin = scope.duration.min;
                            scope.duration.labelFilterMax = scope.duration.max;
                        }
                    }else{
                        scope.duration.min = scope.minDuration;
                        scope.duration.max = scope.maxDuration;
                    }

                    if(newValue.price){
                        var splited = newValue.price.split(",");
                        if(splited.length == 4){
                            scope.currency = splited[1] == "pesos" ? 0 : 1;
                            if (scope.currency == 0) {
                                scope.currencySymbol = '$';
                            } else {
                                scope.currencySymbol = 'U$S';
                            }
                            scope.price.min =  scope.minPrice < Number(splited[2]) ? Number(splited[2]) : scope.minPrice;
                            scope.price.max =  scope.maxPrice > Number(splited[3]) ? Number(splited[3]) : scope.maxPrice;
                            scope.price.labelFilterMin = Number(splited[2]);
                            scope.price.labelFilterMax = Number(splited[3]);
                        }

                        splited[0].split('_').forEach(function(current){
                            switch(current){
                                case "conprecio":
                                    scope.price.withPrice = 1;
                                break;
                                case "gratuita":
                                    scope.price.withPrice = 0;
                                break;
                            }
                        });
                    }else{
                        scope.price.type = 0;
                        scope.price.withPrice = 1;
                        scope.price.withoutPrice = false;
                        scope.price.free = false;
                    }

                    if(Array.isArray(newValue.location)){
                        newValue.location.forEach(function(currentLocation){
                            addSelectedFacet('provincia', {'name':currentLocation, 'label':currentLocation});
                        });
                    }

                    if(newValue.activityFilter){
                        newValue.activityFilter.forEach(function(current){
                             addSelectedFacet('actividad', utils.getActivityByAttr('id', current));
                        });
                    }

                    if(newValue.difficultyFilter){
                        newValue.difficultyFilter.forEach(function(current){
                             addSelectedFacet('dificultad', utils.getDifficulty('id', current));
                        });
                    }

                    if(newValue.region){
                         addSelectedFacet('region', newValue.region);
                    }

                    if(newValue.adventuresFilter){
                        console.log(newValue.adventuresFilter);
                    }
                }
            });

            scope.$watch('ngFacets', function(newValue, oldValue) {
                if (newValue) {
                    var facets = [];
                    if (newValue['activityType.id']) {
                        var facetActivities = [];
                        for (var index in newValue['activityType.id']) {
                            listActivityType.some(function(current) {
                                if (current.id == index) {
                                    var facetActivity = {}
                                    facetActivity.count = newValue['activityType.id'][index];
                                    facetActivity.id = current.id;
                                    facetActivity.name = current.name;
                                    facetActivity.label = current.label;
                                    facetActivity.selected = false;
                                    if(newValue.selectedFacets && newValue.selectedFacets.activity){
                                        newValue.selectedFacets.activity.some(function(currentSelected){
                                            if(current.id == currentSelected){
                                                facetActivity.selected = true;
                                                return true;
                                            }
                                            return false;
                                        });
                                    }
                                    if(scope.ngType != 'activity' || facetActivity.selected){
                                        facetActivities.push(facetActivity);
                                    }
                                    return true;
                                }
                                return false;
                            });
                        }
                        facets.push({
                            'name': 'actividad',
                            'label': 'Tipo de actividad',
                            'list': facetActivities,
                            'type': 'facet'
                        });
                    }

                    if (newValue['golocation.address_components.administrative_area_level_1'] && scope.ngType != 'geoloc') {
                        var facetRegions = [];
                        var exists = false;
                        for (var index in newValue['golocation.address_components.administrative_area_level_1']) {
                            var state = index;
                            var region = utils.getRegionByProvince(index);
                            if(region){
                                if(facetRegions.length > 0){
                                    exists = facetRegions.some(function(current){
                                        if(current.name == region.name){
                                            current.count += newValue['golocation.address_components.administrative_area_level_1'][index];
                                            return true;
                                        }
                                        return false;
                                    });
                                }
                                if(!exists){
                                    var facetRegion = {}
                                    facetRegion.label = region.label;
                                    facetRegion.name = region.name;
                                    facetRegion.count = newValue['golocation.address_components.administrative_area_level_1'][index];
                                    if(newValue.selectedFacets && newValue.selectedFacets.region){
                                        if(newValue.selectedFacets.region.name ==  facetRegion.name){
                                            facetRegion.selected = true;
                                        }
                                    }
                                    facetRegions.push(facetRegion);
                                }
                            }
                        }

                        facets.push({
                            'name': 'region',
                            'label': 'Regiones',
                            'list': facetRegions,
                            'type': 'facet'
                        });
                    }

                    if (newValue['golocation.address_components.administrative_area_level_1'] && scope.ngType != 'geoloc') {
                        var facetLocations = [];
                        var exists = false;
                        for (var index in newValue['golocation.address_components.administrative_area_level_1']) {
                            var state = index;
                            if(facetLocations.length > 0){
                                exists = facetLocations.some(function(current){
                                    if(current.label == state){
                                        current.count += newValue['golocation.address_components.administrative_area_level_1'][index];
                                        return true;
                                    }
                                    return false;
                                });
                            }
                            if(!exists){
                                var facetLocation = {}
                                facetLocation.label = state;
                                facetLocation.name = facetLocation.label;
                                facetLocation.count = newValue['golocation.address_components.administrative_area_level_1'][index];
                                if(newValue.selectedFacets && newValue.selectedFacets.location){
                                    newValue.selectedFacets.location.some(function(currentSelected){
                                        if(facetLocation.name.indexOf(currentSelected) > -1){
                                            facetLocation.selected = true;
                                            return true;
                                        }
                                        return false;
                                    });
                                }
                                facetLocations.push(facetLocation);
                            }
                        }

                        facets.push({
                            'name': 'provincia',
                            'label': 'Provincia',
                            'list': facetLocations,
                            'type': 'facet'
                        });
                    }

                    if (newValue['difficulty']) {
                        var facetDifficulties = [];
                        for (var index in newValue['difficulty']) {
                            difficultyList.some(function(current) {
                                if (current.id == index) {
                                    var facetDifficulty = {}
                                    facetDifficulty.count = newValue['difficulty'][index];
                                    facetDifficulty.id = current.id;
                                    facetDifficulty.name = current.name;
                                    facetDifficulty.label = current.label;
                                    facetDifficulty.selected = false;
                                    if(newValue.selectedFacets && newValue.selectedFacets.difficulty){
                                        newValue.selectedFacets.difficulty.some(function(currentSelected){
                                            if(current.id == currentSelected){
                                                facetDifficulty.selected = true;
                                                return true;
                                            }
                                            return false;
                                        });
                                    }
                                    facetDifficulties.push(facetDifficulty);
                                    return true;
                                }
                                return false;
                            });
                        }
                        facets.push({
                            'name': 'dificultad',
                            'label': 'Dificultad',
                            'list': facetDifficulties,
                            'type': 'facet'
                        });
                    }

                    if (scope.ngIsAdventure) {
                        var facetDuration = { 
                            options: {
                                stepsArray: [
                                    {value: 0, legend: 'Horas'},
                                    {value: 1, legend: 'Días'}
                                ],
                                showTicksValues: true,
                                getPointerColor: function(value) {
                                    return '#6A2871';
                                }
                            } 
                        };
                        scope.duration.slider = facetDuration;

                        scope.price.priceSlider = {
                            options: {
                                minRange : 1,
                                enforceRange: true, 
                                step: 1,
                                floor: scope.price.min,
                                ceil: scope.price.max,
                                getPointerColor: function(value) {
                                    return '#6A2871';
                                },
                                getSelectionBarColor : function(value) {
                                    return '#F39F00';
                                },
                                translate: function(value, sliderId, label) {
                                    return scope.currency ? 'U$S' + value : '$' + value;
                                }
                            }
                        }
                        scope.price.currencySlider = { 
                            options: {
                                stepsArray: [
                                    {value: 0, legend: 'Pesos ARS'},
                                    {value: 1, legend: 'Dolar USD'}
                                ],
                                showTicksValues: true,
                                getPointerColor: function(value) {
                                    return '#6A2871';
                                }
                            }
                        }
                        scope.price.typeSlider =  {
                            label: 'Tipo',
                            options: {
                                stepsArray: [
                                    {value: 1, legend: 'Con precio'},
                                    {value: 0, legend: 'Gratuita'}
                                ],
                                showTicksValues: true,
                                getPointerColor: function(value) {
                                    return '#6A2871';
                                }
                            } 
                        }
                    }

                    facets.forEach(function(currentFacet){
                        var hash = window.location.hash.replace(/pag=\d?&/, '');
                        if(hash.indexOf('filtros') > -1){
                            var indexName = hash.indexOf(currentFacet.name);
                            if(indexName > -1){
                                var subLocation = hash.substr(indexName, hash.length);
                                if(subLocation.indexOf('--') > -1){
                                    var subLocation2 = hash.substr(0, subLocation.indexOf('-'));
                                    var subLocation3 =  hash.substr(subLocation.indexOf('-'), hash.length);
                                    var url = window.location.pathname + subLocation2;
                                    currentFacet.list.forEach(function(current, index){
                                        if(current.selected){
                                            current.url = window.location.pathname + hash.replace("-" + encodeURI(current.name), "");
                                        }else{
                                            current.url = url  + "--"+ current.name + subLocation3;
                                        }
                                    });
                                }else{
                                    var url = window.location.pathname + hash;
                                    currentFacet.list.forEach(function(current, index){
                                        if(current.selected){
                                            current.url = url.replace("-" + encodeURI(current.name), "");
                                        }else{
                                            current.url = url + "-" + current.name;
                                        }
                                    });    
                                }
                            }else{
                                var url = window.location.pathname + hash + "--" + currentFacet.name;
                                currentFacet.list.forEach(function(current, index){
                                    if(current.selected){
                                         current.url = url.replace("-" + encodeURI(current.name), "");
                                    }else{
                                        current.url = url + "-" + current.name;
                                    }
                                });
                            }
                        }else{
                            var url= window.location.pathname + "#filtros--" + currentFacet.name;
                            currentFacet.list.forEach(function(current){
                                if(scope.ngType == 'activity' && currentFacet.name == "actividad" && current.selected){
                                    url = "/buscar-aventura" + window.location.search +  window.location.hash;
                                     current.url = url;
                                }else{
                                    if(current.selected){
                                         current.url = url.replace("-" + encodeURI(current.name), "");
                                    }else{
                                        current.url = url + "-" + current.name;
                                    }
                                }
                            });
                        }
                    });

                    scope.facets = facets;

                        //Necesario para que se refresque el slider
                        $timeout(function() {
                            scope.$broadcast('rzSliderForceRender');
                        }, 100);
                }
            });

            scope.filterByDuration = function(){
                var duration;
                var takeDays;
                if(scope.duration.min >= 0 && scope.duration.max){
                    scope.duration.labelFilterMin = scope.duration.min;
                    scope.duration.labelFilterMax = scope.duration.max;
                    takeDays = scope.takeDays == 0 ? "horas" : "dias";
                    duration = takeDays +'-'+ scope.duration.min +'-'+ scope.duration.max;
                    window.location.hash = maxMin(duration, 'duracion');
                }else{
                    document.querySelector('.error-message.duration').style.display='block';
                    setTimeout(function() {
                        document.querySelector('.error-message.duration').style.display='none';
                    }, 3000);
                }
            }

            scope.filterByPrice = function(){
                var price = createFilterByPrice();
                if(price){
                    window.location.hash = maxMin(price, 'precio');
                }
            }

            scope.changePrice = function(){
                console.log('change');
            }
            
            scope.clearFilter = function(filter, id){
                switch(filter){
                    case 'price':
                        if(scope.price.withPrice){
                            window.location.hash = window.location.hash.replace(/--precio-[a-z]*-[a-z]*-[0-9]*-[0-9]*/, '');
                        }else{
                            window.location.hash = window.location.hash.replace(/--precio-[a-z]*/, '');
                        }
                    break;
                    case 'duration':
                        window.location.hash = window.location.hash.replace(/--duracion-[a-z]*-[0-9]*-[0-9]*/,'');
                    break;
                    case 'actividad':
                        window.location.hash = window.location.hash.replace('-'+encodeURI(id),'');
                    break;
                    case 'dificultad':
                        window.location.hash = window.location.hash.replace('-'+id,'');
                    break;
                    case 'provincia':
                        window.location.hash = window.location.hash.replace('-'+encodeURI(id),'');
                    break;
                    case 'provincia':
                        window.location.hash = window.location.hash.replace('-'+encodeURI(id),'');
                    break;
                    case 'top':
                        window.location.hash = window.location.hash.replace('--top_lugares','');
                    break;
                    case 'region':
                        window.location.hash = window.location.hash.replace('--region-'+encodeURI(id),'');
                    break;
                }
            }

            var maxMin = function(value, name){
                var hash = window.location.hash.replace(/pag=\d?&/, '');
                if(hash.indexOf('filtros') > -1){
                    var indexName = hash.indexOf(name);
                    if(indexName > -1){
                        var subLocation = hash.substr(0, indexName + name.length);
                        var subLocation2 = hash.substr(indexName + name.length, hash.length);
                        if(subLocation2.indexOf('--') > -1){
                            var subLocation2 =  subLocation2.substr(subLocation2.indexOf('--'), hash.length);
                            hash = subLocation + "-" + value + subLocation2;
                        }else{
                            hash = subLocation + "-" + value;
                        }
                    }else{
                        hash = hash + "--" + name + "-" + value;
                    }
                }else{
                    hash = "#filtros--" + name + "-" + value;
                }
                return hash;
            }

            var createFilterByPrice = function(){
                var type = '';
                if(scope.price.withPrice){
                    type = 'conprecio';
                }
                if(scope.price.withoutPrice){
                    if(type){
                        type += '_';
                    }
                    type += 'sinprecio';
                }
                if(!scope.price.withPrice){
                    if(type){
                        type += '_';
                    }
                    type += 'gratuita';
                }

                var price = type;

                if(scope.price.withPrice){
                    if(!isNaN(scope.price.min) && !isNaN(scope.price.max)){
                        if(scope.price.min == 0 && scope.price.max == 0){
                            if(scope.price.withPrice){
                                window.location.hash = window.location.hash.replace(/--precio-[a-z]*/, '');
                            }else{
                                window.location.hash = window.location.hash.replace(/--precio-[a-z]*-[a-z]*-[0-9]*-[0-9]*/, '');
                            }
                            return;
                        }
                        if(scope.currency == 0){
                            price += '-pesos';
                        }else{
                            price += '-dolares';
                        }
                        price += '-'+ scope.price.min +'-'+ scope.price.max;
                        return price
                    }else{
                        document.querySelector('.error-message.price').style.display='block';
                        setTimeout(function() {
                            document.querySelector('.error-message.price').style.display='none';
                        }, 3000);
                        return;
                    }
                }else{
                   return price;
                }
            }
        }
    }
});