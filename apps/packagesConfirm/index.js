var mustache = require('mustache');
var readFile = require('fs-readfile-promise');
var path = require('path');
var utils = require('../../scripts/utils');
var i18nObj = require('./i18n.json');

exports.render = function(req,res){

	var packageId = req.params.packageId;

	var templateFileName = 'desktop.template.html';
	if(req.device && req.device.isMobile){
		templateFileName = 'mobile.template.html';
	}

	var templatePath = path.resolve(__dirname, templateFileName);

	return readFile(templatePath).then(function(buffer){
		var data = {
			text:utils.i18n(i18nObj),
			packageId: packageId
		}

		return {
			title:'Llamaway - Animate a la aventura - Encontra tu aventura',
			metadescription:'El sitio web de Turismo Aventura donde vas a poder encontrar experiencias y actividades para hacer en todo el país',
			html:mustache.render(buffer.toString(),data)
		}
	});
}