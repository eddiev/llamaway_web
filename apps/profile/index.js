var readFile = require('fs-readfile-promise');
var path = require('path');
var mustache = require('mustache');
var utils = require('../../scripts/utils');
var i18nObj = require('./i18n.json');
var api = require('../../helpers/api');
var q = require("q");
var cheerio = require('cheerio');

exports.render = function(req,res){
	var pageName = req.params.pageName;
	var promises = [api.company.getByPageName(pageName)];

	var templateFileName = './desktop.template.html';
	if(req.device && req.device.isMobile){
		templateFileName = './mobile.template.html';
	}
	
	var templatePath = path.resolve(__dirname, templateFileName);

	console.log(q.all(promises).catch(function(err){
		if(err){
			console.log("error 'index/profile' al obtener pageName", err);
		}
		console.log('catch');
	}));

	return q.all(promises).then(function(responses){
		var profile_picture = utils.getCompanyProfileThumbnailSearch(responses[0]);
		var companyId = responses[0]._id;
		
		return readFile(templatePath).then(function(buffer){
			responses[0].golocation.formatted_address = utils.getFormattedAddress(responses[0].golocation);
			var data = {
				company:responses[0],
				companyId:companyId,
				profile_picture:profile_picture,
				companyJSON:JSON.stringify(responses[0]),
				text:utils.i18n(i18nObj)
			}

			var description = responses[0].description.substr(0, 150);
			description = description.replace(/[\"\']/gi, ''); //Tengo que reemplazar la comilla porque me rompe el atributo html
			var $ = cheerio.load(description);

			return {
				title: responses[0].name + ' | Llamaway',
				metadescription: $('div').text() + ' | Llamaway',
				html:mustache.render(buffer.toString(), data)
			};
		});
	});
}