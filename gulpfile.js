var gulp = require('gulp-help')(require('gulp-param')(require('gulp'), process.argv));
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");
var shell = require('gulp-shell');
var livereload = require('gulp-livereload');
var nodemon = require("gulp-nodemon");
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var less = require('gulp-less');
var plumber = require('gulp-plumber');
var glob = require('glob');
var autoprefixer = require('gulp-autoprefixer');
var es = require('event-stream');
var watch = require('gulp-watch');
var watchify = require('watchify');
var colors = require('colors');
var babelify = require("babelify");
var jshint = require('gulp-jshint');
var gutil = require('gulp-util');
var clean = require('gulp-clean');
var fs = require("fs");
var path = require('path');
var runSequence = require('run-sequence');

var lastFingerprints = [];

var currentFingerprint = [1,2,3,4,5,6,7,8,9,10,
                          11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                          21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                          31, 32, 33, 34, 35, 36, 37];

lastFingerprints = lastFingerprints.concat(currentFingerprint);

gulp.task('lint', function() {
  return gulp.src('./apps/**/*public.js')
    .pipe(jshint({
      asi:true, 
      esnext:true
    }))
    .pipe(jshint.reporter('default'))
});

var plumberOption = {
    errorHandler:function(e){
        console.log(e);
        this.emit('end');
    }
}

////////////////////********************** COMPILADORES **********************////////////////////
gulp.task('buildAll', function() {
    runSequence(
        'clean-all',
        ['save_fingerprint', 'build-js', 'less'],
        'minAndClone'
    );
});

gulp.task('build-js', 'Compila todos los archivos Javascirpt dentro de la carpeta apps que finalicen en *public.js y los ubica en public/dist',function(){
    glob('./apps/**/*public.js', function(err, files) { 
        files.forEach(function(entry, index) {
            var b = browserify({
                'entries': [entry],
                'cache': {},
                'packageCache': {},
                'plugin': [watchify]
            });
            b.on('update', build);
            b.on('time', function (time) {
                console.log('Builded: ' + entry + ' in '+ gutil.colors.cyan(time/1000 + ' segs'));
            })
            b.transform(babelify.configure({
                // Use all of the ES2015 spec
                presets: ['es2015']
            }))
            build();

            function build() {
                b
                .bundle()
                .on('error', function (err) {
                    console.log(err.toString());
                    this.emit('end');
                })
                .pipe(source(entry))
                .pipe(gulp.dest('./public/dist/'))
            }
        });
    });
});

gulp.task('test-js', 'Compila todos los archivos Javascirpt dentro de la carpeta apps que finalicen en *public.js y los ubica en public/dist', function(){
    glob('./apps/**/*public.js', function(err, files) {
        files.forEach(function(entry, index) {
            var b = browserify({
                'entries': [entry],
                'cache': {},
                'packageCache': {},
                'plugin': [watchify]
            });
            b.on('update', build);
            b.on('time', function (time) {
                console.log('Builded: ' + entry + ' in '+ gutil.colors.cyan(time/1000 + ' segs'));
            })
            b.transform(babelify.configure({
                // Use all of the ES2015 spec
                presets: ['es2015']
            }))
            build();

            function build() {
                b
                .bundle()
                .on('error', function (err) {
                    console.log(err.toString());
                    this.emit('end');
                })
                .pipe(source(entry))
                .pipe(gulp.dest('./public/dist/'))
                .pipe(livereload());
            }
        });
    });
});

gulp.task('createFingerprints', 'Creo los fingerprints',function(){
    //Clono únicamente archivos minificados (son los que usa prerender y el server)
    cloneWithFingerprints('./public/dist/**/*.min.js', '.min.js', lastFingerprints, true, function(){
        cloneWithFingerprints('./public/dist/**/*.min.css', '.min.css', lastFingerprints, true, function(){
            //Clono archivos (son los que usan localmente). Por ahora no los necesito más, ya que se usa livereload

            //cloneWithFingerprints('./public/dist/**/*.js', '.js', lastFingerprints, false, function(){
            //   cloneWithFingerprints('./public/dist/**/*.css', '.css', lastFingerprints, false, function(){
            //        console.log('Done fingerprints')
            //    });
            //});
        });
    });
});

function cloneWithFingerprints(filepath, fileType, fingerprints, isMinified, cb){
    glob(filepath, function(err, files) {
        files.forEach(function(file) {
            var dir = path.dirname(file);
            var filename = path.basename(file);
            if(isMinified){
                for(var i=0; i<fingerprints.length;i++){
                    fs.createReadStream(dir+'/'+filename).pipe(fs.createWriteStream
                    (dir+'/'+path.basename(file, fileType)+'.'+fingerprints[i]+fileType));
                }
            } else {
                if(!filename.match(/min/g)){
                    console.log(dir+'/'+filename)
                    for(var i=0; i<fingerprints.length;i++){
                        fs.createReadStream(dir+'/'+filename).pipe(fs.createWriteStream
                        (dir+'/'+path.basename(file, fileType)+'.'+fingerprints[i]+fileType));
                    }
                }
            }
        });
        cb();
    });
}

gulp.task('less','Compila todos los archivos LESS dentro de la carpeta apps que finalicen en *public.less y los ubica en public/dist', function() {
    return gulp.src('./apps/**/*public.less')
        .pipe(plumber(plumberOption))
        .pipe(less())
        .pipe(autoprefixer("last 1 version", "> 1%", "ie 8", "ie 7"))
        .pipe(gulp.dest('./public/dist/apps/'));
});
/*************************************************************************************************/
////////////////////********************** MINIFICADORES **********************////////////////////
gulp.task('min' ,false , ['min-js','min-css'])

gulp.task('min-js', 'Uglifyca los archivos JS de la carpeta public/dist', function(){
    return gulp.src(['./public/dist/**/*.js','!**/*.min.js'])
      .pipe(plumber(plumberOption))
      .pipe(uglify({
          mangle: false
      }))
      .pipe(rename(function (path) {
            console.log('Minificando: ' + path.dirname+'\\'+path.basename)
            path.basename += ".min";
      }))
      .pipe(gulp.dest('./public/dist/'));
})

gulp.task('min-css', 'Minifica los archivos CSS de la carpeta public/dist', function(){
    return gulp.src(['./public/dist/**/*.css','!**/*.min.css'])
      .pipe(minifyCSS({
        processImport:false
      }))
      .pipe(rename(function (path) {
            console.log('Minificando: ' + path.dirname+'\\'+path.basename)
            path.basename += ".min";
      }))
      .pipe(gulp.dest('./public/dist/'));
})
/*************************************************************************************************/
////////////////////********************** CLEANERS **********************////////////////////
gulp.task('clean-js', 'Limpieza del directorio /public/dist/apps/**', function () {
    return gulp.src(['./public/dist/**/*.js'], {
        read: false
    })
    .pipe(clean({force: true}));
});
gulp.task('clean-css', 'Limpieza del directorio /public/dist/apps/**', function () {
    return gulp.src(['./public/dist/**/*.css'], {
        read: false
    })
    .pipe(clean({force: true}));
});
gulp.task('clean-all', 'Limpieza del directorio /public/dist/apps/**', function () {
    return gulp.src(['./public/dist'], {
        read: false
    })
    .pipe(clean({force: true}));
});

/*************************************************************************************************/
////////////////////********************** WATCHERS **********************////////////////////
gulp.task('watch',['watch-js','watch-less','watch-templates','watch-statics'])

gulp.task('watch-js', false, ['test-js']);

gulp.task('watch-less', false, ['less'], function() {
    return gulp.watch(['./apps/**/*.less','./less/**/*.less','./helpers/**/*.less'], ['less']);
})

gulp.task('watch-statics',false, function() {
    return gulp.watch(['./public/**/*.css'], notifyLivereload);
})

gulp.task('watch-templates', function() {
    return gulp.watch(['./apps/**/*template.html'], livereload.changed);
})

gulp.task('livereload',false, function(){
    livereload.listen()
})

function notifyLivereload(event) {
    var fileName = require('path').relative('/'+__dirname + '/public', event.path);
    livereload.changed(fileName);
}

/*************************************************************************************************/
////////////////////********************** Otras Yerbas **********************////////////////////

gulp.task('server',false, function (devAPI) {
  var devAPIArg = devAPI ? '--devAPI' : '';
  nodemon({
        script: 'index',
        args: ['--dev',devAPIArg], 
        ignore:['public/**','**/*public.js','helpers/**','scripts/**']
  });
})
gulp.task('internDefault', function() {
    runSequence(
        'watch',
        'livereload',
        'server'
    );
});

gulp.task('minAndClone', function() {
    // Hay que revisar createFingerprints par un callback correcto.
    setTimeout(function(i){
        runSequence(
            'min',
            'createFingerprints',
            'exit'
        );
    }, 8000);
})

gulp.task('exit', function() {
    setTimeout(function(i){
        console.log('Exit now')
        process.exit(0);
    }, 8000);
})

gulp.task('default','Corre el servidor local ejecutando el comando "gulp".',
      ['internDefault'],function(){},{
          options:{
            'devAPI':'Apunta a la version local del api corriendo el localhost:3041'
          }
      });

gulp.task('dev-js',['watch-js','server'])

gulp.task('save_fingerprint', function () {
    var src = require('stream').Readable({
        objectMode: true
    });

    var fingerprint = currentFingerprint[currentFingerprint.length-1];
    console.log('Fingerprint: ' +  fingerprint);

    src._read = function() {
        this.push(new gutil.File({
            cwd: "",
            base: "",
            path: "fingerprint.json",
            contents: new Buffer('{"fingerprint" : '+ fingerprint + '}')

        }));
        this.push(null);
    }
    src.pipe(gulp.dest('./'));

});
