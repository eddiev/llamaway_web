var readFile = require('fs-readfile-promise');
var path = require('path');
var mustache = require('mustache');

var templatePath = path.resolve(__dirname,'./template.html');

var gtmId = process.env.GOOGLE_TAG_MANAGER_ID || 'GTM-PNPNDJ';

exports.render = function(req,res){
	var data = {
		req: req,
		envJSON:JSON.stringify(req.env),
		finger_print: req.env.finger_print,
		gtmId:gtmId
	}
	return readFile(templatePath).then(function(buffer){
		return {
			html:mustache.render(buffer.toString(),data)
		}
	}).catch(function(){
		console.log(arguments)
	});
}