var readFile = require('fs-readfile-promise');
var path = require('path');
var mustache = require('mustache');
var utils = require('../../scripts/utils');
var i18nObj = require('./i18n.json');

exports.render = function(req,res){

	var templateFileName = 'desktop.template.html';
	if(req.device && req.device.isMobile){
		templateFileName = 'mobile.template.html';
	}

	var templatePath = path.resolve(__dirname, templateFileName);

	return readFile(templatePath).then(function(buffer){
		var i18n = utils.i18n(i18nObj, req.env.lang);
		var data = {
			text:i18n,
			rootPath: req.env.rootPath
		}
		return {
			title:'Llamaway - Animate a la aventura - Encontra tu aventura',
			metadescription:' El primer Sitio web de Turismo Aventura, donde vas a encontrar aventuras y excursiones en todo el país',
			html:mustache.render(buffer.toString(),data),
			text:i18n
		};
	});
}

