require('../public/libs/ngstorage/ngStorage.js');


module.exports = angular.module('llamawayServices',['ngStorage'])
.factory('gzzRoute',function($location,$window,$rootScope,$timeout){
	return {
		go:function(path,options){
			$location.path(path);
			if(!$rootScope.$$phase) {
				$rootScope.$apply();
			}
			
			if(options.scroll == 'top'){
				$window.scrollTo(0,0);
			}
		},
		loginAndComeBack:function(){
			location.href = "/ingreso?r=" + encodeURIComponent(location.href)+'&sign=inOrderToContinue';
		},
		registerAndComeBack:function(){
			location.href = "/ingreso?r=" + encodeURIComponent(location.href)+'&sign=inOrderToContinue/#seleccion';
		},
		search:function(name){
			if(name){
				location.href = '/buscar-aventura#!/filtros-'+(name || '');
			}else{
				location.href = '/buscar-aventura#!/filtros';			
			}
		},
		searchRegion:function(name){
			if(name && name != ''){
				location.href = '/buscar-aventura#!/filtros--region-'+ name;
			}
		}
	}
});