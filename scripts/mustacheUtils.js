var utils = require('./utils');

exports._cloudinaryParams = utils.cloudinaryParams;

function getLlamitaSearchResult(){
	var llamita = Math.floor((Math.random() * 3) + 1);
	switch(llamita) {
    case 1:
        return 'https://res.cloudinary.com/llamaway/image/upload/v1457909333/Llamaway_Avatar_01.jpg'
        break;
    case 2:
        return 'https://res.cloudinary.com/llamaway/image/upload/v1457909333/Llamaway_Avatar_02.jpg'
        break;
    default:
        return 'https://res.cloudinary.com/llamaway/image/upload/v1457909334/Llamaway_Avatar_03.jpg'
	} 
}

exports.cloudinary200 = function(){
	var that = this;
	return function(text, render) {
		return that._cloudinaryParams(render(text.trim()),'w_200');
    }	
}


exports.employeeNumberLabel = function(){
	return function(text, render) {
		var value = render(text.trim());
		var dict = {};
		
		dict['10'] = 'Menos de 10';
		dict['100'] = 'Entre 11 y 100';
		dict['250'] = 'Entre 101 y 250';
		dict['1000'] = 'Entre 251 y 1.000';
		dict['5000'] = 'Entre 1.001 y 5.000';
		dict['-1'] =  'Más de 5.000';
		
		return dict[value];
    }	
}

exports.reviewCountLabel = function(){
	return function(text, render) {
		var value = parseInt(render(text.trim()));
		var dict = {};
		value = Math.floor((value/1000));

		var label = (value < 1000 && '<1k') || (value > 1000 && (value+'k'));
		
		return label;
    }	
}

exports.profilePicture = function() {
    return function(profile_picture, render) {
        // Por ahora es una foto. Ver como resolver el array
        var url = '';
        if (profile_picture) {
            //url = this.cloudinaryParams(company.profile_picture.url,'w_200,c_scale')
            url = profile_picture;
        } else {
            url = getLlamitaSearchResult();
        }
    }
}