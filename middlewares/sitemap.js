var sm = require("sitemap");
var api = require('../helpers/api');

var sitemap;

module.exports = function(req, res, next) {
    if (sitemap) {
        if (sitemap.isCacheValid()) {
            getXml(res);
        } else {
            createSitemap(req, res);
        }
    } else {
        createSitemap(req, res);
    }
}

var getXml = function(res) {
    res.header('Content-Type', 'application/xml');
    sitemap.toXML(function(err, xml) {
        if (err) {
            console.log(err);
            return res.status(500).end();
        }
        res.send(xml);
    });
}

var createSitemap = function(req, res) {
    sitemap = sm.createSitemap({
        hostname: 'https://www.llamaway.com',
        cacheTime: 43200000
    });

    sitemap.add({
        url: '/'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-acuaticas'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-avistajes'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-cabalgatas'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-camping'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-ciclismo'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-deportes_extremos'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-deportes_invernales'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-expediciones_travesias'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-montañismo_trekking'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-pesca'
    });
    sitemap.add({
        url: '/buscar-aventura#!/filtros--actividad-otros'
    });
    sitemap.add({
        url: '/llamashop'
    });
    
    getXml(res);
}

