var mustache = require('mustache');
var readFile = require('fs-readfile-promise');
var path = require('path');
var utils = require('../../scripts/utils');
var i18nObj = require('./i18n.json');


exports.render = function(req,res){
	var adventureId = req.params.adventureId;

	var templateFileName = 'desktop.template.html';
	if(req.device && req.device.isMobile){
		templateFileName = 'mobile.template.html';
	}

    var templatePath = path.resolve(__dirname, templateFileName);

	return readFile(templatePath).then(function(buffer){

		var data = {
			text:utils.i18n(i18nObj),
			adventureId:adventureId
		}

		return {
			title:'Cargar imágenes de la aventura',
			html:mustache.render(buffer.toString(),data)
		}
	});

}
