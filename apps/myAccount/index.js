var readFile = require('fs-readfile-promise');
var path = require('path');
var mustache = require('mustache');
var utils = require('../../scripts/utils');
var i18nObj = require('./i18n.json');


exports.render = function(req,res){
	var companyId = req.params.companyId;

	var templateFileName = 'desktop.template.html';
	if(req.device && req.device.isMobile){
		templateFileName = 'mobile.template.html';
	}

	var templatePath = path.resolve(__dirname, templateFileName);

	return readFile(templatePath).then(function(buffer){
		var data = {
			url:req.env.url,
			text:utils.i18n(i18nObj)
		}
		return {
			title:'Mi cuenta',
			html:mustache.render(buffer.toString(),data)
		};
	});
}

