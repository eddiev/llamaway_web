var readFile = require('fs-readfile-promise');
var path = require('path');
var mustache = require('mustache');
var utils = require('../../scripts/utils');
var i18nObj = require('./i18n.json');

var templatePath = path.resolve(__dirname,'./template.html');

exports.render = function(req,res){

	return readFile(templatePath).then(function(buffer){
		var data = {
			text:utils.i18n(i18nObj)
		}
		return {
			title:'Página inexistente',
			html:mustache.render(buffer.toString(),data)
		}
	});
}