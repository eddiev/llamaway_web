var llamaApi = require('../../helpers/api');

angular.module('notFound', [])
    .controller('NotFoundCtrl', function($scope, $rootScope) {
        $scope.showMyAccount = function() {
            location.href = '/mi-cuenta';
        }
        $scope.goHome = function() {
            location.href = '/';
        }
        $scope.back = function() {
            window.history.back();
        }
        loadMissingPerson();

        function loadMissingPerson() {
            llamaApi.notFoundPersons.get().then(function(person) {
                $scope.data.person = person.data;
                $scope.data.missingPerson = true;
            }).catch(function(err) {
                $scope.data.missingPerson = false;
                console.log('error en la api not found' + err)
            }).finally(function() {
                $scope.$apply();
            });
        }

        llamaApi.user.getCurrent().then(function(user) {
            $scope.ix.userLoaded = true;
        }).catch(function() {
            $scope.ix.userLoaded = false;
        });
    });
