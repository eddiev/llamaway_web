var llamaApi = require('../../helpers/api');
var ga = require('../../helpers/ga');

angular.module('packagesConfirm',[]).controller('PackagesConfirmCtrl', function($scope, $q){
	
	llamaApi.loadQ($q);

	$scope.data = {};

	ga.event('User', 'PackageComfirm', "Show");

	$scope.$watch("data.packageId", function(newVal){
		if(newVal){
			llamaApi.package.getPackage(newVal).then(function(llamaPackage){
				$scope.data.package = llamaPackage;
				if(llamaPackage.state == 'pending' || llamaPackage.state == 'intention_payment_mp' || llamaPackage.state == 'other_mp' && llamaPackage.mp.payment.status == 'rejected' || llamaPackage.mp.payment.status == 'cancelled'){
					llamaApi.package.getMPButton($scope.data.packageId).then(function(res){
						$scope.data.button = res.button;
					});
				}else{
					console.log("El paquete no esta pendiente. State:" + llamaPackage.state);
				}
			});
		}
	});

	$scope.confirmPackage = function(){
		llamaApi.package.intentionPaymentMP($scope.data.packageId);
		ga.event('User', 'PackageConfirm', "Success");
		window.location.href = "/mi-cuenta#/mis-paquetes";
	}

	$scope.cancel = function(){
		llamaApi.package.cancel($scope.data.packageId);
		ga.event('User', 'PackageComfirm', "Cancel");
		window.history.back();
	}
});