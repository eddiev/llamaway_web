var directives = require('../../scripts/llamawayDirectives');
var llamaApi = require('../../helpers/api');
var autocompleteModule = require('../../helpers/autocomplete/ng.module');
var cookie = require('cookies-js');
var q = require('q');
var gaModule = require('../../helpers/ga/ng.module');
var ga = require('../../helpers/ga');
var llamaLocation = require('../../helpers/location/ng.module');

angular.module('llamashop', [])
    .controller('LlamashopCtrl', function($scope) {

        $scope.ix = {};

        window.s = $scope;
    });
