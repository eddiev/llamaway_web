var q = require('q');
var mustache = require('mustache');
var readFile = require('fs-readfile-promise');
var path = require('path');
var rootDir = path.dirname(require.main.filename);


exports.render = function(req,res,data){

	data.dnsPrefetch = {
		fontsGoogleApis:"fonts.googleapis.com",
		ajaxGoogleApis:"ajax.googleapis.com",
		fontsGsStatic:"fonts.gstatic.com",
		maxcdnBootstrapcdn:"maxcdn.bootstrapcdn.com",
		llamaApi:req.env.api_host
	}

	var dataForMain = {
		req: req,
		title: data.app.title ? data.app.title : 'Llamaway | Viví la aventura',
		metadescription: data.app.metadescription || '',
		envJSON:JSON.stringify(req.env),
		appName:data.appName,
		content:data.app.html,
		options:data.options,
		dnsPrefetch:data.dnsPrefetch,
		finger_print: req.env.finger_print,
		rootPath: req.env.rootPath,
		domain: 'https://www.llamaway.com/',
		currentUrl: req.url,
		gtmId:req.env.gtmId,
		googleAnalyticsId: req.env.googleAnalyticsId
	}
	
	var templateFileName = 'desktop.template.html';
	if(req.device && req.device.isMobile){
		templateFileName = 'mobile.template.html';
	}
	
	var templatePath = path.resolve(__dirname, templateFileName);

	return readFile(templatePath).then(function(buffer){
		return {
			html:mustache.render(buffer.toString(),dataForMain)
		}
	})	
}

exports.loadApp = function(req,res,appName,options){
	options = options || {};
	 
	var that = this;
	var appModule = require(path.resolve(rootDir+'/apps/'+appName));

	return appModule.render(req,res).then(function(app){
		return that.render(req,res,{
			app:app,
			appName:appName,
			options:options
		});

	}).catch(function(err){
		console.log('Error appModule.render');
		console.log(err);
	});
}
exports.loadAppMiddleware = function(appName,options){
	var that = this;
	return function(req,res,next){
		that.loadApp(req,res,appName,options || {}).then(function(appRes){
			res.end(appRes.html);
		}).catch(function(){
			next();
		})
	}
}