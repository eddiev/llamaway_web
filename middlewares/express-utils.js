var url = require('url');

module.exports = function(req,res,next){
	req.fullURL = (req.connection.encrypted ? 'https://' : 'http://') + req.headers.host + req.url;
	req._location =  url.parse(req.fullURL,true);
	req._location.origin = req._location.protocol + '//' + req._location.host

	next();
}