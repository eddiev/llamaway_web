var mustache = require('mustache');
var readFile = require('fs-readfile-promise');
var path = require('path');
var q = require('q');

var api = require('../../helpers/api');
var utils = require('../../scripts/utils');
var i18nObj = require('./i18n.json');
var mustacheUtils = require('../../scripts/mustacheUtils');

exports.render = function(req,res){
	var templateFileName = 'desktop.template.html';
	if(req.device && req.device.isMobile){
		templateFileName = 'mobile.template.html';
	}
	
	var templatePath = path.resolve(__dirname, templateFileName);
	var adventureId = req.params.adventureId;
	var promises = [api.company.getAdventureById(adventureId)];

	return q.all(promises).then(function(responses){
		var adventure = responses[0];
		for(var i=0;i<adventure.activityType.length; i++){
			switch(adventure.activityType[i].id) {
    			case 1: //'Acúatica'
        			adventure.activityType[i].class = 'sprite-acuatico';
        		break;
			    case 2://'Avistaje de aves':
			        adventure.activityType[i].class = 'sprite-avistaje';
			    break;
    			case 3://'Cabalgatas':
        			adventure.activityType[i].class = 'sprite-cabalgata';
        		break;
			    case 4://'Camping':
			        adventure.activityType[i].class = 'sprite-camping';
			    break;
    			case 5://'Ciclismo':
        			adventure.activityType[i].class = 'sprite-ciclismo';
        		break;
			    case 6://'Deportes Extremos':
			        adventure.activityType[i].class = 'sprite-extremo';
			    break;
    			case 7://'Deportes Invernales':
        			adventure.activityType[i].class = 'sprite-invernal';
        		break;
			    case 8://'Expediciones/Travesías':
			        adventure.activityType[i].class = 'sprite-expedicion';
			    break;
			    case 9://'Montañismo y Trekking':
			        adventure.activityType[i].class = 'sprite-montanismo';
			    break;
    			case 10://'Pesca':
        			adventure.activityType[i].class = 'sprite-pesca';
        		break;
			    case 11://'Tours/Otros':
			        adventure.activityType[i].class = 'sprite-otros';
			    break;
			} 
		}

		if(req.device && req.device.isMobile){
			adventure = utils.getDescriptionFromLookups(adventure);
		} else {
			adventure = utils.getDescriptionFromLookupsDesktop(adventure);
		}

		if(adventure.profile_picture.length == 0){
			adventure.profile_picture[0] = {
				'url': utils.getCompanyProfileThumbnail(adventure, true)
			}
		}
		if(adventure.activityType.length > 4){
			adventure.activityType = adventure.activityType.slice(0, 4);
		}

		//Traigo imágenes transformadas en menor o mejor calidad. Dependiendo del dispositivo
		if(req.device && req.device.isMobile){
			adventure = utils.getProfilePicturesTransformations(adventure, true);
		} else {
			adventure = utils.getProfilePicturesTransformations(adventure, false);
		}

		adventure.description = adventure.description.replace(/\'/g, "&quot;");
		adventure.golocation.formatted_address = utils.getFormattedAddress(adventure.golocation);
		var data = {
			adventure:adventure,
			adventureJSON:JSON.stringify(adventure),
			fullURL:req.fullURL,
			text:utils.i18n(i18nObj),
			utils:mustacheUtils
		}

		var description = adventure.name + ' en ' + adventure.golocation.formatted_address;
		description = description.substr(0, 144);

		return readFile(templatePath).then(function(buffer){
			return {
				title: adventure.name + ' | Llamaway',
				metadescription: description + ' | Llamaway',
				html:mustache.render(buffer.toString(),data)
			};
		});
	}).catch(function(err){
		 console.log('err',err)
	})
}