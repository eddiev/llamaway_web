var ngAutocomplete = require('ng-autocomplete');
var ga = require('../ga');

module.exports = angular.module('location', ['ngAutocomplete', 'ngCookies'])
    .directive('gzzLocationButton', function($q, $cookies) {
        return {
            restrict: 'AE',
            transclude: true,
            scope: {
                ngModel: '=?ngModel',
                ngLabel:'=?ngLabel',
                ngGoToSearch:'=?ngGoToSearch',
                ngLang: '='
            },
            template: '<a href="" class="btn button button-lg button-primary location" type="button" data-ng-click="getLocation()" data-ng-lang="data.text"><i class="fa fa-map-marker {{ngLabel ? \'\' : \'withoutText\'}}"></i>{{ngLabel}}</a>',
            link: function(scope, el, attrs) {
                scope.alertError = function () {
                    alert(scope.ngLang);
                };

                scope.getLocation = function(){
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            if (position.coords && position.coords.latitude && position.coords.longitude) {
                                var geocoder = new google.maps.Geocoder;

                                geocoder.geocode({
                                    'location': {
                                        lat: parseFloat(position.coords.latitude),
                                        lng: parseFloat(position.coords.longitude)
                                    }
                                }, function(results, status) {
                                    if (status === google.maps.GeocoderStatus.OK) {
                                        if (results.length > 0) {
                                            if(scope.ngGoToSearch){
                                                // scope.ngModel = {
                                                //     address: results[0].formatted_address,
                                                //     coords: {
                                                //         lat: results[0].geometry.location.lat(),
                                                //         lng: results[0].geometry.location.lng()
                                                //     }
                                                // }
                                                scope.$emit('gzzLocation_details', results[0]);
                                                scope.$apply();
                                            }else{
                                                results.some(function(current, index) {
                                                    if (current.geometry.location_type == "APPROXIMATE") {
                                                        scope.$apply(function() {
                                                            location.href = '/buscar-aventura#!/filtros--geoloc_' + position.coords.latitude + '_' + position.coords.longitude;
                                                        });
                                                        return true;
                                                    }
                                                    return false;
                                                });
                                            }
                                            scope.$apply();
                                        }else{
                                            results.some(function(current, index) {
                                                if (current.geometry.location_type == "APPROXIMATE") {
                                                    ga.event('User', 'AdventuresCloseToMe', results[index].address_components[0].long_name);
                                                    scope.$apply(function() {
                                                        location.href = '/buscar-aventura#!/filtros--geoloc_' + position.coords.latitude + '_' + position.coords.longitude;
                                                    });
                                                    return true;
                                                }
                                                return false;
                                            });
                                        }
                                    }
                                });
                            }
                        }, scope.alertError);
                    } else {
                        scope.alertError();
                    }
               }
            }
        }
    })

    .directive('gzzGoogleLocation', function() {
        return {
            restrict: 'AE',
            template: '<input type="text" ng-autocomplete ng-model="autocomplete" details="details" location="location"/>',
            link: function(scope, el, attrs) {
                scope.getAddressComponents = function (idetails) {
                    if (!idetails) {
                        return {};
                    }

                    var address_components =  {};

                    idetails.address_components.find(function(el, index) {
                        el.types.find(function (el2, index) {
                            if (el2 === 'administrative_area_level_1') {
                                address_components.administrative_area_level_1 = el.long_name;
                            } else if (el2 === 'administrative_area_level_2') {
                                address_components.administrative_area_level_2 = el.long_name;
                            } else if (el2 === 'locality') {
                                address_components.locality = el.long_name;
                            } else if (el2 === 'country') {
                                address_components.country = el.long_name;
                            }
                        });
                    });
                    
                    return address_components;
                }

                scope.isAddressComponentsComplete = function (idetails) {
                    var address_components =  {};

                    if (idetails === undefined || idetails.address_components === undefined) {
                        return false;
                    }

                    if( Object.prototype.toString.call(idetails.address_components) === '[object Array]' ) {
                        address_components = scope.getAddressComponents(idetails);
                    } else {
                        address_components = idetails.address_components;
                    }

                    if (address_components !== undefined) {
                        if ((address_components.hasOwnProperty("administrative_area_level_1") === true && address_components.administrative_area_level_1 != '') && 
                           (address_components.hasOwnProperty("administrative_area_level_2") === true && address_components.administrative_area_level_2 != '') &&
                           (address_components.hasOwnProperty("country") === true && address_components.country != '')) {
                            return true;
                        }
                    }

                    return false;
                }

                scope.$watch('details', function (idetails) {
                    /*
                        {
                            "location" : {
                               "lat" : 40.714232,
                               "lng" : -73.9612889
                            },
                            "formatted_address" : "277 Bedford Ave, Brooklyn, NY 11211, USA",
                            "administrative_area_level_1": "New York",
                            "administrative_area_level_2": "Kings County"
                        }                    
                    */
                    if (scope.autocomplete === '') {
                        return;
                    } else {
                        if (scope.details !== undefined) {
                            if (!scope.isAddressComponentsComplete(idetails)) {
                                scope.golocation = {};                                
                                return;
                            } else {
                                // Armo el location
                                scope.golocation = {
                                    "formatted_address": scope.details.formatted_address,
                                    "location": {
                                        "lat": scope.details.geometry.location.lat(),
                                        "lng": scope.details.geometry.location.lng()
                                    }
                                }
                                scope.golocation.address_components = scope.getAddressComponents(idetails);
                            }
                        }
                    }
                });
            }
        }
    });