var llamaApi = require('../../helpers/api');

angular.module('callbackMercadoPago', []).controller('CallbackMercadoPagoCtrl', function($scope, $q, gzzRoute) {

    llamaApi.loadQ($q);

    $scope.data = {
        userLoaded: false
    };

    var checkSession = function() {
        return llamaApi.user.getCurrent().catch(function() {
            return false;
        });
    }

    var flag = sessionStorage.getItem('slpkg');
    if(flag){
        $scope.flag = flag;
    }

    checkSession().then(function(user) {
        if (!user) {
            $scope.data.userLoaded = false;
            gzzRoute.loginAndComeBack();
        } else {
            $scope.data.userLoaded = true;
        }
    });

    $scope.$watch("data.packageId", function(newVal) {
        if (newVal) {
	        if (!$scope.data.package) {
                llamaApi.package.getPackage($scope.data.packageId).then(function(llamaPackage) {
                    if (llamaPackage) {
                        $scope.data.package = llamaPackage;
                    } else {
                        console.error('informar error generico');
                    }
                });
	        }
        }
    });

    $scope.notNow = function() {
        if($scope.flag){
            location.href = '/mi-cuenta#/mis-consultas';
        }else{
            location.href = '/mi-cuenta#/mis-paquetes';
        }
    }
});
