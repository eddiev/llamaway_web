var services = require('../../scripts/llamawayServices');
var qs = require('querystringify');
var llamaApi = require('../../helpers/api');
var disposable = require('is-disposable-email');
var llamaLocation = require('../../helpers/location/ng.module');
var ga = require('../../helpers/ga');
var store = require('store2');

angular.module('signInUp',[
	'ngRoute',
	'angularTrix',
	llamaLocation.name
])
.config(function($routeProvider) {

	$routeProvider
	.when("/:show/:status?",{})
	.otherwise( { redirectTo: "/login" });

})
.controller('SignInUpCtrl', function($scope,
	       							  $route,
									  $routeParams,
									  $timeout,
									  $rootScope,
									  gzzRoute,
									  $q,
									  $location,
									  $document){

	llamaApi.loadQ($q)

	var queryParams = qs.parse(location.search);

	$scope.data = {
		genericErrorMessage:'Completá este dato',
		invalidFormat: 'Ingresá un email válido',
		invalidPasswordLength: 'La longitud debe ser mayor a 6 y menor a 20',
		emailNotRegistered: 'El email no se encuentra registrado',
		signin: {},
		error: {
			isInvalidEmail: false
		},
		showTawk: false,
		company:{}

	}

	$scope.ix = {
		sign:queryParams.sign||false
	}

	$scope.$on('$routeChangeSuccess', function (ev, current, prev) {
		$scope.$broadcast('restartValidation', function(){});
		if ($routeParams.show !== "registrar_empresa") {
			$scope.hideTawlkto();
		}
		if(location.search.indexOf("userCreated=false") > -1){
			var isCompany = false;
			if($routeParams.show == "registrar_empresa"){
				isCompany = true;
				store('afterLoginCompany', 'true');
			}
			if($routeParams.show == "registrar_empresa" || $routeParams.show == "registrar_aventurero"){
				if(location.search.indexOf("provider=facebook") > -1){
					llamaApi.user.loginWithFacebook($routeParams.show == "registrar_empresa" ? '/mi-cuenta#/mis-datos' : location.origin, isCompany);
				}
			}else{
				$scope.ix.show = $routeParams.show;
				$scope.ix.status = $routeParams.status;
			}
		}else{
			$scope.ix.show = $routeParams.show;
			$scope.ix.status = $routeParams.status;
		}
	});

	$scope.goHome = function() {
		location.href = '/';
	}

	$scope.signInEmailPassword = function(email, password){
		if(email || password){
			$scope.data.signin.email = email;
			$scope.data.signin.password = password;
		}

		$scope.ix.submitting = 'signin';
		$scope.$broadcast('validateAll','signin',function(valid){
			if(valid){
				var email = $scope.data.signin.email || '';
				var password = $scope.data.signin.password || '';
				llamaApi.user.loginWithEmailAndPassword({
					'email':email,
					'password':password
				},queryParams.r ||  location.origin).catch(function(){
					$scope.ix.wrongEmailOrPassword = true;
					$scope.ix.submitting = false;
				});
			}else{
				$scope.ix.submitting = false;
				$scope.ix.wrongEmailOrPassword = false;
			}
		});
	}

	$scope.showTawlkto = function() {
		//Start of Tawk.to Script
		if (window.Tawk_API === undefined) {
			var $_Tawk_API = {};
			var $_Tawk_LoadStart = new Date();
			var s1 = angular.element(document.createElement("script"));
			var s0 = angular.element(document.getElementsByTagName("script")[0]);
			s1.attr('async', 'true');
			s1.attr('src', 'https://embed.tawk.to/57d1e8c211028a70b19365e6/default');
			s1.attr('charset', 'UTF-8');
			s1.attr('crossorigin','*');
			s0.parent()[0].insertBefore(s1[0], s0[0]);
		} else {
			window.Tawk_API.showWidget();
		}
	}

	$scope.hideTawlkto = function() {
		if (window.Tawk_API !== undefined) {
			window.Tawk_API.hideWidget();
		}
	}

	$scope.signUp = function(event){
		event.preventDefault();
		$scope.ix.submitting = 'signup';
		$scope.ix.emailConflict=false;
		var formName = event.target.name;

		$scope.$broadcast('validateAll', formName, function(valid){
			if(valid){
				if(validateEmail($scope.data.email)){
					$rootScope.$emit('toogleSpinner', true);
					var origin = localStorage.getItem('origin') || "";
					llamaApi.user.signupAndLogin({
						'email':$scope.data.email,
						'password':$scope.data.password,
					}).then(function(newUser){
						if($scope.ix.show == 'registrar_empresa'){
							llamaApi.company.create({
								'contactName':$scope.data.company.contactName,
								'phone':$scope.data.company.phone,
							}).then(function(newCompany){
								(function(){
									localStorage.setItem('reg', 'emp');
									location.href="/mi-cuenta#/mis-datos";
								})();
							}).catch(function(err){
								console.log(err);
								endedSubmit();
							});
						}else{
							localStorage.setItem('reg', 'user');
							(function(){
								location.href="/";
							})();
						}
					}).catch(function(err){
						$rootScope.$emit('toogleSpinner', false);
						if(err.statusCode == 409){
							ga.event('User', 'Email-Conflict','SignUp', $scope.data.email);
							$scope.ix.emailConflict=true;
						}
						endedSubmit();
					});
				}else{
					$scope.data.error.isInvalidEmail = true;
					endedSubmit();
				}
			}else{
				endedSubmit();
			}
		});
	}

	$scope.signInUpWithFacebook = function(){
		var isCompany;
		switch($routeParams.show){
			case "registrar_empresa":
				isCompany = true;
				store('afterLoginCompany', 'true');
				llamaApi.user.loginWithFacebook('/mi-cuenta#/mis-datos', isCompany);
			break;
			case "registrar_aventurero":
				isCompany = false;
			default:
				llamaApi.user.loginWithFacebook(queryParams.r || location.origin, isCompany);
		}
	}

	$scope.recoverPassword = function(){
		$scope.ix.submitting = 'recover';
		$scope.ix.emailNotExists=false;
		$scope.ix.invalidFormat=false;
		$scope.$broadcast('validateAll','recover-password',function(valid){
			if(valid){
				llamaApi.user.recoverPassword($scope.data.recover.email).then(function(response){
					$scope.ix.submitting = false;
					gzzRoute.go('resetPassword/sent');
				}).catch(function (err) {
					if(err.statusCode == 404){
						$scope.ix.emailNotExists=true;
					}
					$scope.$broadcast('validateOne', function(){});
					$scope.ix.submitting = false;
				});
			}else{
				$scope.ix.invalidFormat = true;
				$scope.ix.submitting = false;
			}
		});
	}

	var endedSubmit = function(){
		$rootScope.$emit('toogleSpinner', false);
		$scope.$broadcast('validateOne', function(){});
		$scope.ix.submitting = false;
	}

	function validateEmail(email){
		if(email){
			if(disposable(email)){
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	$scope.showSearchBox = function(){
		$rootScope.$broadcast('showSearchBox');
	}

	/*Trix*/
	$scope.trixChange= function(){
		console.log('Chequear que no haya links')
	}

	$scope.trixAttachmentAdd= function(e){
		console.log('Chequear que no haya archivos')
	}


	window.s = $scope;
});
